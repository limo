#!/bin/sh

# updated: 6/20/2009

if [ -f `which easy_install` ]; then
	echo "Installing mysql-server and subversion" && \
	apt-get install mysql-server-5.0 libmysqlclient15-dev subversion && \
	echo "Beginning source builds" && \
	mkdir -p build && \
	cd build && \

	echo "Building httplib2..." && \
	wget -nc http://httplib2.googlecode.com/files/httplib2-0.4.0.tar.gz && \
	tar xzvf httplib2-0.4.0.tar.gz && \
	echo "Fetching and applying custom patch..." && \
	patch -N build/httplib2-0.4.0/httplib2/__init__.py < build/httplib2.303error.patch
	cd httplib2-0.4.0/ && \
	python setup.py install && \
	cd .. && \

	echo "Building lighttpd..." && \
	wget -nc http://www.lighttpd.net/download/lighttpd-1.4.23.tar.gz &&\
	tar xzvf lighttpd-1.4.23.tar.gz && \
	cd lighttpd-1.4.23 && \
	./configure --disable-ipv6 --disable-lfs --without-zlib --without-bzip2 --without-deflate --prefix=/usr && \
	make && \
	sudo make install && \
	cd .. &&\

	echo "Building MySQL-python" && \
	wget -nc http://downloads.sourceforge.net/mysql-python/MySQL-python-1.2.3c1.tar.gz && \
	tar xzvf MySQL-python-1.2.3c1.tar.gz && \
	cd MySQL-python-1.2.3c1 && \
	python setup.py install && \
	cd .. && \

	echo "Building Imaging" && \
	wget -nc http://effbot.org/downloads/Imaging-1.1.7b1.tar.gz && \
	tar xzvf Imaging-1.1.7b1.tar.gz && \
	cd Imaging-1.1.7b1 && \
	python setup.py install && \
	cd .. && \

	echo "Building multiprocessing" && \
	wget -nc http://pypi.python.org/packages/source/m/multiprocessing/multiprocessing-2.6.0.2.tar.gz &&\
	tar xzvf multiprocessing-2.6.0.2.tar.gz && \
	cd multiprocessing-2.6.0.2 && \
	python setup.py install && \
	cd .. && \

	echo "Building SQLite library" && \
	wget -nc http://www.sqlite.org/sqlite-amalgamation-3.6.15.tar.gz &&\
	tar xzvf sqlite-amalgamation-3.6.15.tar.gz &&\
	cd sqlite-3.6.15 &&\
	./configure --prefix=/usr &&\
	make &&\
	make install &&\
	cd .. && \

	echo "Building SQLite python module: apsw" && \
	svn co http://apsw.googlecode.com/svn/apsw/trunk apsw &&\
	cd apsw && \
	python setup.py install

else
	echo "You must install the python 'setuptools' package. I can't do this automatically."
	exit 1
fi

