from __future__ import with_statement
import hashlib, os
from limoutil import *
from settings import Settings

class _AggreggateFileSet:
	""" Provides a base class for aggreggating text files.

		First generate some test content.

		>>> import os
		>>> if os.path.isfile(Settings.tempDir+"/test.js"): os.remove(Settings.tempDir+"/test.js")
		>>> o = open(Settings.tempDir+"/test.js").write("function func1() {									 \nalert('with lots of space');}")
		>>> o.close()
		>>> if os.path.isfile(Settings.tempDir+"/test2.js"): os.remove(Settings.tempDir+"/test2.js")
		>>> o = open(Settings.tempDir+"/test2.js").write("function func2() { alert('this is function 2');}")
		>>> o.close()

		Then use defaultFile() to load the test content into the file set.

		>>> Cache.JS.defaultFile(Settings.tempDir+"/test.js")
		>>> Cache.JS.defaultFile(Settings.tempDir+"/test2.js")
		>>> hash = hashlib.sha1(''.join([Settings.tempDir+"/test.js",Settings.tempDir+"/test2.js"])).hexdigest()
		>>> assert(Cache.JS.getOutputFile().startswith('cache/'+hash))
		>>> Cache.JS.saveContent()
		>>> assert(os.path.isfile(Cache.JS.getOutputFile())
		>>> Cache.JS.getText()
		'function func1(){alert('with lots of space');}function func2(){alert('this is function 2');}'
	"""

	def __init__(self):
		self._order = []
		self._data = {}
		self._mtimes = {}

	def defaultFile(self, file):
		if file is None or not os.path.isfile(file):
			if Settings.abortOnMissingFile:
				raise HTTPStatusError(404, "Required asset is missing: %s" % file)
			if Settings.warnOnMissingFile:
				log("WARNING: File Cache could not load requested file: %s" % file)
		if self._data.get(file, False): # dont add the same file twice
			if os.path.getmtime(file) > self._mtimes.get(file, 0): # unless its been modified
				log("Reading file from disk into cache: %s" % file)
			else:
				# log("Skipping read of unmodified file: %s %d <= %d " % (file, os.path.getmtime(file), self._mtimes.get(file, 0)))
				return
		if file is not None and os.path.isfile(file):
			self._order.append(file)
			# TODO: lazy reading, only read() if, in the end, the whole request needs to be written into the cache
			self._mtimes[file] = os.path.getmtime(file)
			with open(file,"rb") as f:
				self._data[file] = f.read()

	def getOutputFile(self):
		""" Subclasses should implement this to add the proper extension """
		fname_hash = hashlib.sha1(''.join(self._order)).hexdigest()
		mtime_hash = hashlib.sha1(''.join([str(os.path.getmtime(f)) for f in self._order])).hexdigest()
		return Settings.cacheDir+os.path.sep+"%s-%s" %(fname_hash, mtime_hash)

	def saveContent(self):
		f = self.getOutputFile()
		if not os.path.isfile(f):
			# remove stale cache files first
			"""DISABLED
			key = f[6:10]
			for file in os.listdir(Settings.cacheDir):
				if file == f:
					file = os.path.sep.join([Settings.cacheDir,file])
					log("Removing stale cache file: %s" % (file))
					os.remove(file)
			"""
			# then write the fresh one
			log("Writing files: [%s] to cache: %s" % (', '.join(self.order()), self.getOutputFile()))
			leader = "/* Files: "+', '.join(self.order())+" */\n"
			packed = ''.join([self.filter(self._data[key]) for key in self.order()])
			with open(self.getOutputFile(),'wb') as f:
				f.write(leader)
				f.write(packed)

	def order(self):
		return self._order

	def clear(self):
		self._order = []
		self._data.clear()

	def getText(self):
		self.saveContent()
		with open(z_file,"rb") as f:
			return f.read()

	def filter(self, text):
		""" Subclasses should implement this in order to do minification, compression, etc. """
		return text

import jsmin, cssmin

class _JSFileSet(_AggreggateFileSet):
	def filter(self, text):
		if Settings.developmentMode:
			return text;
		else:
			return jsmin.jsmin(text)
	def getOutputFile(self):
		return _AggreggateFileSet.getOutputFile(self)+".js"

class _CSSFileSet(_AggreggateFileSet):
	def filter(self, text):
		if Settings.developmentMode:
			return text;
		else:
			return cssmin.minimalize(text)
	def getOutputFile(self):
		return _AggreggateFileSet.getOutputFile(self)+".css"
	def order(self):
		return self._order

class Cache:
	CSS = _CSSFileSet()
	JS = _JSFileSet()
