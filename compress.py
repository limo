from __future__ import with_statement
import zlib, cStringIO, os, struct, hashlib
import gzip as _gzip
from limoutil import *
from fcgi.fcgiutil import flatten

def gzip(data, cache_file=None):
	""" Compresses a string, returns the compressed bytes.
		If cache_file is specified, uses that file as a cache.
	"""
	profile("gzip")
	try:
		using_temp_file = False
		if cache_file is None:
			cache_file = os.path.sep.join([Settings.cacheDir,"tmp_"+hashlib.sha1(os.urandom(1024)).hexdigest()])
			# log("Using temp file: %s" % cache_file)
			using_temp_file = True
		if not os.path.isfile(cache_file):
			# log("writing %d bytes to cache file: %s" % (len(data), cache_file))
			with open(cache_file, "wb") as fo:
				fz = _gzip.GzipFile(fileobj=fo, mode="wb", compresslevel=9)
				try:
					fz.write(data)
				finally:
					fz.close()
		with open(cache_file, "rb") as fi:
			data = fi.read()
		# log("gzip: read %d bytes" % len(data))
		if using_temp_file:
			# log("Deleting temp file: %s" % cache_file)
			os.remove(cache_file)
		return data
	finally:
		ms = profile("gzip")
		# log("gzip: %.2f ms" % ms)

def manual_gzip(data, cache_file=None):
	_header = ("\037\213\010\000" # magic, type, flags
	           "\000\000\000\000" # mtime
						 "\002\377") # xfl, os
	co = zlib.compressobj()
	return ''.join([_header, co.compress(data)[2:], co.flush()])
		# struct.pack("<ll",zlib.crc32(data),len(data))])

def generate_file(file, block_size=4096):
	""" Returns a generator that reads a file in blocks, for streaming large files. """
	with open(file, "rb") as f:
		while True:
			data = f.read(block_size)
			if data is None or len(data) == 0:
				break
			yield data

def generate_gzip(input, cache_file=None):
	header = "\037\213"
	buf = cStringIO.StringIO()
	pass_through = False
	for text in flatten(input):
		if text not in (None, ''):
			if not pass_through and len(text) >= 2:
				if str(text[:2]) == header:
					pass_through = True
			buf.write(text)
	if pass_through:
		# log("Detected an already compressed stream, falling into pass-through mode.")
		yield buf.getvalue()
	else:
		yield gzip(buf.getvalue(), cache_file=cache_file)

def generate_deflate(input, cache_file=None):
	# TODO: pass-through mode
	buf = cStringIO.StringIO()
	for text in flatten(input):
		if text not in (None, ''):
			buf.write(text)
	yield deflate(buf.getvalue(), cache_file=cache_file)


def __disabled__generate_gzip(input, cache_file=None):
	""" Returns a generator that reads from the supplied generator,
	and yields it's output, compressed.
	"""
	# log("generating gzip (cache_file: %s)" % cache_file)
	if cache_file is not None and os.path.isfile(cache_file):
		# log("found existing cache file: os.path.isfile('%s') == %s" % (cache_file, os.path.isfile(cache_file)))
		yield generate_file(cache_file)
	else:
		# log("no cache file, generating a stream, teeing the output to a cachefile at: %s" % cache_file)
		co = zlib.compressobj()
		# magic, type, flags + mtime + xfl, os
		header = "\037\213\010\000"+"\000\000\000\000"+ "\002\377"
		sent_header = False
		# the compress module produces 2 extra bytes in the header that are not supported by browsers
		skim_first_bytes = True
		# keep track of the checksum of the data we send out
		checksum = 0
		len_data = 0
		# if this stream is already compressed we will fall into pass_through mode
		pass_through = False
		cache_file = open(cache_file, "wb") if cache_file is not None else None
		try:
			for text in flatten(input):
				if text is not None and len(text) > 0:
					if pass_through:
						yield text
					else:
						if len(text) >= 4 and text[:4] == header[:4]: # detect a compression header
							# log("Detected already compressed stream, flipping to pass trhough mode.")
							pass_through = True
							yield text
							continue
						# keep a rolling checksum of all data sent
						checksum = zlib.crc32(text, checksum)
						len_data += len(text)
						ztext = co.compress(text)
						if len(ztext):
							if not sent_header:
								yield _tee(cache_file, header)
								sent_header = True
							if skim_first_bytes:
								skim_first_bytes = False
								yield _tee(cache_file, ztext[2:])
							else:
								yield _tee(cache_file, ztext)
			yield _tee(cache_file, co.flush())
			yield _tee(cache_file, struct.pack("<l",checksum))
		finally:
			if cache_file is not None:
				cache_file.close()

def _tee(f, data):
	if f is not None:
		f.write(data)
	yield data

def deflate(data, cache_file=None):
	using_temp_file = False
	if cache_file is None:
		cache_file = os.path.sep.join([Settings.cacheDir,"tmp_"+hashlib.sha1(os.urandom(1024)).hexdigest()])
		using_temp_file = True
	if not os.path.isfile(cache_file):
		with open(cache_file, "wb") as fo:
			data = cStringIO.StringIO(zlib.compress(data)).read()
			# log("writing deflate file: %d bytes" % len(data))
			fo.write(data)
	else:
		with open(cache_file, "rb") as fi:
			data = fi.read()
	# log("deflate: read %d bytes" % len(data))
	if using_temp_file:
		os.remove(cache_file)
	return data

