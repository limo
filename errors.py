import datetime

class HTTPStatusError(Exception):
	def __init__(self, code, msg=""):
		self.status = "%s %s" % (str(code), str(msg))
		# no new lines allowed
		self.status = self.status.replace('\n','').replace('\r','')
		self.headers = [('Content-Type', 'text/plain')]
	def __str__(self):
		return self.status
	def __repr__(self):
		return self.status

class FileNotFoundError(Exception):
	def __init__(self, file):
		self.file = file
	def __str__(self):
		return "File Not Found: %s" % self.file
	def __repr__(self):
		return str(self)

class HTTPRedirect(HTTPStatusError):
	def __init__(self, url=None, headers=None, status="303 Other"):
		self.status = status
		self.headers = [('Content-Type','text/plain')]
		if url is not None:
			# do not allow newlines in the url, to prevent header injection
			url = url.replace('\n','').replace('\r', '')
			self.headers = self.headers + [('Location',url),]
		if headers is not None:
			self.headers = self.headers + headers

class DeleteCookie(HTTPRedirect):
	def __init__(self, name):
		expires = "%s=; expires=%s" % (name, (datetime.datetime.utcnow()-datetime.timedelta(days=1)).strftime("%a, %d-%b-%Y %H:%M:%S %Z GMT"))
		HTTPRedirect.__init__(self, headers=[('Set-Cookie',expires)])


class NoSuchSessionException(Exception):
	pass

class LoginError(Exception):
	pass
