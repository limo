
"""
A FastCGI Responder implementation by jesse.dailey@gmail.com
- There are two kinds of packets in the FastCGI world
- Records are for passing between the library and the web server
- Requests are for passing data between the library and some web application
"""
__all__ = ['FastCGIServer', ]
import threading, sys, os, socket, select, struct, cStringIO, thread, datetime
from Queue import Queue
from fcgispec import *
from fcgiutil import *

# this queue will hold FastCGI Records as they come in off the wire
InputQueue = Queue()
# once enough records have come in to create a valid request
# that request is queued here
RequestQueue = Queue()
# once workers take up a request, and start generating output
# they put their output here to be written
OutputQueue = Queue()

_log_traffic = False

class Record(Loggable):
	""" A FastCGI Record.
	Exchanged between us and the webserver.
	"""
	def __init__(self, version=FCGI_VERSION_1, type=FCGI_UNKNOWN_TYPE, reqid=FCGI_NULL_REQUEST_ID, contentData=''):
		self.version = version
		self.type = type
		self.reqid = reqid
		self.contentData = str(contentData)
		self.contentLength = len(contentData)
		self.paddingLength = 0
	def __str__(self):
		data = repr(self.contentData)\
				if self.type in (FCGI_PARAMS, FCGI_STDIN, FCGI_STDOUT, FCGI_STDERR, FCGI_GET_VALUES, FCGI_GET_VALUES_RESULT) \
				else repr(struct.unpack(FCGI_EndRequestBody, self.contentData)) if self.type == FCGI_END_REQUEST\
				else repr(struct.unpack(FCGI_BeginRequestBody, self.contentData)) if self.type == FCGI_BEGIN_REQUEST\
				else repr(struct.unpack(FCGI_UnknownTypeBody, self.contentData)) if self.type == FCGI_UNKNOWN_TYPE else "''"
		return "{%s, %d, %s}" % (self.type, self.reqid, data)
	def read(self, sock):
		global _log_traffic
		try:
			d = sock.recv(FCGI_HEADER_LEN)
			if len(d) == 0:
				raise EOFError()
			(self.version, self.type, self.reqid, self.contentLength, self.paddingLength) \
				= struct.unpack(FCGI_Header, d)
			self.contentData = ''
			while len(self.contentData) < self.contentLength:
				d = sock.recv(self.contentLength - len(self.contentData))
				if len(d) == 0:
					raise EOFError("Recv'd only %d bytes out of %d expected" % (len(d), self.contentLength - len(self.contentData)))
				self.contentData += d
			if self.paddingLength:
				sock.recv(self.paddingLength)
			if _log_traffic:
				self.log("-> %s" % str(self))
		except EOFError:
			raise
		except socket.error:
			raise
		except Exception, e:
			if e[0] not in (4,10054,): # 'Interrupted system call','Connection reset by peer'
				raise
			pass
		return self
	def write(self, sock):
		global _log_traffic
		try:
			sent = 0
			self.paddingLength = -self.contentLength & 7
			assert self.reqid >= 0 and self.reqid <= 65535
			assert self.contentLength >= 0 and self.contentLength <= 65535, "You cannot write more than 64k at one time. Tried to write %d bytes." % self.contentLength
			header = struct.pack(FCGI_Header, self.version, self.type, self.reqid, self.contentLength, self.paddingLength)
			sock.send(header)
			sent += len(header)
			if self.contentLength:
				sent += sock.send(self.contentData)
			if self.paddingLength:
				sent += sock.send('\x00'*self.paddingLength)
			if _log_traffic:
				self.log("<-   %s" % str(self))
		except DeprecationWarning:
			self.log("DeprecationWarning: %s from (%d, %d, %d, %d, %d)" % (str(e), self.version, self.type, self.reqid, self.contentLength, self.paddingLength))
		except:
			raise
		return sent

class Request(Loggable):
	""" A FastCGI Request object, for holding data during the various stages of a request,
	and for generating Records in response to the Request. """
	def __init__(self):
		self._active = False
	def begin(self, sock, reqid, role, flags):
		self._active = True
		self.sock = sock
		self.reqid = reqid
		self.role = role
		self.flags = flags
		self.params = cStringIO.StringIO()
		self.stdin = cStringIO.StringIO()
		self.data = cStringIO.StringIO()
	def _params(self, text):
		assert self._active
		if text == '':
			p = self.params.getvalue()
			self.params = {}
			for pair in FastCGIPairs(p):
				self.params[pair[0]] = pair[1]
			return True
		else:
			self.params.write(text)
			return False
	def _stdin(self, text):
		if text != '':
			self.stdin.write(text)
			return False
		self.stdin.reset()
		return True
	def _data(self, text):
		assert self._active
		if text != '':
			self.data.write(text)
		else:
			self.data.reset()
	def end(self, appStatus=0,protocolStatus=FCGI_REQUEST_COMPLETE):
		if not self._active:
			return
		self._output(type=FCGI_STDOUT, reqid=self.reqid, contentData='')
		self._output(type=FCGI_STDERR, reqid=self.reqid, contentData='')
		self._output(type=FCGI_END_REQUEST, reqid=self.reqid, \
				contentData=struct.pack(FCGI_EndRequestBody, appStatus, protocolStatus ))
		self._active = False
	def write(self, data):
		assert self._active
		for i in range(0,len(data),FCGI_MAX_WRITE):
			x = min(FCGI_MAX_WRITE, len(data) - i)
			self._output(type=FCGI_STDOUT, reqid=self.reqid, contentData=data[i:i+x])
	def error(self, text):
		if not self._active:
			return
		self._output(type=FCGI_STDERR, reqid=self.reqid, contentData=text)
		self._active = False
	def abort(self):
		assert self._active
		self._output(type=FCGI_END_REQUEST, reqid=self.reqid, \
				contentData=struct.pack(FCGI_EndRequestBody, 911, FCGI_REQUEST_COMPLETE))
		self._active = False
	def _output(self, **kw):
		assert self._active
		rec = Record(**kw)
		# self.log("Queueing output: %s" % str(rec))
		OutputQueue.put( (self.sock, rec) )

class RecordDispatcher(NiceThread, Loggable):
	""" This reads Records off an input queue.
	If the record belongs to a partial request, it fills in the pieces.
	If the partial request is now ready, it promotes it to a request queue.
	"""
	def __init__(self, input, output, requestQueue):
		NiceThread.__init__(self)
		self.input = input
		self.output = output
		self.requestQueue = requestQueue
		self.requests = {}
	def thread(self):
		sock, rec = self.input.get()
		if rec.type == FCGI_BEGIN_REQUEST:
			self.requests[rec.reqid] = Request()
			role, flags = struct.unpack(FCGI_BeginRequestBody, rec.contentData)
			self.requests[rec.reqid].begin(sock, rec.reqid, role, flags)
		elif rec.type == FCGI_ABORT_REQUEST:
			self.requests[rec.reqid].abort()
		elif rec.type == FCGI_PARAMS:
			self.requests[rec.reqid]._params(rec.contentData)
		elif rec.type == FCGI_STDIN:
			if self.requests[rec.reqid]._stdin(rec.contentData): # when a request is complete enough to begin
				self.requestQueue.put( self.requests[rec.reqid] ) # add it to the request queue
		elif rec.type == FCGI_DATA:
			self.requests[rec.reqid]._data(rec.contentData)
		elif rec.type == FCGI_GET_VALUES:
			pairs = FastCGIPairs()
			pairs.add( ('FCGI_MAX_CONNS',10000),
					('FCGI_MAX_REQS',10000),
					('FCGI_MPXS_CONNS',1)
					)
			self.log("FCGI_GET_VALUES: -> %s" % repr(pairs.encode()))
			self.output.put( (sock, Record(type=FCGI_GET_VALUES_RESULT, reqid=rec.reqid, \
					contentData=pairs.encode())))
		else:
			self.output.put( (sock, Record(type=FCGI_UNKNOWN_TYPE, reqid=rec.reqid, \
					contentData=struct.pack(FCGI_UnknownTypeBody, rec.type))))
			self.log("UNKNOWN TYPE: %s" % rec.type)
		del rec
		self.input.task_done()

class RequestConsumer(NiceThread, Loggable):
	""" RequestConsumer watches the RequestQueue for complete Request objects.
	When they arrive, it spawns a thread to pump the content out of the handler,
	and write it out over the wire.
	"""
	_workers = 0
	def __init__(self, queue, handler):
		NiceThread.__init__(self)
		self.queue = queue
		self.handler = handler

	def thread(self):
		def process(queue,req, handler):
			status = 0
			try:
				for text in flatten(handler(req)): # handle them
					if text is not None and len(text) > 0 and text != "\n":
						req.write(text) # write/stream all the text back
			except AssertionError:
				self.log("Assertion Error on request id %s" % req.reqid)
				raise
			except Exception, e:
				try:
					req.error("Application Error: %s "%str(e)) # write any errors out to the server's fastcgi error log
				except:
					pass
				raise
			req.end(appStatus=status)
			queue.task_done() # mark a task complete
			RequestConsumer._workers -= 1
			self.log("Request %d finished. Queues: (input: %d output: %d request: %d active: %d readers: %d)" \
					% (req.reqid, InputQueue.qsize(), OutputQueue.qsize(), RequestQueue.qsize(), RequestConsumer._workers, RecordReader._count))
		req = self.queue.get() # block until a new request is ready in the queue
		try:
			th = threading.Thread(target=process, args=(self.queue,req,self.handler,))
			th.setDaemon(True)
			RequestConsumer._workers += 1
			th.start() # then start a thread to process it
		except threading.error:
			# retry once
			th = threading.Thread(target=process, args=(self.queue,req,self.handler,))
			th.setDaemon(True)
			RequestConsumer._workers += 1
			th.start() # then start a thread to process it

class RecordReader(NiceThread, Loggable):
	""" Sits on an open tcp socket, reading FCGI Records and queueing them someplace. """
	_count = 0
	def __init__(self, client_socket, queue):
		NiceThread.__init__(self)
		self.sock = client_socket
		self.queue = queue
		self.start_time = datetime.datetime.now()
		RecordReader._count += 1
	def thread(self):
		try:
			rec = Record().read(self.sock)
			self.queue.put((self.sock, rec))
		except EOFError:
			self.please_stop()
		except socket.error, e:
			if e[0] not in (10053, 10054,):
				raise
			self.please_stop()
		except Exception, e:
			self.log("Exiting unexpectedly due to error: %s %s" % (str(type(e)),str(e)))
			self.please_stop()
	def onexit(self):
		RecordReader._count -= 1

class RecordWriter(NiceThread, Loggable):
	""" Sits on a queue, writing new arrivals out to an open tcp socket. """
	def __init__(self, queue):
		NiceThread.__init__(self)
		self.queue = queue
		self.started = datetime.datetime.now()
	def thread(self):
		sock, r = self.queue.get()
		try:
			r.write(sock)
		except socket.error, e:
			if e[0] not in (10053,10054):
				raise
			# self.log("Ignoring error: %s discarding: %s" % (str(e),str(r)))
			pass

		del r
		self.queue.task_done()

class FastCGIServer(Loggable):
	""" A FastCGIServer spawns 2 main threads:
	1- A dispatcher that FastCGI and handles getting them processed by an application
	2- A writer that pushes the output queue onto a network socket
	n- Then each new client socket opened by the web server will spawn a new
	reader that pulls bytes off a network socket and turns them into FastCGI records for the dispatcher.

	This design allows for multiple connections, multiplexing, and streaming of large files.

	To take advantage of this, the application is expected to return a generator.
	Or, even better, nested generators (generators that sometimes return other generators).

	log_traffic will log every packet back and forth between us and the web server.
	This settings affects all active instances of FastCGIServer at the same time
	(so, the last time you call run(), the log_traffic flag will apply to all other running instances)
	"""
	def __init__(self, bindAddress=('localhost',7143)):
		self.bindAddress = bindAddress
		self._run = True
	def run(self, handler, timeout=1.0, log_traffic=False):
		global _log_traffic
		_log_traffic = log_traffic
		self.log("Binding socket: %s" % str(self.bindAddress))
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.sock.bind(self.bindAddress)
		self.sock.listen(socket.SOMAXCONN)
		self.log("Starting writer thread...")
		writer = RecordWriter(OutputQueue)
		writer.start()
		self.log("Starting dispatcher...")
		dispatcher = RecordDispatcher(InputQueue,OutputQueue,RequestQueue)
		dispatcher.start()
		self.log("Starting consumer...")
		worker = RequestConsumer(RequestQueue, handler)
		worker.start()
		_interrupt = False

		try:
			while self._run:
				try:
					r, w, e = select.select([self.sock], [], [], timeout)
				except select.error, e:
					if e[0] in (errno.EAGAIN, errno.EINTR):
						continue
					raise

				if r and len(r) > 0:
					try:
						sock, addr, = self.sock.accept()
					except socket.error, e:
						if e[0] in (errno.EAGAIN, errno.EINTR):
							continue
						raise
					reader = RecordReader(sock, InputQueue)
					reader.start()
				else:
					# self.log("Idle.")
					pass
		except KeyboardInterrupt:
			_interrupt = True
			pass

		self.log("Shutting down socket")
		try:
			self.sock.shutdown(socket.SHUT_WR)
			self.log("Closing socket")
			self.sock.close()
		except socket.error, e:
			if e[0] not in (128,10057):
				raise
			pass
		self.log("Asking workers to leave")
		# fire the labor
		worker.please_stop()
		writer.please_stop()
		# then the manager
		dispatcher.please_stop()
		self.log("Shutting off the lights and locking the doors.")
		return False # return True to restart

if __name__ == "__main__":
	def test_app(req):
		import random, time
		yield "Status: 200 OK\r\n"
		yield "Content-Type: text/html\r\n"
		yield "\r\n"
		yield "Hello World<br>"
		time.sleep(.5)
		def stuff():
			for x in (req.params, req.stdin, req.data):
				yield "STUFF: %s<br>" % str(x)
		yield stuff()
		yield """<form action='/' method='post' enctype="multipart/form-data">
			<input type=hidden name='foo' value='bar'>
			<input type=file name="file">
			<input type='submit'></form>"""
	FastCGIServer().run(test_app)
