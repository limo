
FCGI_LISTENSOCK_FILENO = 0

# typedef struct {
#   unsigned char version; -> B
#    unsigned char type; -> B
#    unsigned char requestIdB1; -> H
#    unsigned char requestIdB0;
#    unsigned char contentLengthB1; -> H
#    unsigned char contentLengthB0;
#    unsigned char paddingLength; -> B
#    unsigned char reserved;
#}
FCGI_Header = "!BBHHBx"

# While not part of the official spec, the definition of the header implies it
# since contentLength is only 2 bytes, no record can describe more than that much data
FCGI_MAX_WRITE = 65535

FCGI_HEADER_LEN =  8

FCGI_VERSION_1 =           1

FCGI_BEGIN_REQUEST =       1
FCGI_ABORT_REQUEST =       2
FCGI_END_REQUEST =         3
FCGI_PARAMS =              4
FCGI_STDIN =               5
FCGI_STDOUT =              6
FCGI_STDERR =              7
FCGI_DATA =                8
FCGI_GET_VALUES =          9
FCGI_GET_VALUES_RESULT =  10
FCGI_UNKNOWN_TYPE =       11
FCGI_MAXTYPE = (FCGI_UNKNOWN_TYPE)

FCGI_NULL_REQUEST_ID =     0

#typedef struct {
#    unsigned char roleB1;
#    unsigned char roleB0;
#    unsigned char flags;
#    unsigned char reserved[5];
#}
FCGI_BeginRequestBody = "!Hbxxxxx"

#typedef struct {
    #FCGI_Header header;
    #FCGI_BeginRequestBody body;
#}
FCGI_BeginRequestRecord = FCGI_Header + FCGI_BeginRequestBody


FCGI_KEEP_CONN =  1

FCGI_RESPONDER =  1
FCGI_AUTHORIZER = 2
FCGI_FILTER =     3

#typedef struct {
    #unsigned char appStatusB3;
    #unsigned char appStatusB2;
    #unsigned char appStatusB1;
    #unsigned char appStatusB0;
    #unsigned char protocolStatus;
    #unsigned char reserved[3];
#}
FCGI_EndRequestBody = "!LBxxx"

# typedef struct {
    # FCGI_Header header;
    # FCGI_EndRequestBody body;
# }
FCGI_EndRequestRecord = FCGI_Header + FCGI_EndRequestBody

FCGI_REQUEST_COMPLETE = 0
FCGI_CANT_MPX_CONN =    1
FCGI_OVERLOADED =       2
FCGI_UNKNOWN_ROLE =     3

FCGI_MAX_CONNS =  "FCGI_MAX_CONNS"
FCGI_MAX_REQS =   "FCGI_MAX_REQS"
FCGI_MPXS_CONNS = "FCGI_MPXS_CONNS"

# typedef struct {
    # unsigned char type;
    # unsigned char reserved[7];
# }
FCGI_UnknownTypeBody = "!Bxxxxxxx"

#typedef struct {
    #FCGI_Header header;
    #FCGI_UnknownTypeBody body;
#}
FCGI_UnknownTypeRecord = FCGI_Header + FCGI_UnknownTypeBody

