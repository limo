from __future__ import with_statement
import datetime, thread, threading, struct, os, sys
import cgitb

def log(msg):
	with open("fcgi.log","a") as f:
		f.write(msg+"\n")

class Loggable():
	def log(self, msg):
		log("%s %s %s: %s" % (str(datetime.datetime.now()), pad(thread.get_ident(),5), self.__class__.__name__, msg))

def ellipsis(text,maxlen,endsize=7):
	""" ellipsis is a helper function
		It grabs a little off both ends and puts an ellipsis in the middle.

		>>> ellipsis("1234567890_2_4_6_8_01_3_5_7_9_", 20)
		1234567890..._5_7_9_
		>>> ellipsis("1234567890", 8, 2)
		123...90
		>>> len(ellipsis("1234567890", 8, 2))
		8

	"""
	if type(text) not in (str,unicode):
		text = str(text)
	assert type(text) in (str, unicode)
	if len(text) <= maxlen:
		return text
	if endsize == 0:
		return text[0:maxlen-3]+"..."
	return text[0:maxlen-(endsize+3)]+"..."+text[-endsize:]

def pad(s,min,char=' '):
	s = str(s)
	if len(s) < min:
		return s + (char * (min - len(s)))
	return s
def ms_elapsed(since):
	d = (datetime.datetime.now() - since)
	return (d.seconds *1000.0) + (d.microseconds / 1000.0)

class NiceThread(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		self.setDaemon(True)
		self._run = True
	def please_stop(self):
		self._run = False
	def run(self):
		while self._run:
			self.thread()
		self.onexit()
	def onexit(self):
		pass # override this in a subclass
	def thread(self):
		pass # override this in a subclass

class FastCGIPairs(Loggable):
	""" FastCGI uses a unique pair encoding:
		<1-byte length><1-byte length><name><value>
	"""
	def __init__(self, s):
		self.pairs = []
		self._i = -1 # this is the counter if this object is used as an iterator
		while len(s):
			# decode one pair
			# dont forget to follow logic from the spec: Lengths of 127 bytes and less can be encoded in one byte, while longer lengths are always encoded in four bytes
			name_len = struct.unpack("!B",s[0])[0]
			if name_len >> 7 == 1:
				name_len = struct.unpack("!L",s[0:4])[0]
				name_len &= 0x7FFFFF
				s = s[4:]
			else:
				s = s[1:]

			value_len = struct.unpack("!B",s[0])[0]
			if value_len >> 7 == 1: # if the 4-byte marker is set
				value_len = struct.unpack("!L", s[0:4])[0] # then get its long value
				value_len &= 0x7FFFFF # clear the marker bit
				s = s[4:]
			else:
				s = s[1:]
			name = s[0:name_len]
			s = s[name_len:]
			value = s[0:value_len]
			s = s[value_len:]
			self.pairs.append( (name,value) )
	def add(self, pair):
		self.pairs.append(pair)
	def encode(self):
		ret = ''
		for pair in pairs:
			a = len(pair[0])
			b = len(pair[1])
			if len(a) > 127:
				pa = "L"
			else:
				pa = "B"
			if len(b) > 127:
				pb = "L"
			else:
				pb = "B"
			ret += struct.pack("!"+pa+pb, a, b)
			ret += pair[0]
			ret += pair[1]
		return ret
	def __iter__(self):
		return self
	def next(self):
		self._i += 1
		if self._i >= len(self.pairs):
			self._i = -1
			raise StopIteration()
		return self.pairs[self._i]

def flatten(gen):
	""" Returns a generator that iterates depth-first through 'gen', a potentially nested generator.

		All the generators throughout the flatten() process are allowed to yield lists, tuples, simple values, or other generators.

		Simples values flatten() to str() of themselves.

		The other value types (list,tuple,generator) will recurse and flatten their contents.

		>>> def foo():
		...		for c in ('f','o','o'):
		...			yield c

		>>> def bar():
		...		for c in ('b','a','r'):
		...			yield c

		>>> def gen():
		...		yield [foo(),bar()]

		>>> list(flatten(gen))
		['f', 'o', 'o', 'b', 'a', 'r']

		>>> def bad():
		...		yield 1
		...		yield 1/0

		>>> def gen():
		...		yield [foo(), bad()]

		>>> list(flatten(gen()))
		Traceback (most recent call last):
			...
		GeneratorException: Caused by:
		<type 'exceptions.ZeroDivisionError'> integer division or modulo by zero
		Stack:
		('generator', ['gen', '<doctest __main__.flatten[5]>', 1]):
			('list',1):
				('generator', ['bad', '<doctest __main__.flatten[4]>', 1]):
		<BLANKLINE>

	"""
	try:
		stack = '(not-yet-defined)'
		if type(gen) in (str, unicode):
			yield gen
		elif type(gen).__name__ == 'function':
			stack = ('function',gen.__name__)
			for i in flatten(gen()):
				yield i
		elif type(gen) in (list,tuple):
			for i in range(len(gen)):
				stack = ('list',i)
				for j in flatten(gen[i]):
					yield j
		elif type(gen).__name__ == 'generator':
			stack = ('generator', [gen.gi_frame.f_code.co_name, gen.gi_frame.f_code.co_filename.split(os.path.sep)[-1], gen.gi_frame.f_lineno])
			for i in gen:
				for j in flatten(i):
					yield j
		else:
			if gen is not None:
				yield str(gen)
	except Exception:
		(a,b,c) = sys.exc_info()
		raise GeneratorException(b, c, stack)

class GeneratorException(Exception):
	def __init__(self, error, tb=None, stack=[]):
		t = type(error)
		if hasattr(t, "__name__") and t.__name__ == 'GeneratorException':
			self.error = error.error
			self.stack = stack + (error.stack,)
			self.tb = error.tb
			del error
		else:
			self.error = error
			self.stack = stack
			self.tb = tb
	def __del__(self):
		try:
			del self.error
		except:
			pass
		try:
			del self.tb
		except:
			pass
	def __str__(self):
		def unroll_stack(stack,n=0):
			if len(stack) > 0:
				return ("\t"*n) \
						+ str(stack[:2])+":\n" \
						+ (unroll_stack(stack[2],n+1) if len(stack) > 2 else '')
		return "Caused by:\n%s %s\nStack:\n%s\nTB:%s\n" \
				% (str(type(self.error)),
						str(self.error),
						unroll_stack(self.stack),
						self.tb)

if __name__ == '__main__':
	import doctest
	doctest.testmod()
