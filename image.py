import Image, ImageDraw, ImageFont

class ImageLab:
	""" ImageLab provides a virtual workbench, on which to build images.
			See: test()
			Wraps all of Image and ImageDraw, from PIL.
	"""

	def __init__(self, image=None):
		self._image = image
		self._history = [image]
	def test():
		im = ImageLab() \
			.new("RGBA", [300,500], (0,0,0,0)) \
			.fourcornerbox([0,0], (300,500), radius=10, fill="black", outline="grey") \
			.cropratio(1.0) \
			.text([40,20], "This is a test", font=ImageFont.truetype("fonts/trebuc.ttf", 28)) \
			.resize([200,200]) \
			.save("images/image-lab-test.png", "PNG") \
			.undo(2) \
			.save("images/image-lab-test2.png", "PNG") # will save a 300x500
		return im
	test = staticmethod(test)


	# creation methods
	def new(self, mode, size):                                   self._save(); self._image = Image.new(mode, size); return self;
	def new(self, mode, size, color):                            self._save(); self._image = Image.new(mode, size, color); return self;
	def open(self, infile):                                      self._save(); self._image = Image.open(infile); return self;
	def open(self, infile, mode):                                self._save(); self._image = Image.open(infile, mode); return self;
	def blend(self, image1, image2, alpha):                      self._save(); self._image = Image.blend(image1, image2, alpha); return self;
	def composite(self, image1, image2, mask):                   self._save(); self._image = Image.composite(image1, image2, mask); return self;
	def eval(self, image, function):                             self._save(); self._image = Image.eval(image, function); return self;
	def frombuffer(self, mode, size, data):                      self._save(); self._image = Image.frombuffer(mode, size, data); return self;
	def frombuffer(self, mode, size, data, decoder, parameters): self._save(); self._image = Image.frombuffer(mode, size, data, decoder, parameters); return self;
	def fromstring(self, mode, size, data):                      self._save(); self._image = Image.fromstring(mode, size, data); return self;
	def fromstring(self, mode, size, data, decoder, parameters): self._save(); self._image = Image.fromstring(mode, size, data, decoder, parameters); return self;
	def merge(self, mode, bands):                                self._save(); self._image = Image.merge(mode, bands); return self;

	# custom creation methods
	# def newfrommacro(self, mode, size, color, macro):

	def save(self, outfile, format):                  self._save(); self._image.save(outfile, format); return self;

	# history methods
	def undo(self, i=1):
		assert(i > 0);
		self._image = self._history[-i]
		del self._history[-i:len(self._history)]
		return self
	
	def _save(self):
		if self._image is not None:
			self._history.append(self._image.copy())
		else:
			self._history.append(None)

	# data methods
	def getbands(self):                               return self._image.getbands();
	def getbbox(self):                                return self._image.getbbox();
	def getcolors(self):                              return self._image.getcolors();
	def getcolors(self, maxcolors):                   return self._image.getcolors(maxcolors);
	def getdata(self):                                return self._image.getdata();
	def getextrema(self):                             return self._image.getextrema();
	def getpixel(self, xy):                           return self._image.getpixel(xy);
	def histogram(self):                              return self._image.histogram();
	def histogram(self, mask):                        return self._image.histogram(mask);
	def split(self):                                  return self._image.split();
	def tell(self):                                   return self._image.tell();
	def tobitmap(self):                               return self._image.tobitmap();
	def tostring(self):                               return self._image.tostring();
	def tostring(self, encoder, parameters):          return self._image.tostring(encoder, parameters);
	def textsize(self, string, options):              d = ImageDraw.Draw(self._image); ret = d.textsize(string, options); del d; return ret;

	# manipulation methods
	def convert(self, mode):                          self._save(); self._image = self._image.convert(mode); return self;
	def convert(self, mode, matrix):                  self._save(); self._image = self._image.convert(mode, matrix); return self;
	def copy(self):                                   self._save(); self._image = self._image.copy(); return self;
	def crop(self, box):                              self._save(); self._image = self._image.crop(box); return self;
	def cropratio(self, ratio):
		(w,h) = self._image.size
		return self.crop([0, 0, w, int(w*ratio)])

	def draft(self, mode, size):                      self._save(); self._image.draft(mode, size); return self;
	def filter(self, filter):                         self._save(); self._image = self._image.filter(filter); return self;
	def fromstring(self, data):                       self._save(); self._image.fromstring(data); return self;
	def fromstring(self, data, decoder, parameters):  self._save(); self._image.fromstring(data, decoder, parameters); return self;
	def load(self):                                   self._save(); self._image.load(); return self;
	def offset(self, xoffset, yoffset):               self._save(); self._image = self._image.offset(xoffset, yoffset); return self;
	def paste(self, image, box):                      self._save(); self._image.paste(image, box); return self;
	def paste(self, colour, box):                     self._save(); self._image.paste(colour, box); return self;
	def paste(self, image, box, mask):                self._save(); self._image.paste(image, box, mask); return self;
	def paste(self, colour, box, mask):               self._save(); self._image.paste(colour, box, mask); return self;
	def point(self, table):                           self._save(); self._image = self._image.point(table); return self;
	def point(self, function):                        self._save(); self._image = self._image.point(function); return self;
	def point(self, table, mode):                     self._save(); self._image = self._image.point(table, mode); return self;
	def point(self, function, mode):                  self._save(); self._image = self._image.point(function, mode); return self;
	def putalpha(self, band):                         self._save(); self._image.putalpha(band); return self;
	def putdata(self, data):                          self._save(); self._image.putdata(data); return self;
	def putdata(self, data, scale, offset):           self._save(); self._image.putdata(data, scale, offset); return self;
	def putpalette(self, sequence):                   self._save(); self._image.putpalette(sequence); return self;
	def putpixel(self, xy, colour):                   self._save(); self._image.putpixel(xy, colour); return self;
	def resize(self, size):                           self._save(); self._image = self._image.resize(size, Image.ANTIALIAS); return self;
	def rotate(self, angle, expand=False):            self._save(); self._image = self._image.rotate(angle, expand=expand); return self;
	def seek(self, frame):                            self._save(); self._image.seek(frame); return self;
	def show(self):                                   self._save(); self._image.show(); return self;
	def thumbnail(self, size):                        self._save(); self._image.thumbnail(size); return self;
	def thumbnail(self, size, filter):                self._save(); self._image.thumbnail(size, filter); return self;
	def transform(self, size, method, data):          self._save(); self._image = self._image.transform(size, method, data); return self;
	def transform(self, size, method, data, filter):  self._save(); self._image = self._image.transform(size, method, data, filter); return self;
	def transform(self, size, EXTENT, data):          self._save(); self._image = self._image.transform(size, EXTENT, data); return self;
	def transform(self, size, EXTENT, data, filter):  self._save(); self._image = self._image.transform(size, EXTENT, data, filter); return self;
	def transform(self, size, AFFINE, data):          self._save(); self._image = self._image.transform(size, AFFINE, data); return self;
	def transform(self, size, AFFINE, data, filter):  self._save(); self._image = self._image.transform(size, AFFINE, data, filter); return self;
	def transform(self, size, QUAD, data):            self._save(); self._image = self._image.transform(size, QUAD, data); return self;
	def transform(self, size, QUAD, data, filter):    self._save(); self._image = self._image.transform(size, QUAD, data, filter); return self;
	def transform(self, size, MESH, data):            self._save(); self._image = self._image.transform(size, MESH, data); return self;
	def transform(self, size, MESH, data, filter):    self._save(); self._image = self._image.transform(size, MESH, data, filter); return self;
	def transpose(self, method):                      self._save(); self._image = self._image.transpose(method); return self;
	def verify(self):                                 self._save(); self._image.verify(); return self;

	# drawing methods
	def arc(self, xy, start, end, outline="black"):                    self._save(); d = ImageDraw.Draw(self._image); d.arc(xy, start, end, outline=outline); del d; return self;
	def bitmap(self, xy, bitmap):                                      self._save(); d = ImageDraw.Draw(self._image); d.bitmap(xy, bitmap); del d; return self;
	def chord(self, xy, start, end, outline="black", fill="black"):    self._save(); d = ImageDraw.Draw(self._image); d.chord(xy, start, end, outline=outline, fill=fill); del d; return self;
	def ellipse(self, xy, outline="black", fill="black"):              self._save(); d = ImageDraw.Draw(self._image); d.ellipse(xy, outline=outline, fill=fill); del d; return self;
	def line(self, xy, fill="black", width=1):                         self._save(); d = ImageDraw.Draw(self._image); d.line(xy, fill=fill, width=width); del d; return self;
	def pieslice(self, xy, start, end, outline="black", fill="black"): self._save(); d = ImageDraw.Draw(self._image); d.pieslice(xy, start, end, outline=outline, fill=fill); del d; return self;
	def point(self, xy, fill="black" ):                                self._save(); d = ImageDraw.Draw(self._image); d.point(xy, fill=fill); del d; return self;
	def polygon(self, xy, outline="black", fill="black"):              self._save(); d = ImageDraw.Draw(self._image); d.polygon(xy, outline=outline, fill=fill); del d; return self;
	def rectangle(self, box,outline="black", fill="black"):            self._save(); d = ImageDraw.Draw(self._image); d.rectangle(box, outline=outline, fill=fill); del d; return self;
	def text(self, position, string, font=None):
		if font is None:
			font = ImageFont.truetype("fonts/arial.ttf", 12)
		self._save()
		d = ImageDraw.Draw(self._image)
		if font:
			d.text(position, string, font=font);
		else:
			d.text(position, string)
		del d; 
		return self;
	
	# custom drawing methods
	def corner(self, center, corner=0, radius=5, fill="black", outline="black"):
		return self.pieslice([center[0]-radius, center[1]-radius, center[0]+radius, center[1]+radius], corner*90, (corner*90)+90, fill=fill, outline=outline)

	def circle(self, origin, radius, fill="black", outline="black"):
		return self.fourcornerbox([origin[0]-radius, origin[1]-radius]\
				, [radius*2,radius*2]\
				, radius=radius, fill=fill, outline=outline)

	def fourcornerbox(self, origin, size, radius=5, fill="black", outline="black"):
		o = origin
		(w,h) = size
		w -= 1
		h -= 1
		r = radius
		self._save()
		mark = len(self._history)
		ret = self\
			.corner([o[0]+r, o[1]+r], 2, r, fill=fill, outline=outline) \
			.corner([o[0]+w-r, o[1]+r], 3, r, fill=fill, outline=outline) \
			.corner([o[0]+w-r, o[1]+h-r], 0, r, fill=fill, outline=outline) \
			.corner([o[0]+r, o[1]+h-r], 1, r, fill=fill, outline=outline) \
			.rectangle([o[0],o[1]+r,o[0]+w,o[1]+h-r], fill=fill, outline=fill) \
			.rectangle([o[0]+r,o[1],o[0]+w-r,o[1]+h], fill=fill, outline=fill) \
			.line([o[0]+r, o[1], o[0]+w-r, o[1]], fill=outline, width=1) \
			.line([o[0], o[1]+r, o[0], o[1]+h-r], fill=outline, width=1) \
			.line([o[0]+r, o[1]+h, o[0]+w-r, o[1]+h], fill=outline, width=1) \
			.line([o[0]+w, o[1]+r, o[0]+w, o[1]+h-r], fill=outline, width=1)
		# hide the history for these commands, so that undo will skip back over a fourcorner box in one call
		del self._history[mark:len(self._history)]
		return self

