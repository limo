#!/usr/bin/env python

__all__ = []

if __name__ == "__main__":
	import os, sys
	sys.path.append(os.path.sep.join(__file__.split(os.path.sep)[:-1]))
	from settings import Settings
	from limoutil import *
	from optparse import OptionParser
	from server import LimoServer

	parser = OptionParser()
	parser.add_option("-c","--children", help="How many child processes to spawn", default=0)
	parser.add_option("-p","--port",help="Listen on tcp port", default=0)
	parser.add_option("-s","--socket",help="Listen on unix socket", default=0)
	(options, args) = parser.parse_args()
	options.children = int(options.children)
	if options.children == 0:
		options.children = int( os.environ.get('LIMO_MAX_PROCS',0) )
	options.port = int(options.port)
	options.socket = str(options.socket)
	if options.port != 0:
		s = ('localhost',options.port)
	elif options.socket != "0":
		s = options.socket
	else:
		s = Settings.defaultSocket

	try:
		log("Server starting on socket: %s" % str(s))
		if options.children > 0:
			LimoServer().run(socket=s,count=options.children)
		else:
			LimoServer().run(socket=s)
	finally:
		log("Server is gone.")

