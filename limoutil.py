from __future__ import with_statement
import os, datetime, thread, re, types, sys
from settings import Settings
_pid = "[PID: %d] " % os.getpid()
_pid = _pid + " " * (12 - len(_pid))
def log(msg):
	if Settings.disableAllLogging:
		return
	assert type(msg) == str or type(msg) == unicode
	if msg[-1] <> "\n":
		msg += "\n"
	with open(os.path.sep.join( __file__.split(os.path.sep)[:-1] + ["limo.log"] ), "a") as f:
		f.write(_pid + msg)

_profiles = {}
def profile(name):
	if Settings.profileServer:
		if( isinstance(name, types.FunctionType) ):
			# if the first arg is a function,
			# we are called as a decorator,
			# so add some named profile calls around the function
			def new(*args,**kw):
				profile('function:'+name.__name__)
				ret = name(*args,**kw)
				profile('function:'+name.__name__)
				return ret
			return new
		# else, we are called in the old-fashioned way
		id = thread.get_ident() # profiling is stored per-thread
		if _profiles.get(id, False) is False:
			_profiles[id] = {}
		if _profiles[id].get(name, False) is False:
			_profiles[id][name] = datetime.datetime.now()
		else:
			ret = ms_elapsed(_profiles[id][name])
			del _profiles[id][name]
			return ret
	return None

class Loggable(object):
	def log(self, msg):
		log("%s: %s" % (self.__class__.__name__, msg))

def ms_elapsed(since):
	d = (datetime.datetime.now() - since)
	return (d.seconds *1000.0) + (d.microseconds / 1000.0)

def htmlescape(s):
	assert type(s) in (str, unicode), "urlescape() only works on strings"
	return s.replace("<","&lt;").replace(">","&gt;")

def ellipsis(text, maxlen, endsize=7):
	""" ellipsis is a helper function provided to all templates.
		It grabs a little off both ends and puts an ellipsis in the middle.

		>>> ellipsis("1234567890_2_4_6_8_01_3_5_7_9_", 20)
		1234567890..._5_7_9_
		>>> ellipsis("1234567890", 8, 2)
		123...90
		>>> len(ellipsis("1234567890", 8, 2))
		8

	"""
	if type(text) not in (str,unicode):
		text = str(text)
	assert type(text) in (str, unicode)
	if len(text) <= maxlen:
		return text
	if endsize == 0:
		return text[0:maxlen-3]+"..."
	return text[0:maxlen-(endsize+3)]+"..."+text[-endsize:]

def pad(text, minlen, fillchar=' '):
	return text + fillchar * (minlen - len(text))

def urlencode(s):
	assert type(s) == str, "urlencode() only works on strings"
	ret = cStringIO.StringIO()
	for c in s:
		if not c.isalnum():
			ret.write("%%%s" % hex(ord(c))[2:])
		else:
			ret.write(c)
	return ret.getvalue()

def urldecode(text):
	""" urldecode is a helper function provided to all templates.
	>>> urldecode("Text%21")
	Text!
	"""
	f = re.search(r"%([A-Za-z0-9]+)", text)
	if f is not None:
		pos = f.start(1)
		return urldecode(text[:pos-1] + chr(int(text[pos:pos+2], 16)) + text[pos+2:])
	else:
		return text.replace("+", " ")

def quoted(s):
	return '"' + '"\n\t+ "'.join(str(s).replace("\\","\\\\").replace('"','\\"').splitlines()) + '"'

def json(obj):
	if hasattr(obj, "__json__"):
		return obj.__json__()
	else:
		t = type(obj)
		if t in (str, unicode):
			return quoted(obj)
		elif t in (int,float,long):
			return str(obj)
		elif t in (types.GeneratorType, list, tuple):
			return '['+", ".join([json(x) for x in obj])+']'
		elif t == types.NoneType:
			return "null"
		elif t == dict:
			return '{'+', '.join(["%s: %s" % (k,json(v)) for k,v in obj.items()])+'}'
		elif classInheritsFrom(t, 'Exception'):
			import traceback
			(a,b,c) = sys.exc_info()
			tb = traceback.format_tb(c)
			del a,b,c
			return ";(function(){ var _e = new Error("+quoted(obj)+"); _e.stack = ["+','.join([quoted(x) for x in tb])+"]; throw _e; })();"

def classInheritsFrom(obj, classname):
	if not hasattr(obj, '__bases__'):
		return False
	for c in obj.__bases__:
		if c.__name__ == classname:
			return True
	for c in obj.__bases__:
		if classInheritsFrom(c, classname):
			return True
	return False

def classname(c):
	if hasattr(c, '__class__'):
		return c.__class__.__name__
	return None
