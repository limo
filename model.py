from db import Model, Query, Row, safe
from limoutil import *
from settings import Settings


class LimoModel(Model):
	def model(self):
		self\
		.table('pages')\
			.column('pid int primary key')\
			.column('url varchar(256) not null')\
			.column('name varchar(256) not null')\
			.column('active int(1) not null default 1')\
		.table('blocks')\
			.column('bid int primary key')\
			.column('name varchar(512) not null')\
			.column('active int(1) not null default 1')\
			.column("module varchar(32) null")\
		.table('page_blocks')\
			.column('pid int not null')\
			.column('bid int not null')\
			.column('region varchar(32) not null')\
			.column('weight int not null default 0')\
			.column('active int(1) not null default 1')\
			.column('primary key (pid, bid, region, weight)')\
			.column('index (pid)')\
			.column('index (bid)')\
			.column('index (region)')\
		.table("users")\
			.column("uid int primary key auto_increment")\
			.column("name varchar(64) not null")\
			.column("login varchar(32) not null")\
			.column("passwd varchar(40) not null")\
			.column("email varchar(128)")\
			.column("theme varchar(64) not null default 'default'")\
			.column("active int(1) not null default 1")\
		.table("groups")\
			.column("gid int primary key auto_increment")\
			.column("name text not null")\
		.table("roles")\
			.column("rid int primary key auto_increment")\
			.column("name varchar(64) not null")\
		.table("group_roles")\
			.column("gid int not null")\
			.column("rid int not null")\
			.column("primary key (gid, rid)")\
		.table("group_users")\
			.column("gid int not null")\
			.column("uid int not null")\
			.column("primary key (gid, uid)")\
		.table("user_roles")\
			.column("uid int not null")\
			.column("rid int not null")\
			.column("primary key (uid, rid)")\
		.table("sessions",options="ENGINE=InnoDB")\
			.column("sid int primary key auto_increment")\
			.column("sha varchar(40) not null")\
			.column("uid int not null default '0'")\
			.column("data text")\
			.column("unique index (sha)")\
			.column("index (sid, sha)")\
		.table("visits",options="ENGINE=InnoDB")\
			.column("vid int primary key auto_increment")\
			.column("entered timestamp default current_timestamp")\
			.column("sid int not null")\
			.column("url varchar(512) not null", version=1)\
			.column("remote varchar(21) not null")\
			.column("unique index (vid, entered, sid, remote, url(32))")\
		.table("modules")\
			.col("mid int not null primary key auto_increment")\
			.col("name varchar(32) not null")\
			.col("active int(1) not null default '1'")\
			.col("weight int not null default '1'")\
			.col("unique index (name)")\
		.defaultRole('admin')\
		.defaultRole('login')\
		.defaultRole('debug')\
		.defaultGroup('admin')\
		.assignRoleToGroup('admin', 'admin')\
		.assignRoleToGroup('debug', 'admin')\
		.defaultGroup('users')\
		.assignRoleToGroup('login', 'users')\
		.defaultUser('admin', 'admin', 'admin', 'admin@admin.com', 1)\
		.assignUserToGroup('admin','admin')\
		.assignUserToGroup('admin','users')\
		.defaultBlock('DebugBlock')\
		.defaultBlock('HeadBlock')\
		.defaultBlock('HeaderBlock')\
		.defaultBlock('AdminMenuBlock')\
		.defaultBlock('EmptyBlock')\
		.defaultPage('/','Home')\
			.attachBlockToRegion('/', 'head', 'HeadBlock')\
			.attachBlockToRegion('/', 'header', 'HeaderBlock')\
			.attachBlockToRegion('/', 'footer', 'DebugBlock')\
			.attachBlockToRegion('/', 'left', 'AdminMenuBlock')\
		.defaultBlock('StaticBlock')\
		.defaultPage('/cache', 'Static cache for dynamically generated content')\
			.attachBlockToRegion('/cache', 'entire', 'StaticBlock')\
		.defaultBlock('RegisterBlock')\
		.defaultPage('/register', 'Registration')\
			.attachBlockToRegion('/register', 'main', 'RegisterBlock')\
		.defaultBlock('SessionBlock')\
			.attachBlockToRegion('/', 'left', 'SessionBlock')\
		.defaultBlock('VisitBlock')\
		.defaultPage('/visits')\
			.attachBlockToRegion('/visits', 'main', 'VisitBlock')\
		.defaultBlock('ModuleBlock')\
		.defaultPage('/modules')\
			.attachBlockToRegion('/modules', 'main', 'ModuleBlock')\

		return Model.model(self)

	def defaultPage(self, url, name=""):
		page_id = Model.int_uid(url)
		return self.table("pages").data([page_id, url, name])
	def deletePage(self, url):
		pid = Model.int_uid(url)
		return self.deletePageByID(pid)
	def deletePageByID(self, pageid):
		Query("delete from pages where pid = %d" % pageid)
		return self

	def defaultBlock(self, name, active=1):
		block_id = Model.int_uid(name)
		return self.table("blocks").data([block_id, name, active])
	def disableBlock(self, name):
		Query("update blocks set active = 0 where name = '%s'" % (safe(name)))
		return self
	def enableBlock(self, name):
		Query("update blocks set active = 1 where name = '%s'" % (safe(name)))
		return self

	def enableModule(self, name):
		Query("update modules set active = 1 where name = '%s'" % (safe(name)))
		return self
	def disableModule(self, name):
		Query("update modules set active = 0 where name = '%s'" % (safe(name)))
		return self

	def attachBlockToRegion(self, url, region, block_name, weight=0):
		page_id = Model.int_uid(url)
		block_id = Model.int_uid(block_name)
		return self.attachBlockToRegionByID(page_id, region, block_id, weight)
	def attachBlockToRegionByID(self, page_id, region, block_id, weight=0):
		return self.table("page_blocks").data([page_id, block_id, region, weight])

	def defaultRole(self, name):
		role_id = Model.int_uid(name)
		return self.table("roles").data([role_id, name])
	def defaultGroup(self, name):
		group_id = Model.int_uid(name)
		return self.table("groups").data([group_id, name])
	def defaultUser(self, name, login, passwd, email, active=1):
		user_id = Model.int_uid(login)
		passwd = Model.uid(passwd)
		return self.table("users").data([user_id, name, login, passwd, email, active])

	def assignRoleToGroup(self, role_name, group_name):
		role_id = Model.int_uid(role_name)
		group_id = Model.int_uid(group_name)
		return self.assignRoleToGroupByID(role_id, group_id)
	def assignRoleToGroupByID(self, role_id, group_id):
		return self.table("group_roles").data([group_id, role_id])

	def assignRoleToUser(self, role_name, user_name):
		role_id = Model.int_uid(role_name)
		user_id = Model.int_uid(user_name)
		return self.assignRoleToUserByID(role_id, user_id)
	def assignRoleToUserByID(self, role_id, user_id):
		return self.table("user_roles").data([user_id, role_id])

	def assignUserToGroup(self, login, group_name):
		user_id = Model.int_uid(login)
		group_id = Model.int_uid(group_name)
		return self.table("group_users").data([group_id, user_id])

	def getSiteMap(self):
		""" Returns a dict full of everything known about the site. """
		site = {}
		pages = Query(name="pageset", sql="""
			select p.pid, p.url, p.name as page_name, b.name as block_name, pb.region, pb.weight
			from pages p
			join page_blocks pb on pb.pid = p.pid
			join blocks b on b.bid = pb.bid
			where b.active = 1 and pb.active = 1
			order by pb.weight asc
			""")
		for p in pages.Query("select distinct url,pid from __self__ order by url"):
			site[p.url] = {}
			for r in pages.Query("select distinct region from pageset where pid = %s order by weight" % p.pid)['region']:
				b = pages.Query("select block_name from pageset where region = '%s' and pid = %s order by weight" % (r,p.pid))['block_name']
				site[p.url][r] = ','.join([unicode(v) for v in b])
				if Settings.debugSitemap:
					log("[ %s ] %s -> %s" % (p.url, r, b))
		return site
