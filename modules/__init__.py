import os, sys
from limoutil import *
from db import FastQuery, safe, Model
from settings import Settings
import modules.core
from theme import Theme
import datetime
from model import LimoModel

__all__ = ["core", "blocks", "init", "clear"]
blocks = {}
_mtimes = {}

def init(skipModels=False, olderThan=None):
	global blocks
	if Settings.logModuleLoading:
		log("Importing all modules:")

	LimoModel() # instantiate once to make sure db has been put in place

	# first grab all the system block from modules.core
	for block in dir(modules.core):
		type = eval("modules.core."+block)
		if classInheritsFrom(type, "LimoBlock"):
			blocks[block] = type
	for module_dir in os.listdir(os.path.sep.join(__file__.split(os.path.sep)[:-1])):
		# for everything in /modules/module_dir
		if os.path.isdir(os.path.sep.join(["modules",module_dir])):
			if Settings.logModuleLoading:
				log("\t%s" % module_dir)
			for d in os.listdir(os.path.sep.join(["modules",module_dir])):
				# save a copy of d (which right now is just the filename)
				n = d
				# make d into the fully qualified path
				d = os.path.sep.join(["modules",module_dir,d])
				# if d is a directory
				if os.path.isdir(d):
					# then this should be a module
					FastQuery("insert ignore into modules (name) values ('%s')" % safe(n))
					for row in FastQuery("select active, weight from modules where name = '%s'" % safe(n)):
						(active, weight) = row
						break
					# only if active (disabled by default)
					if int(active) == 0:
						if Settings.logModuleLoading:
							log("\t\t%s (ignored)" % n)
					else:
						profile("module_%s" % n)
						# search all subtrees of the module, and register all the files
						# the theme module will use these and the weights to find the 'right' file later on
						len_d = len(d.split(os.path.sep))
						for mf in os.walk(d):
							loc = mf[0]
							# mf[0] has the full path, remove the base from the front of it
							loc_offset = loc.replace(d, '')
							# mf[2] is a list of the files in this dir
							for f in mf[2]:
								if f[0] not in ("_",".") and f.split(".")[-1] not in ("pyc",): # skip some files
									# .py files are imported, and any Block's or Model's are harvested out of them
									if f.endswith(".py"):
										ff = os.path.sep.join((mf[0],f)) # ff is a full physical path to the .py file
										mt = os.path.getmtime(ff)
										if _mtimes.get(ff, 0.0) >= mt:
											# log('skipping')
											continue # skip files the we've already loaded that havent changed
										log('(re-)loading %s' % ff)
										_mtimes[ff] = mt
										# get the name of the module we are about to import
										m = f.replace(".py","")
										# import the module
										i = n+"_"+m # it will be imported and assigned to this name
										exec "%(i)s = __import__('modules.%(module_dir)s.%(n)s.%(m)s').%(module_dir)s.%(n)s.%(m)s" % locals()
										exec "reload(%(i)s)" % locals()
										for b in dir(eval(i)):
											# for every member defined in the module
											member_type = eval("%s.%s" % (i,b))
											# log("Found member type: %s" % str(member_type))
											# if that member is a Block
											if classInheritsFrom(member_type, 'LimoBlock'):
												# log("Loading Block member")
												# make sure that we have a record of the blocks existence
												bid = Model.int_uid(b)
												FastQuery("insert ignore into blocks (bid, name, module) values (%d, '%s', '%s')" % (bid, b, m))
												# and that the block is active
												for row in FastQuery("select active from blocks where bid = %d" % (bid,) ):
													(active,) = row
												if int(active) == 0:
													if Settings.logModuleLoading:
														log("Skipping disabled block %s." % (b,))
												else:
													# then bind the block globally so its available for rendering
													log("blocks['%s'] = modules.%s.%s.%s.%s" % (b,module_dir,n,m,b))
													exec "blocks['%s'] = modules.%s.%s.%s.%s" % (b,module_dir,n,m,b)
											elif not skipModels and classInheritsFrom(member_type, 'Model'):
												# log("Loading Model object: %s" % str(member_type))
												# or if the member is a Model
												member_type() # instantiate it once to init the model in the database
											elif Settings.developmentMode and classInheritsFrom(member_type, 'LimoTest'):
												# log("Loading Test...%s" % str(member_type))
												member_type() # instantiate it once to put it in the global list of tests: LimoTest.all()
												# on a production system, they never take up memory, and are thus never able to be run from the test console
									else:
										# all other files are registered with the theme, making them overridable later on
										Theme().provideFile( os.path.sep.join([loc_offset, f]), os.path.sep.join([loc, f]), weight)
						elapsed = profile("module_%s" % n)
						if Settings.logModuleLoading:
							log("\t\t%s (in %sms)" % (n, elapsed))

def clear():
	global blocks
	del blocks
	blocks = {}

