from modules.core import HTMLBlock, LimoModel
from db import Query, Model, safe
from template import Template, TemplateNotFoundError
from errors import *

class PageEditModel(LimoModel):
	def model(self):
		self\
			.defaultPage("/pages", "")\
			.defaultBlock("PageEditBlock")\
			.attachBlockToRegion("/pages", 'main', "PageEditBlock")\
			.defaultRole("edit pages")\
			.assignRoleToGroup("edit pages", "admin")\
		.execute()

	def removeBlockFromRegion(self, pid, bid, region):
		if pid and bid and region:
			Query("""delete from page_blocks where pid = %d, bid = %d, region = '%s'""" \
				% (int(pid), int(bid), safe(region)))
		return self

	def toggleBlockActive(self, bid):
		if bid:
			Query("""update blocks set active = mod((active + 1),2) where bid = %d""" % int(bid))
		else:
			raise HTTPStatusError(500, "Missing required arguments.")

class PageEditBlock(HTMLBlock):
	def action(self):
		if self.args.get('action',False):
			if self.args['action'] == 'add_block':
				pass
			elif self.args['action'] == 'remove_block':
				pass
			elif self.args['action'] == 'add_page':
				pass
			elif self.args['action'] == 'remove_page':
				pass

	def init(self):
		self.model = PageEditModel()
		self.requireJS('jquery.js')
		self.requireJS('block-pageedit.js')
		self.requireCSS('block-pageedit.css')

	def render(self, next=None):
		self.title = "All Pages"
		sitemap = self.model.getSiteMap()
		self.body = self.template("block-pageedit", {'sitemap': sitemap})
		yield HTMLBlock.render(self, next)
