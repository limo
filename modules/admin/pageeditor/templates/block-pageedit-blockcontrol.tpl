<div class="blockcontrol" pid="%(page.pid)s" region="%(block.region)s" bid="%(block.bid)s">
	<span class="move-up-block"><a href="#">Move Up</a></span>
	<span class="move-down-block"><a href="#">Move Down</a></span>
	<span class="remove-block"><a href="#">Remove</a></span>
	<span class="region">%(block.region)s:</span>
	<span class="name">%(block.block_name)s</span><span class="weight">(%(block.weight)s)</span>
	<div class="clearboth">&nbsp;</div>
</div>
