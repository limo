#!/usr/bin/env python

from __future__ import with_statement
import re, datetime, time, os, sys, gzip, math, compress, hashlib, httplib2
from cache import Cache
from template import Template
from db import Query
from errors import *
from session import User
from settings import Settings
from model import LimoModel
from theme import Theme
from limoutil import *

class LimoBlock(Loggable):
	""" LimoBlock is the very basic block object, it represents some block on content, and its associated actions.
		Most people will want to inherit from HTMLBlock, which adds several html specific capabilities.
	"""
	def __init__(self, request, response, session):
		self.id = self.__class__.__name__.lower().replace('block','')
		self.request = request
		self.response = response
		self.session = session
		self.url = [u for u in request["REQUEST_URI"].split("/") if u is not '']
		self.args = {}
		# build self.args from both the query_string and the post_data
		for p in request["QUERY_STRING"].split("&"):
			t = p.split("=")
			if len(t) == 2:
				self.args[t[0]] = t[1]
			else:
				self.args[p] = True
		for p in request["POST_DATA"].split("&"):
			t = p.split("=")
			if len(t) == 2:
				self.args[t[0]] = t[1]
			elif len(p) > 0:
				self.args[p] = True
	def init(self):
		"""
		this is not __init__()
		its not for object creation, its used as phase-1 of the rendering process
		do things like requireCSS, requireJS, set headers, cookies, etc. here
		"""
		pass
	def render(self, next=None):
		""" render() is a generator function that yields some block of content.
		If this Block is rendering as part of a pipeline of blocks,
		'next' will be a generator for getting the rest of the stuff from the pipe
		"""
		yield next
	def action(self):
		""" Sub-classes that define action() are expected to raise exceptions.
		HTTPRedirect, and HTTPStatusError exceptions will be written back to the browser,
		any other exception is considered an action failure and the exception is propagated.
		"""
		pass


class HTMLBlock(LimoBlock):
	def __init__(self, request, response, session):
		LimoBlock.__init__(self, request, response, session)
		self.title = ''
		self.body = None
		self.before = ''
		self.after = ''

	def init(self):
		self.requireCSS("main.css")

	def render(self, next=None):
		""" render() is a generator function that yields some block of content.
		If this Block is rendering as part of a pipeline of blocks,
		'next' will be a generator for getting all the stuff rendered before us in the pipe
		"""
		if self.body is not None:
			yield self.template("block", {'id': self.id, 'body': self.body, 'title': self.title, 'after': self.after, 'before': self.before })
		yield next

	def getFile(self, file,_theme=None):
		if _theme is None:
			_theme = self.session.data.get('theme','default')
		return Theme().getFile(file, _theme)

	def setPageTitle(self, title, append=True):
		""" Set some text for the page title. """
		HeadBlock.setTitle(title,append)

	def _getRemoteFile(self, file):
		""" file is a remote url, which will be fetched with httplib2, written to disk, and its file name returned. """
		fname = file.split("?")[0]
		fname = re.sub("[^A-Za-z0-9_]","_", fname)
		fname = os.path.sep.join((Settings.cacheDir,"_".join((fname, hashlib.sha1(file).hexdigest()))))
		if not os.path.isfile(fname):
			self.log("Downloading %s into %s" % (file,fname))
			(response, content) = httplib2.Http().request(file)
			with open(fname, "wb") as out:
				out.write(content)
		return fname

	def requireCSS(self, file):
		""" Add a CSS file to the aggreggator for this page. """
		if file[:6] in ("http:/","https:"):
			fname = self._getRemoteFile(file)
			Cache.CSS.defaultFile(fname)
		else:
			Cache.CSS.defaultFile(self.getFile(("css",file,)))

	def requireJS(self, file):
		""" Add a JS file to the aggreggator for this page. """
		if file[:6] in ("http:/","https:"):
			fname = self._getRemoteFile(file)
			Cache.JS.defaultFile(fname)
		else:
			Cache.JS.defaultFile(self.getFile(("js",file,)))

	def template(self, name, params):
		""" Render the named template using the specified params.
		"""
		if not name.endswith(".tpl"):
			name += ".tpl"
		yield Template.render(self.getFile(("templates",name,)), params)

	def meta(self, **kw):
		""" Add a <meta> tag to the <head>.
		All keyword arguments become attributes.
		"""
		HeadBlock.addMeta(**kw)

	def link(self, **kw):
		""" Add a <link> tag to the <head>.
		All keyword arguments become attributes.
		"""
		HeadBlock.addLink(**kw)

class TestBlockA(LimoBlock):
	def render(self, next=None):
		yield "rendered A: "
		yield LimoBlock.render(self, next)
class TestBlockB(LimoBlock):
	def render(self, next=None):
		yield "rendered B: "
		yield LimoBlock.render(self, next)
class TestBlockC(HTMLBlock):
	def render(self, next=None):
		self.title = "TestBlock C"
		self.body = next
		self.after = "after C"
		yield HTMLBlock.render(self, next)

class EmptyBlock(LimoBlock):
	def render(self, next=None):
		yield None

class SessionBlock(HTMLBlock):
	def action(self):
		# self.log("args = %s" % self.args)
		if self.args.get('action', False) == 'login':
			u = User.getUser(self.args.get('username',None), self.args.get('password',None))
			assert u, "Login failed: No such username/password"
			self.session.user = u
			assert self.session.hasRole('login'), "Account does not have login permission"
			self.session.uid = u.uid
			self.session.addMessage("%s, Thank you for logging in!" % u.name)
		elif self.args.get('action', False) == 'edit':
			uid = int(self.args.get('uid', 0))
			assert uid > 0, "No user"
			assert (self.session.uid == uid or self.session.hasRole('admin')), "Forbidden"
			user = User(uid)
			if self.args.get('newpass', False):
				user.setPassword(self.args['newpass'])
				self.session.addMessage("Password updated.")
		elif self.args.get('action', False) == 'logout':
			if self.session.user is not None:
				self.session.uid = 0
				self.session.user = None
			self.session.addMessage("Goodbye!")
	def render(self, next=None):
		if self.session.user is None:
			args = { 'username': '' }
			args.update(self.args)
			self.title = self.template("title-login", args)
			self.body = self.template("form-login", args)
		else:
			self.title = self.template("title-welcome", {'name': self.session.user.name})
			self.body = self.template("body-welcome", {'name': self.session.user.name})
		yield HTMLBlock.render(self, next)

class VisitBlock(HTMLBlock):
	def render(self, next=None):
		page = int(self.args.get('p',1))
		perpage = int(self.args.get('pp',10))
		rows = Query("select * from visits order by vid desc limit %d, %d" %((page-1)*perpage, perpage))
		pages = Query("select count(*) from visits")[0][0]
		pages = int(pages/perpage) + 1
		self.title = "Visits"
		self.body = self.template("block-visits", locals())
		return HTMLBlock.render(self, next)

class DebugBlock(HTMLBlock):
	def init(self):
		self.requireJS("jquery.js")
		self.requireJS("debug.js")
		self.requireCSS("debug.css")
	def render(self, next=None):
		if self.session.hasRole('admin'):
			self.title = "Debug Block"
			queries = [(sql,ms) for (sql,ms) in Query.getQueryLog()]
			Query.clearQueryLog()
			self.body = self.template("body-debug", \
					{ 'counts': Template.debug_counts, \
						'timings': Template.debug_timing, \
						'queries': queries, \
						'request': self.request, \
						'response': self.response, \
						'session': htmlescape(str(self.session)), \
					})
			# HACK: TODO replace this
			Template.debug_timing.clear()
			Template.debug_counts.clear()
			yield HTMLBlock.render(self, next)

class HeadBlock(HTMLBlock):
	tags = []
	title = ''
	@staticmethod
	def addMeta(**kw):
		HeadBlock.tags.append( ('meta', kw) )
	@staticmethod
	def addLink(**kw):
		HeadBlock.tags.append( ('link', kw) )
	@staticmethod
	def setTitle( text, append=True ):
		if append:
			HeadBlock.title += text
		else:
			HeadBlock.title = text

	def __del__(self):
		HeadBlock.title = ''
		HeadBlock.tags = []

	def render(self, next=None):
		if self.request.get('HTTPS','') == 'on':
			# dont bother downloading css or js over ssl? probably should have the option... to avoid a browser warning
			# TODO
			js_file = "http://"+self.request.get('HTTP_HOST','server')+"/"+Cache.JS.getOutputFile().replace(os.path.sep,"/")
			css_file = "http://"+self.request.get('HTTP_HOST','server')+"/"+Cache.CSS.getOutputFile().replace(os.path.sep,"/")
		else:
			js_file = "/"+Cache.JS.getOutputFile().replace(os.path.sep,"/")
			css_file = "/"+Cache.CSS.getOutputFile().replace(os.path.sep,"/")

		yield self.template("block-head", {
				'title': HeadBlock.title,
				'css_file':css_file,
				'js_file':js_file,
				'tags': ( ['<',tag[0]] + [' %s="%s"' % (k,v) for k,v in tag[1].items()] + [' />'] \
					for tag in HeadBlock.tags ),
				})

class StaticBlock(LimoBlock):
	""" Serves static files.
		Does file-cached compression along the way.
	"""
	# date format: Thu, 01 Dec 1994 16:00:00 GMT
	http_date_format = "%a, %d %b %Y %H:%M:%S"
	http_date_format_tz = "%a, %d %b %Y %H:%M:%S %Z"
	def __init__(self, request, response, session):
		LimoBlock.__init__(self, request, response, session)
		self.typemap = {
			'xml': 'text/xml',
				'png': 'image/png',
				'ico': 'image/png',
				'jpg': 'image/jpeg',
				'jpeg': 'image/jpeg',
				'gif': 'image/gif',
				'css': 'text/css',
				'js': 'text/javascript',
				'html': 'text/html',
				'htm': 'text/html'
		}

	def _setContentType(self, file):
		ext = file.split(".")
		if ext[-1] in ("gzip","deflate","gz"):
			ext = ext[-2]
		else:
			ext = ext[-1]
		try:
			type = self.typemap[ext]
			# set the content-type
			# self.log("Setting Content-Type: %s" % type)
			self.response.headers['Content-Type'] = type
		except KeyError:
			raise HTTPStatusError(404, "Cant determine content-type for file: %s" % file)

	def _setExpires(self, file):
		# set an expires date one year in the future
		self.response.headers['Expires'] = (datetime.datetime.utcnow() + datetime.timedelta(365)).strftime(self.http_date_format_tz)

	def _setLastModified(self, file):
		# set the last-modified time
		try:
			mtime = datetime.datetime.fromtimestamp(os.path.getmtime(file))
			mtime = mtime.replace(microsecond=0)
			# convert the mtime to utc
			mtime = mtime - datetime.timedelta(seconds=time.timezone)
			# self.log("Checking mtime of file: %s %s" % (file, mtime))
			if self.request.get('HTTP_IF_MODIFIED_SINCE',False):
				try:
					since = datetime.datetime.strptime(self.request['HTTP_IF_MODIFIED_SINCE'], self.http_date_format)
					# self.log("Checking If-Modified-Since: %s" % since)
				except ValueError:
					try:
						since = datetime.datetime.strptime(self.request['HTTP_IF_MODIFIED_SINCE'], self.http_date_format_tz)
						# self.log("Checking If-Modified-Since: %s" % since)
					except ValueError:
						raise HTTPStatusError(400, "Invalid Header If-Modified-Since: '%s' is not a proper date format." % (self.request['HTTP_IF_MODIFIED_SINCE']))
				# self.log("Comparing %s >= %s" % (since, mtime))
				if since >= mtime:
					raise HTTPStatusError(304, "Not Modified")
		except OSError, e:
			# always local time here because os.path.getmtime is always going to return local as well
			self.log("Ignoring OSError: %s" % str(e))
			mtime = datetime.datetime.utcnow()
		if Settings.supportEtags:
			etag = hashlib.sha1(str(mtime)).hexdigest()
			if self.request.get('HTTP_IF_NONE_MATCH','') == etag:
				raise HTTPStatusError(304, "Not Modified")
			self.response.headers['ETag'] = etag
		self.response.headers['Last-Modified'] = mtime.strftime(self.http_date_format_tz)

	def _setCompression(self, file):
		self.compression = Settings.getCompression(self.request.get('HTTP_USER_AGENT',''))
		if self.request.get('HTTP_ACCEPT_ENCODING','').find(self.compression) == -1:
			self.compression = None
			self.zfile = None
		elif self.response.headers.get('Content-Encoding',None) == self.compression:
			# if there is already a compression layer in place
			# just pass along the right cache file name
			self.zfile = self.file+"."+self.compression
			self.request['LIMO_CACHE_FILE'] = self.file
		else:
			self.response.headers['Content-Encoding'] = self.compression
			self.response.headers['Vary'] = 'Accept-Encoding'
		# log("core: Using compression: %s" % self.compression);

	def init(self):
		url = [u for u in self.url if u is not '']
		url[0] = Settings.cacheDir
		self.file = os.path.sep.join(url)
		self._setContentType(self.file)
		self._setExpires(self.file)
		self._setLastModified(self.file)
		self._setCompression(self.file)

	def render(self, next=None):
		try:
			if None not in (self.compression, self.zfile):
				yield compress.generate_gzip(compress.generate_file(self.file),cache_file=self.zfile)
			else:
				yield compress.generate_file(self.file)
		except OSError, e:
			if Settings.showVerbose404:
				raise HTTPStatusError(404, s + " -> OSError: %s" % str(e))
			else:
				raise HTTPStatusError(404, s)
		except IOError, e:
			if Settings.showVerbose404:
				raise HTTPStatusError(404, s + " -> IOError: %s" % str(e))
			else:
				raise HTTPStatusError(404, s)

class HeaderBlock(HTMLBlock):
	def render(self, next=None):
		self.title = ""
		self.body = self.template("header", locals())
		return HTMLBlock.render(self, next);

class RegisterBlock(HTMLBlock):
	def action(self):
		if self.args.get('action', False) == 'register':
			name = self.args.get('name', False)
			login = self.args.get('login', False)
			password = self.args.get('password', False)
			email = self.args.get('email', False)
			assert name and login and password and email, "Missing required fields"
			assert len(self.args['password']) > 4, "Your password was too short."
			assert self.args['password'] == self.args['password2'], "Your passwords did not match."
			assert len(self.args['email']) > 4, "Your email was too short."
			assert self.args['email'] == self.args['email2'], "Your emails did not match."
			uid = User.createUser(name, login, password, email)
			assert uid, "Failed to create user, database error."
			self.session.addMessage("<div class='success'>You are now registered as %s, your login is '%s'." % (name, login))
	def render(self, next=None):
		self.title = "Registration Form"
		name = self.args.get('name', '')
		email = self.args.get('email','')
		email2 = self.args.get('email2','')
		login = self.args.get('login','')
		self.body = self.template("form-register", locals())
		return HTMLBlock.render(self, next)

class AdminMenuBlock(HTMLBlock):
	def render(self, next=None):
		if self.session.hasRole('admin'):
			self.title = 'Admin Menu'
			self.body = self.template("admin-menu", {})
		else:
			self.title = ''
			self.body = ''
		yield HTMLBlock.render(self, next)

class ModuleBlock(HTMLBlock):
	def init(self):
		self.modules = Query("select mid, name, active from modules")
		self.requireCSS("modules.css")
	def action(self):
		if self.args.get('action',False):
			if self.session.hasRole('admin'):
				name = self.args.get('name',False)
				assert name, "'name' is required"
				assert self.args['action'] in ("enable","disable"), "Unknown action: %s" % self.args['action']
				if self.args['action'] == 'enable':
					LimoModel().enableModule(name)
					self.session.addMessage("Module enabled: %s" % name)
				elif self.args['action'] == 'disable':
					LimoModel().disableModule(name)
					self.session.addMessage("Module disabled: %s" % name)
			else:
				raise HTTPStatusError(403, "Forbidden")
	def render(self, next=None):
		self.title = "Modules"
		def body():
			yield """
			<ul id="module-list" class="module-list">
			"""
			for row in modules:
				yield self.template("module-row", {"row": row})
			yield """
			</ul>
			"""
		self.body = body()
		return HTMLBlock.render(self, next)

if __name__ == "__main__":
	import doctest
	doctest.testmod()
