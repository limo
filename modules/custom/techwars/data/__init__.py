#!/usr/bin/env python
from __future__ import with_statement
import os, datetime, struct, re, threading, math, sys, time
from multiprocessing import Process, Queue, Array, Pipe
from limoutil import log, Loggable, json
"""
This module reads the ascii format ZCTA files from the 2000 census.
"""

__all__ = ['RegionSet','Region','Polygon']

class Polygon:
	def __init__(self, corners):
		self.corners = corners
	def contains(self, xy):
		# for the first iteration check between the last point and the first
		verts = self.corners
		i = 0
		v = len(verts)
		j = v - 1
		x,y = xy
		inside = False
		while i < v: # then check every pair of points the rest of the way around
			if (verts[i][1]>y) != (verts[j][1]>y) \
				and (x < (verts[j][0]-verts[i][0]) * (y-verts[i][1]) / (verts[j][1]-verts[i][1]) + verts[i][0]):
				inside = not inside
			j = i
			i += 1
		return inside
	@staticmethod
	def circle_distance(lon1, lat1, lon2, lat2):
		R = 6371; # km
		dLat = deg_to_rad(lat2-lat1)/2.0
		dLon = deg_to_rad(lon2-lon1)/2.0
		a = math.sin(dLat) * math.sin(dLat) +\
			math.cos(deg_to_rad(lat1)) * math.cos(deg_to_rad(lat2)) * \
			math.sin(dLon) * math.sin(dLon)
		c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
		return R * c
	def __str__(self):
		return "{Polygon: (%d corners) %s}" % (len(self.corners), str(self.corners))
	def __repr__(self):
		return self.__str__()
	def __json__(self):
		return "["+",".join(["[%.15f, %.15f]" % (x,y) for x,y in self.corners])+"]"

class Region:
	def __init__(self, id, corners, neighbors=None):
		self.id = id
		assert type(corners) in (list,tuple)
		assert type(corners[0]) in (list, tuple)
		assert type(corners[0][0]) in (list, tuple)
		self.polygons = [Polygon(c) for c in corners]
		self.neighbors = neighbors
	def __str__(self):
		n = self.neighbors if self.neighbors is not None else []
		return "{Region: %d Polygons: %s Neighbors (%d): [%s]}" \
		% (self.id, str(len(self.polygons)), len(n), \
			str(n))
	def __repr__(self):
		return str(self)
	def __json__(self):
		return json({'id':self.id, 'polygons':self.polygons, 'neighbors':self.neighbors})
	def getCenter(self):
		return self.polygons[0][0]
	def contains(self, xy):
		for p in self.polygons:
			if p.contains(xy):
				return True
		return False
	# TODO: distance between regions, etc.
	def distance(self, region):
		min = 9999.0
		for pa in self.polygons:
			for pb in region.polygons:
				for ca in pa.corners:
					for cb in pb.corners:
						yield (abs(ca[0]-cb[0]), abs(ca[1]-cb[1]))

class RegionSet(dict,Loggable):
	def __init__(self, path="."):
		self.path = path
		# self.log("loading dat files from path: %s" % path)
		# self.update(load_all_dat_files(path))
		self.log("computing cache index...")
		self.datfile = os.path.sep.join((self.path, "dat.cache"))
		self.index = self.compute_cache_index(self.datfile)
		self.centers = {} # just the center points, as we get them
		for key in self.index.keys(): # make sure we have the keys in place, just late-load the data
			self[key] = None
		self.log("loading adjacency lists...")
		self.adjfile = os.path.sep.join((self.path, "adj.cache"))
		self.adj = load_adj(self.adjfile)

	def getCenter(self, regionid):
		if self.get(regionid,None) is None:
			if self.centers.get(regionid,None) is None:
				self.log("reading center point from index: %d" % regionid)
				with open(self.datfile, "rb") as inp:
					for offset in self.index[regionid]:
						inp.seek(offset - struct.calcsize("IH"))
						(ident,count) = _sip("IH", inp)
						self.centers[regionid] = _sip("ff", inp)
						break
			return self.centers[regionid]
		else:
			return self[regionid].getCenter()

	def __getitem__(self, k):
		if self.get(k,None) is None:
			with open(self.datfile,"rb") as inp:
				self[k] = []
				for offset in self.index[k]:
					self.log("loading cache entry: %d from %d" % (k, offset))
					inp.seek(offset - struct.calcsize("IH"))
					(ident, count) = _sip("IH", inp)
					assert ident == k, "Corrupt Index: %d != %d" % (ident, k)
					corners = _sip("ff" * count, inp) # read off one big block of floats
					self[k].append([tuple(corners[x:x+2]) for x in xrange(0,count*2,2)]) # then break them up into groups of 3: x,y,z
			self[k] = Region(k, self[k])
			self[k].neighbors = self.adj[k]
		return dict.__getitem__(self,k)

	def compute_cache_index(self, fname):
		index = {}
		with open(fname,"rb") as inp:
			try:
				while True:
					(ident, count) = _sip("IH", inp)
					index.setdefault(ident,[]).append(inp.tell())
					inp.seek(count*struct.calcsize("ff"),1)
			except EOFError:
				pass
		return index

def load_dat_file(filename):
	""" Returns a list of pairs: [id, corners] """
	records = {}
	with open(filename.replace(".dat","a.dat")) as a:
		lines = a.readlines()
		c = len(lines)
		for r in xrange(0,c,6):
			(id, name, _, _, _, _) = lines[r:r+6]
			id = id.strip()
			name = name.strip().replace('"','')
			# print id,"->",name
			records[id] = name
	ret = []
	with open(filename) as b:
		cur = None
		for line in b.readlines():
			# print "LINE:",len(line),line
			c = len(line)
			if c == 67:
				try:
					cur = int(records[line[0:10].strip()])
					ret.append( [cur, [] ] )
				except IndexError:
					raise IndexError("cur: %s len: %d" % (cur, len(bounds)))
				except ValueError:
					cur = None
					continue
				x = float(line[10:38])
				y = float(line[38:66])
				# print 'NEW RECORD:',cur,line
			elif c == 57:
				x = float(line[0:28])
				y = float(line[28:56])
			elif line.startswith('END'):
				cur = None
				continue
			else:
				continue # ignore the -99999 lines
			if cur is not None:
				ret[-1][1].append( (x,y,) )
	return ret

def _sip(fmt, f, repeat=1): # given a fmt string, read enough bytes to satisfy it, return parsed values in a list
	size = struct.calcsize(fmt)
	try:
		if repeat > 1:
			return [struct.unpack(fmt, f.read(size)) for i in xrange(repeat)]
		return struct.unpack(fmt, f.read(size))
	except struct.error:
		raise EOFError(f)

def load_all_dat_files(path):
	"""
	 Returns a list of Regions
	"""
	regions = {}
	# print "Reading all .dat files"
	log("load_all_dat_files: from %s (cwd: %s)" % (path, os.getcwd()))
	file = os.path.sep.join([path,"dat.cache"])
	if os.path.isfile(file):
		# print "Cache file found, reading."
		start = datetime.datetime.now()
		with open(file, "rb") as input:
			i = 0
			while True:
				i += 1
				try:
					(id,len_v) = _sip("IH", input)
					corners = _sip("ff" * len_v, input)
					regions.setdefault(id, []).append([tuple(corners[x:x+2]) for x in xrange(0,len_v*2,2)])
				except EOFError:
					break
			# print "Reading cache file took",(datetime.datetime.now() - start)," for %d regions" % len(regions.keys())
	else:
		# print "Cache file",file,"not found, creating."
		start = datetime.datetime.now()
		with open(file, "wb") as out:
			for f in os.listdir(path):
				if f[-7:] == "d00.dat":
					for id, corners in load_dat_file(f):
						regions.setdefault(id, []).append(corners)
						out.write(struct.pack("IH",id,len(corners)))
						out.write(''.join([struct.pack("ff", x,y) for x,y in corners]))
		# print "Creating cache file took",(datetime.datetime.now() - start)," for %d regions" % len(regions.keys())
	for id, corners in regions.items():
		regions[id] = Region(id, corners)
	return regions

def _linear_dist(x1,y1, x2,y2):
	return (((x1 + x2) ** 2) + ((y1 + y2) ** 2)) ** .5

def deg_to_rad(d):
	return d * 0.0174532925

def rad_to_deg(r):
	return r * 57.2957795

def load_adj(file):
	""" Reads the adj arrays from the cache file.
	Use the program from bounds.c to create the cache file (python is too slow for it).
	"""
	with open(file,"rb") as f:
		# print "Loading",file
		start = datetime.datetime.now()
		adj = [None] * 99999
		count = 0
		while True:
			try:
				(id,len_v) = _sip("IH",f)
				adj[id] = _sip("I"*len_v, f)
				count += 1
			except EOFError:
				break
		# print "Took",(datetime.datetime.now() - start),"for %d records" % count
		return adj

if __name__ == "__main__":
	#import doctest
	#doctest.testmod()
	R = RegionSet(".")
