
#include <cstdio>
#include <ctime>
#include <cmath>
#include <cassert>
#include <pthread.h>

typedef unsigned char byte;

#define NTHREADS 6
#define BATCHSIZE (99999/NTHREADS)

/* a simple linked list */
typedef struct Node {
	int v;
	Node *next;
	Node():v(0),next(NULL) {}
	Node(int v, Node *next):v(v),next(next) {}
};
typedef struct List {
	int item;
	short len;
	Node *last;
	Node *first;
	List():item(0),len(0),first(NULL),last(NULL) { }
};

/* some data structures for the region boundaries */
typedef struct Polygon {
	int corner_count;
	float *corners;
	Polygon():corner_count(0),corners(NULL) { }
	Polygon(int count, float *c):corner_count(count),corners(c) { }
};

typedef struct Region {
	List *polygons;
	Region():polygons(new List()) { }
};

Region *regions = new Region[99999]; // all the regions
List *adj = new List[99999]; // for each region, a linked list of neighbors

void list_append(List *list, int item) {
	if ( list->first == NULL ) {
		list->first = new Node(item, NULL);
		list->last = list->first;
	} else {
		list->last->next = new Node(item,NULL);
		list->last = list->last->next;
	}
	list->len += 1;
}

typedef void *iter_func(int item);

void list_iter(List *list, iter_func f) {
	Node *cur = list->first;
	for( ;cur != NULL;cur = cur->next ) {
		(*f)(cur->v);
	}
}

void print_item(int item) {
	printf("item: %d", item);
}

float fabs(float x) {
	if ( x < 0.0 ) return -1.0 * x;
	return x;
}
int abs(int x) {
	if ( x < 0 ) return -1 * x;
	return x;
}

template <class T>
T max(T a,T b) {
	if ( a > b ) {
		return a;
	}
	return b;
}

template <class T>
T min(T a, T b) {
	if ( a < b ) {
		return a;
	}
	return b;
}

float dist_sqrd(float ax, float bx, float ay, float by) {
	float dx = ax - bx;
	float dy = ay - by;
	return (dx*dx) + (dy*dy);
}
float dist(float ax, float bx, float ay, float by) {
	return sqrt(dist_sqrd(ax, bx, ay, by));
}

float deg_to_rad(float d) {
	return d * 0.0174532925;
}

float circle_distance(float lon1, float lat1, float lon2, float lat2) {
	float R = 6371;
	float dLat = deg_to_rad(lat2-lat1)/2.0;
	float dLon = deg_to_rad(lon2-lon1)/2.0;
	float a = sin(dLat) * sin(dLat) +
		cos(deg_to_rad(lat1)) * cos(deg_to_rad(lat2)) *
		sin(dLon) * sin(dLon);
	float c = 2 * atan2(sqrt(a), sqrt(1-a));
	return R * c;
}

float error(float a, float b) {
	return fabs(a-b);
}


bool share_a_corner(int ida, int idb, Polygon *a, Polygon *b) {
	float min_d = 9999.0;
	for ( int ca = 0; ca < a->corner_count*2; ca += 2) {
		float ax = a->corners[ca];
		float ay = a->corners[ca+1];
		// if ( ida == 10001 && idb == 10010 ) printf("\r\na = %d:[ %f, %f ]", ca, ax, ay);
		for ( int cb = 0; cb < b->corner_count*2; cb += 2) {
			float bx = b->corners[cb];
			float by = b->corners[cb+1];
			// if ( ida == 10001 && idb == 10010 ) printf("\r\nb = %d:[ %f, %f ]", cb, bx, by);
#ifdef USE_ERROR
			float e = error(ax,bx);
			float f = error(ay,by);
			if ( e < .02 && f < .01 ) {
				return 1;
			} else if ( 0 && (e > .5 || f > 1 ) ) {
				return 0;
			}
#endif
#ifdef USE_DISTANCE
			float d = circle_distance(ax, ay, bx, by);
			if ( d > 100.0 ) return 0;
			if ( d < min_d ) {
				min_d = d;
			}
#endif
		}
	}
#ifdef USE_DISTANCE
	if ( min_d < .18 ) {
		if (ida == 1001
			|| ida == 11211
			|| ida == 10001)
			printf("\r\nids: %d,%d min: %f\r\n", ida, idb, min_d);
		return 1;
	}
#endif
	return 0;
}

template <class T>
typedef struct Grid {
	/* Grid is a compact 2d-array of 1-bit booleans */
	T *data;
	int w, h;
	int n;
	int storage_size;
	Grid(int N):w(N),h(N) {
		storage_size = sizeof(T)*8;
		n = (int)((float(w)/storage_size)*h);
		printf("Grid: Allocating (%d*%d)/%d = %d\r\n",w,h,storage_size,n);
		data = new T[n];
		printf("Grid: Initializing...\r\n");
		for (int i =0; i < n; i++) {
			data[i] = 0;
		}
		printf("Grid: Finished...\r\n");
	}
	bool getbit(int x, int y) {
		int o = y*w + x;
		int a = o / storage_size;
		int b = o % storage_size;
		//printf("bit: %d %d -> %d %d\r\n", x, y, a, b);
		assert( a < n );
		return (data[a] & (1<<b))>>b;
	}
	bool setbit(int x, int y, bool val) {
		int o = y*w + x;
		int a = o / storage_size;
		int b = o % storage_size;
		// printf("bits: w: %d n: %d o:%d x:%d y:%d -> a:%d b:%d      \r\n", w, n, o, x, y, a, b);
		//bits: w: 31913 n: 15892674 o:1017131136 x:0 y:31872 -> a:15892674 b:0
		assert( a < n );
		T mask = (1<<b);
		fflush(NULL);
		if ( val ) {
			data[a] |= mask;
		} else {
			data[a] ^= mask;
		}
	}
};

int *index_to_id = NULL;
int *id_to_index = NULL;
Grid<int> *grid = NULL;

/* worker_func gets spawned multiple times, it loads a batch of regions */
void *worker_func(void *arg) {
	int first = (int)arg;
	int last = first + BATCHSIZE;
	printf("Worker: Running from %d to %d\n\n", first, last);
	int start = clock();
	int cxt_count = 0;
	int row_count = 0;
	for ( int i = first; i < last; i++ ) {
		if ( id_to_index[i] == -1 ) continue;
		Region *a = &regions[i];
		clock_t row_start = clock();
		if ( a == NULL || a->polygons == NULL ) continue;
		for(Node *na = a->polygons->first; na != NULL; na = na->next ) {
			Polygon *pa = (Polygon *)na->v;
			if( pa->corner_count > 0 ) {
				row_count++;
				for ( int j = 1001; j < 99950; j++ ) {
					int index_i = id_to_index[i];
					int index_j = id_to_index[j];
					if ( i == j
						|| index_j == -1
						|| grid->getbit(index_i,index_j)
						|| grid->getbit(index_j,index_i)
						)
						continue;
					Region *b = &regions[j];
					if ( b == NULL || b->polygons == NULL ) continue;
					for (Node *nb = b->polygons->first; nb != NULL; nb = nb->next ) {
						Polygon *pb = (Polygon *)nb->v;
						if ( pb->corner_count > 0 ) {
							if ( share_a_corner(i, j, pa, pb) == 1 ) {
								list_append(&adj[i], j);
								list_append(&adj[j], i);
								grid->setbit(index_i, index_j, 1);
								grid->setbit(index_j, index_i, 1);
								cxt_count += 2;
							}
						}
					}
				}
			}
		}
		int elapsed = (clock() - start);
		if ( 1 ) {
			float pct_complete = float(i-first) / BATCHSIZE;
			int remaining = elapsed * (int)((1 - pct_complete) / pct_complete);
			printf("[[Thread: %5d] %4.2f%% Elapsed: %5d s ETA: %5d s RPS: %8.2f ROW: %5d CXT: %5d] Region %5d took %3d ms   \n"
				, first, pct_complete*100, int(elapsed/1000), int(remaining/1000)
				, ((i-first) * 1000.0) / elapsed, row_count, cxt_count
				, i, (clock() - row_start)
				);
			fflush(NULL);
		}
	}
	printf("\r\nComputing adj took: %d ms for %d connections\r\n", (clock() - start), cxt_count);
}

int main() {
	clock_t start = clock();

	FILE *f = fopen("dat.cache", "rb");
	int region_count = 0;
	while ( ! feof(f) ) {
		int id = 0;
		short num_v = 0;
		if ( fread(&id, sizeof(int), 1, f) == 0 ) break;
		if ( fread(&num_v, sizeof(short), 1, f) == 0 ) break;
		int num_floats = num_v*2;
		float *row = new float[num_floats];
		if ( fread(row, sizeof(float), num_floats, f) < num_floats ) break;
		list_append(regions[id].polygons, (int)new Polygon(num_v, row));
		region_count++;
	}
	printf("Loading %d boundaries took: %d ms\r\n", region_count, (clock()- start));
	fclose(f);
	printf("Building index...\r\n");
	int cur = 0;
	index_to_id = new int[region_count];
	id_to_index = new int[99999];
	for( int i = 0; i < 99999; i++) {
		if( regions[i].polygons->len > 0 ) {
			index_to_id[cur] = i;
			id_to_index[i] = cur;
			cur++;
		} else {
			id_to_index[i] = -1;
		}
	}

	// this is the grid that the worker threads will fill in
	grid = new Grid<int>(region_count);

	// create a bunch of worker threads
	// that will open up each set of boundaries
	// and figures out who borders what
	pthread_t threads[NTHREADS];
	for( int i = 0; i < NTHREADS; i++) {
		printf("Creating thread i: %d arg: %d\n", i, BATCHSIZE*i);
		pthread_create(&threads[i],
			NULL,
			worker_func,
			(void *)(BATCHSIZE*i));
	}
	for( int i = 0; i < NTHREADS; i++) {
		pthread_join(threads[i], NULL);
	}

	FILE *out = fopen("adj.cache","wb");
	for ( int i = 0; i < 99999; i++) {
		List *list = &adj[i];
		if ( list->len == 0 ) continue;
		// printf("index: %d (%d items): ", i, list->len);
		fwrite(&i, sizeof(int), 1, out);
		fwrite(&list->len, sizeof(short), 1, out);
		Node *cur = list->first;
		while( cur != NULL ) {
			fwrite(&cur->v, sizeof(int), 1, out);
			// printf("%d ", cur->v);
			cur = cur->next;
		}
		// printf("\r\n");
	}
	fclose(out);
}

