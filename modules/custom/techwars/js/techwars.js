
jQuery.action = function (action, args, func, error_func) {
	$.get("/techwars/actions/", jQuery.extend(args, {action:action}), function( data, status ) {
		try {
			data = eval(data);
		} catch( e ) {
			// TODO: intercept errors like session timeouts, and maybe permissions
			if( error_func && error_func(e) )
				return;
			console.log(e);
			showError(e.toString());
			throw e;
		}
		func(data, status);
	});
}

jQuery.fn.dialog = function(options) {
	var settings = jQuery.extend({
		position: [0,0],
		modal:true,
		opacity:0.3,
		zIndex:128,
	}, options);
	return this.each(function() {
		if( settings.modal ) {
			var d = $(this).before("<div class='dialog-modal-overlay'></div>").prev(".dialog-modal-overlay").get(0);
			d.style.display = 'none';
			d.style.position = 'absolute';
			d.style.width = $(document).width() + "px";
			d.style.height = $(document).height() + "px";
			d.style.top = "0px";
			d.style.left = "0px";
			d.style.opacity = settings.opacity;
			d.style.backgroundColor = "black";
			d.style.zIndex = settings.zIndex - 1;
			this.overlay = d; // save a reference to the overlay
		}
		this.style.position = 'absolute';
		this.style.left = parseInt(settings.position[0]) + 'px';
		this.style.top = parseInt(settings.position[1]) + 'px';
		this.style.zIndex = settings.zIndex;
		this.className += ' dialog';
		this.style.display = 'none';
		$(this).bind('close', function(evt) {
			this.style.display = 'none';
			if( settings.modal ) {
				this.overlay.style.display = 'none';
			}
		});
		$(this).bind('open', function(evt) {
			this.style.display = 'block';
			if( settings.modal ) {
				this.overlay.style.display = 'block';
			}
		});
		$(this).bind('moveTo', function(evt, xy) {
			this.style.position = 'absolute';
			var top, left;
			var w = $(this).width();
			var h = $(this).height();
			var ww = $(window).width();
			var wh = $(window).height();
			if( xy instanceof Array ) {
				top = parseInt(xy[1]);
				left = parseInt(xy[0]);
			} else if ( xy instanceof Object ) {
				top = parseInt(xy.top);
				left = parseInt(xy.left);
			}
			// snap to the edges
			top = Math.min(Math.max(top - h/2,0), wh - h);
			left = Math.min(Math.max(left - w/2,0), ww - w);
			this.style.top = top + 'px';
			this.style.left = left + 'px';
		});
	});
}

jQuery.fn.close = function() {
	return this.each(function() {
		$(this).trigger('close');
	});
}

jQuery.fn.open = function() {
	return this.each(function() {
		$(this).trigger('open');
	});
}

jQuery.fn.reset = function(amount) {
	return this.each(function() {
		$(this).trigger('reset');
	});
}

jQuery.fn.moveTo = function(x,y) {
	return this.each(function() {
		$(this).trigger('moveTo', [x,y]);
	});
}

jQuery.fn.log = function(prefix) {
	return this.each(function() {
		console.log((prefix ? prefix+":" : ""), this);
	});
}

var debug = function(msg) {
	if( $("#debug-list").length == 0 ) {
		$("#block-debug").append("<ul id='debug-list'>");
	}
	$("#debug-list").append("<li>"+msg+"</li>");
}

jQuery.fn.energyField = function(options) {
	var settings = jQuery.extend({
		rate: 1.0/60,
		timeout: 3000,
		start: new Date(),
	}, options);
	return this.each(function() {
		$(this).data('settings',settings);
		var since = function() {
			return ((new Date() - settings.start)/1000.0);
		}
		var t = this;
		t.originalEnergy = parseFloat($(t).text());
		var timeout = function() {
			var new_energy =  t.originalEnergy + ( since() * settings.rate );
			$(t).text(new_energy.toFixed(2));
			setTimeout( timeout, settings.timeout);
		}
		setTimeout( timeout, Math.random()*settings.timeout);
		$(this).bind('reset', function() {
			t.originalEnergy = 0.0;
			settings.start = new Date();
		});
	});
}

jQuery.fn.takeEnergy = function(amount) {
	return this.each(function() {
		this.originalEnergy -=  amount;
	});
}

jQuery.fn.centerOf = function() {
	var w = $(this).width();
	var h = $(this).height();
	return [w/2, h/2];
}

jQuery.fn.rightSideOf = function() {
	var pos = $(this).offset();
	pos.left += $(this).width();
	return pos;
}

function sum(a) {
	if( a.length == 0 )
		return 0;
	return a[0] + sum(a.slice(1));
}

function showError(msg, callback) {
	$("#error-dialog")
		.close()
		.moveTo($(window).centerOf())
		.html("<div class='error'>"+msg+"</div>"
			+"<button class='ok'>Ok</button>")
		.open()
		.find("button.ok").click(function() {
			$("#error-dialog").close();
			if( callback ) {
				callback.apply($("#error-dialog"));
			}
		});
}

function statusUpdateLoop() {
	$.action("statusUpdate",
		{},
		function( data ) {
			for(var i = 0; i < data.length; i++ ) {
				console.log('statusUpdate',data[i]);
				var kind = data[i][0];
				var row = eval(data[i][1]);
				switch( kind ) {
					case "REGIONLOST":
						// expects region id
						var id = parseInt(row)
						$("#status-dialog").close()
							.html("Region "+id+" has been overrun!<br>Fight back quickly before it is reinforced! <button>Ok</button>")
							.moveTo($(window).centerOf())
							.open()
							.find("button").click(function() {
								$("#status-dialog").close();
								$("#"+id).fadeOut(1000, function() {$(this).remove();});
							});
					break;
					case "UNITS":
						// expects [region id, [unit counts]]
						var id = parseInt(row[0])
						var j = 0;
						$("#"+id+" div.units li").each(function() {
							$(this).text(parseFloat(row[1][j++]).toFixed(2));
						});
					break;
				}
			}
			setTimeout(statusUpdateLoop, 15000);
		},
		function( error ) {
			if( error.message == "Must select a player." ) 
				return true;
		}
	);
}

$(document).ready(function() {
	
	if( window.location.hash.length > 0 ) {
		window.location.hash = '';
	}

	/*** Status Updates ***
	 **********************/
	// init the dialog
	$(document.body).append("<div id='status-dialog'>&nbsp;</div>");
	$("#status-dialog").dialog({modal:false});
	// start a server update loop
	setTimeout(statusUpdateLoop, Math.random()*3000);


	/*** Showing Errors ***
	 **********************/
	$(document.body).append("<div id='error-dialog'>&nbsp;</div>");
	$("#error-dialog").dialog();

	/*** Select Player ***
	 *********************/
	$("#btn-select-player").click(function() {
		$.action("selectPlayer"
			, { player: $("#select-player").val(), }
			, function(data) {
				location.reload();
			}
		);
	});

	/*** Deselect Player ***
	 ***********************/
	$("#block-techwarsplayer a.deselect").click(function() {
		$.action("clearSelectedPlayer", {}
			, function( data ) {
				window.location.reload(false);
			}
		);
	});

	/*** Delete Player button ***
	 ***********************/
	$("#block-techwarsplayer a.delete").click(function() {
		var pos = $(this).offset();
		pos.left += $(this).width();
		$("#delete-player-confirm-dialog")
			.moveTo(pos)
			.open();
	});
	$("#delete-player-confirm-dialog")
		.dialog()
		.find("button.yes").click(function() {
			$.action("deleteSelectedPlayer", {}
				, function( data ) {
					window.location.reload();
				});
			}
		).parent()
		.find("button.no").click(function() {
			$("#delete-player-confirm-dialog").close();
		});
		
	/*** Create Player ***
	 *********************/
	$("#btn-create-player").click(function() {
		$.action( "createPlayer"
			, { player: $("#new-name").val() }
			, function(data) {
				location.reload();
			}
			, function(error) {
				$("#btn-create-player").after("<div class='error'>"+error.toString()+"</div>");
			}
		);
	});

	/*** Energy Fields ***
	 *********************/
	$("#region-list div.energy").energyField();
	$("#region-list div.bldgs .output").each(function() {
		var id = parseInt(this.parentNode.className) - 1;
		var bcount = parseInt($(this).parent().text());
		var rate = ProductionRates[id] * bcount;
		$(this).energyField({rate: rate});
	});

	/*** Attack Button ***
	 *********************/
	$("#attack-dialog").dialog();
	$("#victory-dialog").dialog()
		.find("button").click(function() {
			$("#victory-dialog").close();
			window.location.reload(false);
		});
	$("#region-list a.attack").click(function(evt) {
		evt.preventDefault();
		evt.stopPropagation();
		// clear any highlights
		$("#region-list div.hilite").removeClass('hilite')
			.find("li").unbind('click');
		// find the region node being operated on
		var item = $(this).parents(".region-item");
		var attacker = item.attr('id');
		var attackersNode = item.find(".units");
		console.log('attacker',attacker);
		// loop over the neighbors and get list of possible targets
		var targets = jQuery.map(	item.find("div.neighbors li"), function(elem) {
			var text = $(elem).text();
			if( $("#"+text).length > 0 ) return null; // if this element is one of our own, dont attack it
			return text;
		});
		console.log('targets',targets);
		// loop over the units and get list of available units
		var units = jQuery.map( item.find("div.units li"), function(elem) {
			return parseInt($(elem).text()); // round to ints when attacking
		});
		console.log('units',units);

		// compute the position of the dialog just to the right of the link
		var pos = $(this).offset();
		pos.left += $(this).width();

		// if there are no targets:
		if( targets.length == 0 ) {
			$("#attack-dialog").html("<div class='error'>No targets available.</div><button>Cancel</button>")
				.moveTo(pos).open()
				.find("button").click(function() {
					$("#attack-dialog").close();
				});
		} else {
			$("#attack-dialog")
				.html("<span>Select A Target:</span><ul><li>"
					+ targets.join("</li><li>")
					+ "</li></ul>")
				.moveTo(pos)
				.open()
				.find("li").click(function() {
					var defender = $(this).text();
					console.log('defender', defender);
					$("#attack-dialog")
						.close()
						.html("<span>Select A Unit:</span><ul>"
							+ "<li>"+units[0]+" Soldiers</li>"
							+ "<li>"+units[1]+" Tanks</li>"
							+ "<li>"+units[2]+" Planes</li>"
							+ "</ul>")
						.open()
						.find("li").click(function() {
							var utid = $(this).parent().find("li").index(this) + 1;
							var count = parseInt($(this).text()); // round DOWN when attacking
							console.log('utid',utid,'count',count);
							$.action("computeAttack", {
									attacker: attacker
									, defender: defender
									, utid: utid
									, count: count
								}, function(data) {
									console.log('attack result:',data);
									var energy = data[0];
									var attackers = data[1];
									var defenders = data[2];
									// update the attackers display
									var i = 0;
									attackersNode.find("li").each(function() {
										$(this).text(attackers[i++]);
									})
									// update the energy display
									item.find(".energy").takeEnergy(energy);
									// update the defender's display
									i = 0;
									$("#"+defender).find(".units").find("li").each(function() {
										$(this).text(defenders[i++]);
									});
									if( sum(defenders) == 0 ) { // VICTORY!
										$("#attack-dialog").close();
										$("#victory-dialog").open();
									} else { // the fight continues
										$("#attack-dialog")
											.close()
											.html("<h3>Attack Report:</h3>"
												+ "<span>Attackers remaining:</span><div>"+$.map(attackers, function(x) { return x.toFixed(2); }).join(" ")+"</div>"
												+ "<span>Defenders remaining:</span><div>"+$.map(defenders, function(x) { return x.toFixed(2); }).join(" ")+"</div>"
												+ "<button>close</button>")
											.find("button").click(function() {
												$("#attack-dialog").close();
											})
											.open();
									}
								}
							);
						}); // end unit type li click
				}) // end region li click
				.each(function() {
					var id = $(this).text();
					if( $("#"+id).length > 0 ) {
						$(this).css('text-decoration','none').css('cursor','default').unbind('click');
					}
				});
		}
	}); // end Attack Button

	/*** Attack from Reserves ***
	 ****************************/
	$("#block-techwarsplayer .attack").click(function(evt) {
			console.log('attack click');
			evt.stopPropagation();
			evt.preventDefault();
			// first ask what region to attac
			$("#attack-dialog").html("What region would you like to attack? <input type='text' size=6 /><button>Continue</button>")
				.moveTo($(this).rightSideOf())
				.open()
				.find("button").click(function() {
					var target = $(this).prev("input").val();
					target = parseInt(target);
					if( !( target > 999 && target < 99999 ) ) {
						showError("Invalid target.");
						return;
					}
					console.log('target',target);
					// close the dialog so we can see the list of units to select from
					$("#attack-dialog").close();
					// then hilite the reserve unit list to select a unit to attack with
					$("#block-techwarsplayer div.reserves").addClass("hilite")
						.find("li").click(function() {
							$("#block-techwarsplayer div.reserves").removeClass('hilite').unbind('click');
							var utid = $("#block-techwarsplayer div.reserves li").index(this) + 1;
							var maxunits = parseFloat($(this).text());
							if( maxunits == 0 ) {
								showError("Cant attack with 0 units.");
								return;
							}
							console.log('utid',utid);
							if( !( utid > 0 && utid < 4 ) ) {
								showError("Invalid unit type id: "+utid);
								return;
							}
							$.action("computeAttackFromReserves",
								{ defender: target
								, utid: utid
								},
								function( data ) {
									var attackers = data[0];
									var defenders = data[1];
									if( sum(defenders) == 0 ) {
										// show a victory message
										$("#attack-dialog")
											.close()
											.html("Victory! You have captured a region. <button>Claim Region</button>")
											.open()
											.find("button").click(function() {
												window.location.reload(false);
											});
									} else {
										// show the battle results
										$("#attack-dialog")
											.close()
											.html("Defenders remaining: " + defenders.join(', ') + "<br />"
													+ "Attackers remaining: " + attackers.join(', ') + "<br />"
													+ "<button>OK</button>")
											.open()
											.find("button").click(function() {
												$("#attack-dialog").close();
												var i = 0;
												$("#block-techwarsplayer div.reserves li").addClass('litehilite').each(function() {
													$(this).text(parseFloat(attackers[i++]).toFixed(2));
												});
												setTimeout(function() {$("#block-techwarsplayer div.reserves li").removeClass('litehilite');}, 1000);
											});
									}
								},
								function( error ) {
									showError(error.toString());
								}
							);
						});
				});
	});

	/*** Move from Reserves ***
	 **************************/
	$("#block-techwarsplayer .move").click(function(evt) {
		evt.preventDefault();
		evt.stopPropagation();
		var pos = $(this).offset();
		pos.left += $(this).width();
		// highlight the reserve units
		$("#block-techwarsplayer div.reserves").addClass("hilite")
			.find("li").click(function() {
				// and once one is picked
				// remove this highlight and the click
				$("#block-techwarsplayer div.reserves").removeClass('hilite').unbind('click');
				// and get the selected values
				var utid = $("#block-techwarsplayer div.reserves li").index(this) + 1;
				var maxcount = parseFloat($(this).text());
				if( maxcount == 0 ) {
					showError("Cant move 0 units.");
					return;
				}
				console.log('utid',utid);
				if( !( utid > 0 && utid < 4 ) ) {
					showError("Invalid unit type id: "+utid);
					return;
				}
				// highlight all the regions we own
				$("#region-list h3.region-title span").addClass('hilite')
					.click(function() {
						// remove the highlight and the clicking
						$("#region-list h3.region-title span").removeClass('hilite').unbind('click');
						// and pick this one
						var target = $(this).text();
						target = parseInt(target);
						if( !( target > 999 && target < 99999 ) ) {
							showError("Invalid target.");
							return;
						}
						console.log('target',target);
						$("#transfer-dialog")
							.close()
							.moveTo(pos)
							.html("How many would you like to send? <input type='text' size='6' /> <button>Send</button>")
							.open()
							.find("button").click(function() {
								var count = $(this).prev("input").val();
								if( count < 1 || count > maxcount ) {
									showError("Entry '"+count+"' is invalid, not in [1.."+maxcount+"]");
									return;
								}
								$.action("moveReserves",
									{ utid: utid
									, count: count
									, target: target
									},
									function( data ) {
										$("#transfer-dialog").html("").close();
										var reserves = data[0];
										var region_units = data[1];
										var i = 0;
										// update the reserve units
										$("#block-techwarsplayer div.reserves li").each(function() {
											$(this).text(parseFloat(reserves[i++]).toFixed(2));
										}).addClass('litehilite');
										// update the region units
										i = 0;
										$("#"+target+" div.units li").each(function() {
											$(this).text(parseFloat(region_units[i++]).toFixed(2));
										}).addClass('litehilite');
										// wait 2 seconds and clear the hilite
										setTimeout(function() { $("#region-list .litehilite, #block-techwarsplayer .litehilite").removeClass('litehilite'); }, 1000);
									},
									function( error ) {
										showError(error.toString());
									}
								);
							});
					});
			});
		return false;
	});

	/*** Transfer Button ***
	 ***********************/
	$("#transfer-dialog").dialog(); // init the dialog
	$("#region-list a.transfer").click(function(evt) {
		evt.preventDefault();
		evt.stopPropagation();
		$("#region-list div.hilite")
			.removeClass('hilite')
			.find("li")
			.unbind('click');
		var item = $(this).parents(".region-item");
		// get the source region id
		var source = item.attr('id');
		item
		.find("div.units").addClass('hilite')
		.find("li").click(function() {
			// get the unit type to send
			var utid = $(this).attr('className');
			// get all owned regions
			var targets = jQuery.map(	$("#region-list li.region-item"), function(elem) {
				if( elem.id == source ) return null;
				return elem.id;
			});
			// show a dialog asking which region to move to
			var pos = $(this).offset();
			pos.left += $(this).width();
			$("#transfer-dialog")
			.moveTo( pos )
			.open()
			.html( "<ul><li>"
				+ targets.join("</li><li>")
				+ "</li></ul><button>cancel</button>")
			.find("button").click(function() {
				$(this).parent().close();
				item.find(".hilite").removeClass("hilite").find("li").unbind("click");
			}).parent()
			.find("li").click(function() {
				// set the destination
				var destination = $(this).text();
				// re-display the dialog asking how many
				$("#transfer-dialog")
				.close()
				.html("How many units?: "
					+"<input type='text' size='4'>"
					+"<button class='ok'>ok</button>"
					+"<button class='cancel'>cancel</button>")
				.open()
				.find("input").bind('focus,click,keydown',function() {
					$(this).parent().find(".error").remove();
				})
				.val($("#"+source+" div.units li."+utid).text()) // default the quantity input to the max
				.parent()
				.find("button.ok").click(function() {
					var count = parseInt($(this).parent().find("input").val());
					if( count > 0 ) {
					 // ask the server to move
						$.action("moveTroops", {
								regiona: source
								, regionb: destination
								, utid: utid
								, count: count
							}, function( data ) {
								// the data is the total energy spent
								if( data > 0.0 ) {
									$("#transfer-dialog").close();
									$("#region-list .hilite").removeClass('hilite').find("li").unbind("click");
									var li = $("#"+source+" div.units li."+utid);
									li.text( parseFloat(li.text()) - count);
									li = $("#"+destination+" div.units li."+utid);
									li.text( parseFloat(li.text()) + count);
									$("#"+source+" div.energy")
										.takeEnergy(data);
								} else {
									// no energy spent, TODO: show an error?
								}
							}, function( error ) {
								$("#transfer-dialog").append("<div class='error'>"+error.toString()+"</div>");
								console.log(error);
								return true; // handled it
							}
						);
					} else {
						$(this).parent().append("<div class='error'>Enter a number &gt; 0.</div>");
					}
				})
				.parent()
				.find("button.cancel").click(function() {
					$("#transfer-dialog").close();
					$("#region-list .hilite").removeClass('hilite');
				});
			});
		});
	}); // end Transfer button

	/*** Redeem Button ***
	 *********************/
	$("#region-list a.redeem").click(function(evt) {
		evt.stopPropagation();
		evt.preventDefault();
		var item = $(this).parents(".region-item");
		$.action("redeemOutput", {
				region: item.attr('id')
			}, function( data ) {
				item.find("div.units").addClass('litehilite').find("li").each(function() {
					var id = parseInt(this.className) - 1;
					$(this).text(parseFloat(data[id]).toFixed(2));
				});
				setTimeout(function() { item.find("div.units").removeClass('litehilite'); }, 1000);
				item.find("div.bldgs .output").text('0.00').reset()
			}
		);
		return false;
	});

	/*** Build Dialog ***
	 ********************/
	$("#build-dialog")
		.dialog()
		.find("a.cancel")
		.click(function(evt) {
			evt.preventDefault();
			evt.stopPropagation();
			$("#build-dialog").close();
			return false;
		});

	/*** Build Button ***
	 ********************/
	$("#region-list a.build").click(function(evt) {
		evt.stopPropagation();
		evt.preventDefault();
		var pos = $(this).offset();
		pos.left += $(this).width();
		var item = $(this).parents(".region-item");
		$("#build-dialog")
			.moveTo( pos )
			.open()
			.find("li")
			.click(function() {
				var btid = $(this).attr('btid');
				$.action("addBuilding", {
					region: item.attr('id')
					, btid: $(this).attr('btid')
					}, function( data ) {
						if( data instanceof Array ) {
							var cost = data[0];
							data = data[1];
							item.find("div.energy").takeEnergy(cost);
							item.find("div.bldgs").addClass('litehilite').find("li").each(function() {
								var id = parseInt(this.className) - 1;
								$(this).text(parseInt(data[id]));
							});
							setTimeout(function() { item.find("div.bldgs").removeClass('litehilite'); }, 1000);
							$("#build-dialog").close().find("li").unbind("click");
							item.find("div.outputs li").each(function() {
								var id = parseInt(this.className) - 1;
								var count = parseInt($(this).parents(".region-item").find("div.bldgs li."+this.className).text());
								var rate = ProductionRates[id] * count;
								$(this).data('settings').rate = rate;
								$(this).data('settings').start = new Date();
							});
						} else {
							console.log("unknown response: "+data);
						}
					}, function( error ) {
						$("#build-dialog").prepend("<div class='error'>"+error.toString()+"</div>");
						console.log(error);
						return true;
					}
				);
			});
		return false;
	});

	$("#attack-dialog").dialog();
}); // end document.ready
