#!/usr/bin/python env

from modules.core import LimoBlock, HTMLBlock, LimoModel
from db import FastQuery, safe, Query
from template import Template
from limoutil import *
from fcgi.fcgiutil import flatten

import types, traceback, sys

# zip code data:
from data import RegionSet

from operator import itemgetter

dataloc = os.path.sep.join(["modules","custom","techwars","data"])
_tw_regions = RegionSet(dataloc)

def ints(a):
	return [int(x if x is not None else 0) for x in a]

def floats(a):
	return [float(x if x is not None else 0) for x in a]

class TechWarsModel(LimoModel):
	energyRate = 1.0/60
	def __init__(self):
		global _tw_regions
		self.regions = _tw_regions
		LimoModel.__init__(self)
	def model(self):
		self \
			.table("building_types")\
				.col("btid int not null primary key")\
				.col("name varchar(32) not null")\
				.col("code varchar(32) not null")\
				.col("cost float not null")\
				.data([1, 'Barracks', "BARRACKS", 10.0 ])\
				.data([2, 'Factory', "FACTORY", 20.0])\
				.data([3, 'Runway', "RUNWAY", 20.0])\
				.data([4, 'Power Plant', "PLANT", 50.0])\
				.data([5, 'Overflow', "OVERFLOW", 50.0])\
				.data([6, 'Output Circuit', "CIRCUIT", 50.0])\
			.table("unit_types")\
				.col('utid int not null primary key')\
				.col("name varchar(32) not null")\
				.col("hp int not null default 1")\
				.data([1, 'Soldier', 1])\
				.data([2, 'Tank', 5])\
				.data([3, 'Plane', 5])\
			.table("production_rates")\
				.col("btid int not null")\
				.col("utid int not null")\
				.col("rate float not null")\
				.col("primary key (btid, utid)")\
				.data([1,1,5.0/60.0])\
				.data([2,2,1.0/60.0])\
				.data([3,3,1.0/60.0])\
				.data([4,4,0.0])\
				.data([5,5,0.0])\
				.data([6,6,0.0])\
			.table("unit_matchups")\
				.col("utida int not null")\
				.col("utidb int not null")\
				.col("weight float not null")\
				.col("primary key (utida, utidb)")\
				.data([1,1,1.0])\
				.data([1,2,0.3])\
				.data([1,3,3.0])\
				.data([2,1,3.0])\
				.data([2,2,1.0])\
				.data([2,3,0.3])\
				.data([3,1,0.3])\
				.data([3,2,3.0])\
				.data([3,3,1.0])\
			.table("action_types")\
				.col("atid int not null primary key")\
				.col("name varchar(32) not null")\
				.col("unique key (name)")\
				.data([1, 'attack'])\
				.data([2, 'move'])\
			.table("action_costs")\
				.col("utid int not null")\
				.col("atid int not null")\
				.col("cost float not null")\
				.col("primary key (utid, atid)")\
				.col("index (atid)")\
				.data([1,1,0.2])\
				.data([2,1,1.0])\
				.data([3,1,1.0])\
				.data([1,2,0.6])\
				.data([2,2,2.0])\
				.data([3,2,1.0])\
			.table("units")\
				.col("utid int not null")\
				.col("region int not null")\
				.col("primary key (utid, region)")\
				.col("count float not null")\
				.col("index (region)")\
			.table("buildings")\
				.col("btid int not null")\
				.col("region int not null")\
				.col("primary key (btid, region)")\
				.col("count float  not null")\
				.col("started datetime not null")\
				.col("index (region)")\
			.table("players")\
				.col("id int not null auto_increment primary key")\
				.col("uid int not null")\
				.col("name varchar(32) not null")\
				.col("created datetime not null")\
				.data([2, self.int_uid('admin'), 'techwarsadmin'])\
			.table("regions")\
				.col("id int not null primary key")\
				.col("owner int not null")\
				.col("energy_timer datetime not null")\
				.col("energy_spent float not null default '0.0'", version=1)\
				.col("energy_group int null default null")\
				.col("index (owner)")\
			.table("energy_groups")\
				.col("id int not null auto_increment primary key")\
				.col("started datetime not null")\
				.col("spent float not null default 0.0")\
				.col("count int not null default 0")\
			.table("alliances")\
				.col("ida int not null")\
				.col("idb int not null")\
				.col("primary key (ida, idb)")\
				.col("index (ida)")\
				.col("index (idb)")\
			.table("reserves")\
				.col("utid int not null")\
				.col("player int not null")\
				.col("primary key (utid, player)")\
				.col("index(player)")\
				.col("count float not null default 0.0")\
			.table("status_updates")\
				.col("id int not null primary key auto_increment")\
				.col("player int not null")\
				.col("kind varchar(32) not null")\
				.col("data varchar(1024) null")\
				.col("sent bool not null default false")\
				.col("entered datetime not null")\
			.defaultPage("/techwars")\
			.defaultPage("/techwars/actions")\
			.defaultBlock("TechWarsBlock")\
			.defaultBlock("TechWarsPlayerBlock")\
			.attachBlockToRegion("/techwars", "main", "TechWarsBlock")\
			.attachBlockToRegion("/techwars", "header", "EmptyBlock")\
			.attachBlockToRegion("/techwars", "left", "SessionBlock")\
			.attachBlockToRegion("/techwars", "left", "TechWarsPlayerBlock")\
			.attachBlockToRegion("/techwars/actions", "entire", "TechWarsActions")\
			.execute()
		ret = super(LimoModel, self).model()
		rate = self.getEnergyRate()
		FastQuery("""
			create or replace view regions_view as
			select 
			r.id, r.owner, r.energy_group
			, pr.utid, u.count AS ucount
			, bt.btid, b.count AS bcount
			, (time_to_sec(timediff(now(), r.energy_timer)) * %f) - r.energy_spent AS region_energy
			, (time_to_sec(timediff(now(), g.started)) * %f) - g.spent AS group_energy
			, (pr.rate * b.count * time_to_sec(timediff(now(), b.started) )) as boutput
			, pr.rate
			FROM regions r
			JOIN building_types bt on 1=1
			LEFT JOIN buildings b on b.region = r.id AND b.btid = bt.btid
			LEFT JOIN production_rates pr on pr.btid = bt.btid
			LEFT JOIN units u on u.region = r.id AND u.utid = pr.utid
			LEFT JOIN energy_groups g on g.id = r.energy_group
			;
		""" % (rate,rate))
		count = [row[0] for row in FastQuery("""select count(*) from regions""")][0]
		if count == 0:
			log("The regions have not been loaded, inserting...")
			# the regions have not been loaded
			keys= self.regions.keys()
			# insert 100 rows at a time
			for i in range(0,len(keys),100):
				log("Inserting row %d" % i)
				ids = keys[i:i+100]
				FastQuery("insert ignore into regions (id)"\
					+ " union all ".join(["select %d" % d for d in ids])\
					+ ";")
		return ret


	### Energy functions ###
	########################
	def getEnergy(self, region):
		""" Energy regenerates over time, so rather than have a background task updating energy,
			we store the starting time, compute how much energy regenerated between now and then,
			and then we subtract out all expenses since then.
		"""
		for row in FastQuery("""
			SELECT region_energy, group_energy
			FROM regions_view rv
			WHERE id = %d """ % region):
			if row[1] not in (None, "None", 0): # if there is a group id
				# compute the group's energy
				energy = row[1]
			else:
				# compute just this regions energy
				energy = row[0]
			return energy

	def getEnergyRate(self):
		return 1.0/60.0 # 1 energy every minute

	def resetEnergy(self, region):
		FastQuery("update regions set energy_timer = now(), energy_spent = 0 where id = %d" % region)
		return self

	def normalizeEnergyTimer(self, region):
		spent = -1 * self.getEnergy(region)
		FastQuery("update regions set energy_timer = now(), energy_spent = %f where id = %d" % (spent, region))

	def takeEnergy(self, region, amount):
		group = self.getEnergyGroup(region)
		if group is not None:
			self.takeEnergyFromGroup(group, amount)
		FastQuery("update regions set energy_spent = energy_spent + %f where id = %d" % (amount, region))
		return self

	def giveEnergy(self, region, amount):
		energy = self.getEnergy(region) + amount
		group = self.getEnergyGroup(region)
		if group is not None:
			self.giveEnergyToGroup(group, amount)
		else:
			self.takeEnergy(region, -amount)
			if energy > 25:
				self.overflowEnergy(region)

	### Energy Groups ###
	#####################
	def createEnergyGroup(self):
		return [row[0] for row in FastQuery("""
			insert into energy_groups (started) values (now());
			select last_insert_id() as id;""")][0]

	def getEnergyGroup(self, region):
		for row in FastQuery("select energy_group from regions where id = %d" % region):
			return row[0]
		return None

	def takeEnergyFromGroup(self, group, amount):
		FastQuery("update energy_groups set spent = spent + %f where id = %d" % (amount, group))
		return self

	def giveEnergyToGroup(self, group, amount):
		self.takeEnergyFromGroup(group, -amount)
		return self

	### Energy Overflow ###
	#######################
	_overflowing = {}
	_targetCycle = []
	def overflowEnergy(self, region):
		""" Process energy overflow for one region.
			Potentially recursive, as giveEnergy could cause more overflows.
			Cycle-detection is in place for creating energy groups.
		"""
		group = self.getEnergyGroup(region)
		if group is not None:
			return # dont overflow regions in a group

		if target is None:
			self._targetCycle = []
		elif self._targetCycle.contains(region):
			group = self.createEnergyGroup() # create a new group for the regions
			FastQuery("update regions set energy_group = %d where region in (%s) " % ( group, ', '.join(self._targetCycle)))
			self._targetCycle = []
			return # dont overflow regions newly added to a group
		elif self._overflowing.has_key(region):
			return # just kill any cycles that arent the result of an explicit circuit

		self._overflowing[region] = 1
		energy = self.getEnergy(region)
		if target is not None:
			self.giveEnergy(target, energy)
		else:
			n = []
			for id,v in self.getNeighborRelations(region).items():
				if v in (self.RELATE_SELF, self.RELATE_FRIEND):
					n.append(id)
			if len(n) > 0:
				# give equally to all neighbors
				energy = energy / len(n)
				for id in n:
					self.giveEnergy(id, energy)
		self.resetEnergy()
		del self._overflowing[region]
		return len(self._overflowing.keys())

	def computeOverflow(self):
		""" Find all the regions with Overflow buildings,
		and if their energy is too high, spill it over onto neighbors.
		"""
		for row in FastQuery("""
			SELECT r.id as region, c.end as target
			FROM region r
			JOIN buildings b on b.region = r.id
			LEFT JOIN circuits c on c.start = r.id
			WHERE b.btid =
				(SELECT btid FROM building_types WHERE code = 'OVERFLOW')
			"""):
			if row[1] > 25:
				overflowEnergy(row[0], row[2])

	def getProductionRates(self):
		return [row[0] for row in FastQuery("""
			select rate from production_rates order by btid
			""")]
	def getActionCosts(self):
		return [row[0] for row in FastQuery("select cost from action_costs order by atid")]

	### Buildings ###
	#################
	def getBuildingTypes(self):
		return [row for row in FastQuery("select * from building_types order by btid")]

	def addBuilding(self, region, btid, count=1):
		energyCost = [row[0] for row in FastQuery("select cost from building_types where btid = %d" % btid)][0]
		energyCost *= count
		energyAvailable = self.getEnergy(region)
		assert energyCost <= energyAvailable, "You do not have enough energy to build that. Need %s." % energyCost
		self.takeEnergy(region, energyCost)
		FastQuery("insert ignore into buildings (btid, region, started, count) values (%d,%d,now(),0.0)" % (btid,region))
		FastQuery("update buildings set count = count + %f where region = %d and btid = %d" % (count, region, btid))
		return energyCost

	def getBuildings(self, region):
		return [float(row[0] if row[0] is not None else 0.0) for row in FastQuery("""
			select b.count from building_types bt
			left join buildings b on b.btid = bt.btid and b.region = %d
			order by bt.btid
			""" % region)]

	def clearBuildings(self, region):
		FastQuery("delete from buildings where region = %d" % region)

	def getBuildingOuptut(self, region, btid):
		ret = [0.0,] * 3
		for row in FastQuery("""select started, count, rate/60 from buildings b
				join production_rates pr on pr.btid = b.btid
				where region = %d and btid = %d"""):
			return (datetime.datetime.now() - row[0]).seconds * count

	def resetBuildingOutput(self, region):
		FastQuery("update buildings set started = now() where region = %d " % (region ))

	def redeemBuildingOutput(self, region):
		""" Take all the building output, add it to the unit pool, then clear the buildings """
		for row in FastQuery("""
			select r.btid, r.boutput, pr.utid from regions_view r
			join production_rates pr on pr.btid = r.btid
			where r.id = %d
			""" % region):
			log("redeeming: %s" % str(row))
			self.addUnits(region, row[2], row[1])
		self.resetBuildingOutput(region)
		return self

	### Units ###
	#############
	def getUnits(self, region):
		return floats([row[0] for row in FastQuery("""
			select u.count from unit_types ut
			left join units u on u.utid = ut.utid and u.region = %d
			order by ut.utid""" % region)])

	def addUnits(self, region, utid, count):
		FastQuery("""
			begin work;
			insert ignore into units (region, utid, count) values (%d, %d, 0);
			update units set count = count + %d where region = %d and utid = %d;
			commit;
			""" % (region, utid, count, region, utid))
		return self

	def getReserveUnits(self, player):
		return floats([row[0] for row in FastQuery("""
			select u.count from unit_types ut
			left join reserves u on u.utid = ut.utid and u.player = %d
			order by ut.utid""" % player)])


  ### Players ###
	###############
	def createPlayer(self, uid, name):
		assert self.checkPlayer(name), "Player already exists, or is invalid."
		id = self.int_uid(name)
		FastQuery("""
			begin work;
			insert ignore into players (uid, id, name) values (%d, %d, '%s');
			insert ignore into reserves (utid, player, count) 
				select 1, %d, 50.0
				union all
				select 2, %d, 10.0
				union all
				select 3, %d, 10.0;
			commit;
			""" % (int(uid), id, safe(name), id, id, id))
		return id
	
	def deletePlayer(self, pid):
		FastQuery("""
			begin work;
			delete from reserves where player = %(pid)d;
			delete from players where id = %(pid)d;
			delete from alliances where ida = %(pid)d or idb = %(pid)d;
			update regions set owner = 0 where owner = %(pid)d;
			commit;
		""" % locals())
	
	def getReserves(self, pid):
		return floats([row[0] for row in FastQuery("""
			select r.count from unit_types ut
			left join reserves r on r.utid = ut.utid and r.player = %d
			""" % int(pid))])

	def claimRegion(self, player, region):
		FastQuery("update regions set owner = %d where id = %d" % (player, region))
		self.resetEnergy(region)
		return self

	def checkPlayer(self, name):
		if name is None or len(name) < 3 or name == "None":
			return False
		for row in FastQuery("select pid from players where name = '%s'" % ( safe(name) )):
			return False
		return True

	def getPlayers(self, uid):
		for row in FastQuery("select id, name from players where uid = %d" % int(uid)):
			yield row

	def getPlayer(self, id):
		for row in FastQuery("select name from players where id = %d" % int(id)):
			return row[0]
		return None

	def getRegion(self, rid):
		id = int(rid)
		q = Query("""
			SELECT id, owner, energy_group, region_energy, group_energy, utid, ucount, btid, bcount, boutput
			FROM regions_view
			WHERE id = %d""" % (id))
		return Region.fromQuery(q, id)

	def getRegions(self, player):
		q = Query("""
			SELECT id, owner, energy_group, region_energy, group_energy, utid, ucount, btid, bcount, boutput
			FROM regions_view
			WHERE owner = %d""" % int(player))
		ret = {}
		# do some in-memory queries to sort out the structured data
		for row in q.Query("select distinct id from __self__"):
			log('looping id:'+row.id)
			id = int(row.id)
			ret[id] = Region.fromQuery(q, id)
		return ret

	def getNeighbors(self, rid):
		global _tw_regions
		return _tw_regions[rid].neighbors

	RELATE_SELF = 0
	RELATE_ALLY = 1
	RELATE_HOSTILE = 2
	def getNeighborRelations(self, region):
		ret = {}
		for row in FastQuery("""
			select r.id, r.owner, (a.ida != null) as ally from regions r
			left join alliances a on a.ida = r.id or a.idb = r.id
			where r.id in (%s)""" % ', '.join(self.getNeighbors(region))):
			if row[1] == region:
				ret[row[0]] = self.RELATE_SELF
			elif row[2] == True:
				ret[row[0]] = self.RELATE_ALLY
			else:
				ret[row[0]] = self.RELATE_HOSTILE
		return ret

	### Moving Troops ###
	#####################
	def moveTroops(self, utid, count, regiona, regionb):
		counta = 0
		countb = 0
		for row in FastQuery("select count from units where utid = %(utid)d and region = %(regiona)d" % locals()):
			counta = row[0]
			break
		for row in FastQuery("select count from units where utid = %(utid)d and region = %(regionb)d" % locals()):
			countb = row[0]
			break
		assert count > 0 and count <= counta, "You do not have enough troops to do that"
		energy = self.getEnergy(regiona)
		cost = Query(sql="""
			select cost from action_costs
				where atid = (select atid from action_types
					where name = 'move')
				and utid = %d""" % utid, cache=True)[0]['cost']
		cost *= count
		assert cost <= energy, "You do not have the energy to move"
		self.takeEnergy(regiona, cost)
		counta = max(0, counta - count)
		countb = countb + count
		FastQuery("""
			begin work;
			update units set count = %(counta)f where region = %(regiona)d and utid = %(utid)d;
			replace into units (utid, region, count) values (%(utid)d, %(regionb)d, %(countb)f);
			commit;""" % locals())
		return cost

	def moveReserves(self, player, utid, count, dest_region):
		cur_reserves = floats([row[0] for row in FastQuery("""
			select count from reserves where player = %d and utid = %d
			""" % (player, utid))])
		assert len(cur_reserves) > 0, "No such reserves to move with."
		cur_reserves = cur_reserves[0]
		assert count > 0 and count <= cur_reserves, "Invalid count '%s' not in [1..%d)" % (count, cur_reserves)
		cur_units = floats([row[0] for row in FastQuery("""
			select count from units where region = %d and utid = %d
		""" % (dest_region, utid))])
		if len(cur_units):
			cur_units = cur_units[0]
		else:
			cur_units = 0.0
		FastQuery("""
			begin work;
			update reserves set count = %f where player = %d and utid = %d;
			replace into units (region, utid, count) values (%d, %d, %f);
			commit;
			""" % ( cur_reserves - count, player, utid, dest_region, utid, cur_units + count ))
		return self

	### Combat ###
	##############
	def computeDamage(self, attackType, attackCount, defenderType, defenderCount):
		attackDamage = [row[0] for row in \
			FastQuery("select weight from unit_matchups where utida = %d and utidb = %d" % (int(attackType), int(defenderType)))]\
			[0]
		# how much total attack damage will be done
		attackDamage *= attackCount
		# log("attack Damage: %f" % attackDamage)
		# how much hp on one defender
		baseDefenderHP = [row[0] for row in \
			FastQuery("select hp from unit_types where utid = %d" % int(defenderType))]\
			[0]
		# hwo much hp by all the defenders
		defenderHP = baseDefenderHP * defenderCount
		# decrement the hp by the damage
		defenderHP -= attackDamage
		# figure out how many defenders survived
		defenderCount = max(0,defenderHP / baseDefenderHP)
		# log("defender count after: %f" % defenderCount)
		return defenderCount
	
	def getDefenderType(self, attackType, defenders):
		""" Returns [1..3] on success. 0 on error. """
		defenderType = ((attackType + 1) % 3) + 1
		if defenders[defenderType - 1] < 1: # if no units of this type
			defenderType = ((defenderType + 1) % 3) + 1 # try the next
		if defenders[defenderType - 1] < 1:
			defenderType = ((defenderType + 1) % 3) + 1 # and the next
		return defenderType # it is possible to return a type for which the count is 0 (meaning there was no one to defend)

	def computeAttack(self, player, attacker, defender, utid, count):
		"""
			Every attack has 2 phases: attack and counter-attack.
			During the attack phase, the attacker region sends all of it units of type 'utid' to attack the defender region.
			 - Damage is calculated and the defender's numbers are updated
			During the counter-attack phase, the attacking unit is vulnerable, and the defender region selects the best available counter-attacking units
			 - Damage is calculated and the attacker's numbers are updated

			 Returns [ <energy remaining>, <attacker units>, <defender units> ]
		"""
		assert count > 0, "Cant attack with 0 units."

		# ATTACK PHASE:
		# how many units are available
		attackCountTotal = [row[0] for row in FastQuery("""
			select count from units where region = %d and utid = %d
			""" % (int(attacker), int(utid)))][0]
		assert count <= attackCountTotal, "Not Enough Units %d < %d" % (count, attackCountTotal)
		# how many units will be attacking
		attackCount = count
		# what type are they
		attackType = utid
		# how much energy does the attacking region need in order to attack with this many guys
		attackCost = [row[0] for row in \
			FastQuery("select cost from action_costs where atid = 1 and utid = %d" % int(attackType))]\
			[0]
		assert attackCost > 0, "Expected attack cost > 0, not: %f, check the action_costs table." % attackCost
		attackCost *= attackCount
		# how much energy is available
		availableEnergy = self.getEnergy(attacker)
		assert attackCost <= availableEnergy, "Not Enough Energy %f < %f" % (attackCost, availableEnergy)
		# consume the energy
		availableEnergy -= attackCost
		self.takeEnergy(attacker, attackCost)
		# how many defenders of all types
		defenderCounts = self.getUnits(defender)
		defenderType = self.getDefenderType(attackType, defenerCounts)
		defenderCount = defenderCounts[defenderType - 1]
		if defenderCount > 0:
			defenderCount = self.computeDamage(attackType, attackCount, defenderType, defenderCount)
			# update the db
			FastQuery("replace into units (utid, region, count) values (%d, %d, %f)" % ( int(defenderType), int(defender), defenderCount))
			# update our local cache of the counts
			defenderCounts[defenderType] = defenderCount
			# COUNTER-ATTACK PHASE
			caDefendType = attackType # the one who attacked now defends
			caAttackType = min(1,(caDefendType + 1) % 4) # the best available counter attacker hits them
			caAttackDamage = 0
			for i in range(3):
				if defenderCounts[caAttackType] > 0:
					break
				else:
					caAttackType = ((caAttackType + 1) % 3) + 1
			if defenderCounts[caAttackType] > 0:
				caDefenderCount = self.computeDamage(caAttackType, defenderCounts[caAttackType], caDefendType, attackCount)
				# log("counter attack defender after: %f" % caDefenderCount)
				FastQuery("replace into units (utid, region, count) values (%d, %d, %f)" % (int(caDefendType), int(attacker), float(caDefenderCount)))
		# RESULTS
		attackers = [0,] * 3
		defenders = [0,] * 3
		for row in FastQuery("select utid, count from units where region = %d" % int(attacker)):
			attackers[row[0] - 1] = int(row[1])
		for row in FastQuery("select utid, count from units where region = %d" % int(defender)):
			defenders[row[0] - 1] = int(row[1])
		# if we win, sieze the land
		if sum(defenders) == 0:
			self.claimRegion(player, defender)
		return [attackCost, attackers, defenders]

	def computeAttackFromReserves(self, player, defender, utid):
		""" Attacking from reserves is slightly different.
			* You can only send all of one type, so you dont pick a count.
			* There is only one region involved, so half of the counts come from the reserves table
			But, the damage calculation should all be the same.
		"""
		player = int(player)
		defender = int(defender)
		self.log("Attack: %d attacks region: %d" % (player, defender))
		attackType = int(utid)
		attackCount = [row[0] for row in FastQuery("""
			select count from reserves where player = %d and utid = %d
			""" % (player, attackType))][0]
		self.log("Attack: with attackType %d count %f" % (attackType, attackCount))
		# there is no attackCost, because attacking from reserves is free.
		defenderCounts = self.getUnits(defender)
		# if there were none of the proferred type, loop through the other
		defenderType = self.getDefenderType(attackType, defenderCounts)
		defenderCount = defenderCounts[defenderType - 1]
		if defenderCount > 0:
			oldCount = defenderCount
			defenderCount = defenderCounts[defenderType - 1] = self.computeDamage(attackType, attackCount, defenderType, defenderCount)
			damageDone = oldCount - defenderCount
			self.log("Attack: %f attackers did %f damage" % ( attackCount, damageDone))
			# update the db
			FastQuery(" replace into units (count, utid, region) values ( %f, %d, %d) " % (defenderCount, defenderType, defender))
			# COUNTER-ATTACK
			# First, select the best counter-attacker
			counterAttacker = ((attackType + 1) % 3) + 1
			for i in range(3):
				if defenderCounts[counterAttacker - 1] > 0:
					break
				counterAttacker = ((counterAttacker + 1) % 3) + 1
				self.log("Attack: trying counter-attacker %d" % counterAttacker)
			self.log("Attack: Counter-attacker: %d count %f" % (counterAttacker, defenderCounts[counterAttacker - 1]))
			# Second, do the attack
			if defenderCounts[counterAttacker - 1] > 0:
				oldCount = attackCount
				attackCount = self.computeDamage(counterAttacker, defenderCounts[counterAttacker - 1], attackType, attackCount)
				damageDone = oldCount - attackCount
				self.log("Attack: attackers took %f" % damageDone)
				# update the db (reserves table)
				FastQuery(" update reserves set count = %f where utid = %d and player = %d " % (attackCount, attackType, player))
		# RESULTS
		attackers = [0,] * 3
		defenders = [0,] * 3
		for row in FastQuery("select utid, count from reserves where player = %d order by utid" % player):
			attackers[row[0] - 1] = int(row[1])
		for row in FastQuery("select utid, count from units where region = %d" % defender):
			defenders[row[0] - 1] = int(row[1])
		# get the defending player (so we can send updates to them)
		defenderPlayer = [row[0] for row in FastQuery("""
			select owner from regions where id = %d""" % defender)][0]
		# if we win, sieze the land
		if sum(defenders) == 0:
			self.claimRegion(player, defender)
			# queue a REGIONLOST update for the defender
			if defenderPlayer > 0:
				self.queueStatusUpdate(defenderPlayer, "REGIONLOST", defender)
		else:
			# queue a UNITS update
			if defenderPlayer > 0:
				self.queueStatusUpdate(defenderPlayer, "UNITS", [ defender, defenders ])
		return [attackers, defenders]


	### Status Updates ###
	######################
			
	def queueStatusUpdate(self, player, kind, data):
		FastQuery("""
			insert into status_updates (player, kind, data, entered) values (%d, '%s', '%s', now());
			"""  % (int(player), safe(kind), json(data)))

	def getUnreadStatusUpdates(self, player):
		ret = [row[0:3] for row in FastQuery("""
			select id, kind, data from status_updates where sent = 0 and player = %d order by entered asc;
			""" % player)]
		if len(ret) > 0:
			FastQuery("""
				update status_updates set sent = 1 where id in (%s)
				""" % (','.join([str(row[0]) for row in ret])))
		return [row[1:3] for row in ret]


class Region():
	@staticmethod
	def fromQuery(q, id):
		""" Takes 'q', a Query object.  Does a series of sub-queries to build a Region object. """
		for row in q.Query("select distinct id, owner, energy_group, group_energy, region_energy from __self__ where id = %d" % id):
			if row.energy_group in (None,"None"):
				ret = Region(row.id, row.owner, row.region_energy)
			else:
				ret = Region(row.id, row.owner, row.group_energy, row.energy_group)
			ret.units = [0,] * 6
			ret.bldgs = [0,] * 6
			ret.outputs = [0,] * 6
			for row in q.Query('select distinct utid,ucount from __self__ where id = %d order by utid' % id):
				row = [x if x != "None" else 0 for x in row]
				if row[0] == 0:
					continue
				ret.units[int(row[0])-1] = float(row[1])
			for row in q.Query('select distinct btid,bcount from __self__ where id = %d order by btid' % id):
				row = [x if x != "None" else 0 for x in row]
				if row[0] == 0:
					continue
				ret.bldgs[int(row[0])-1] = float(row[1])
			for row in q.Query('select distinct btid, boutput from __self__ where id = %d order by btid' % id):
				row = [x if x != "None" else 0 for x in row]
				if row[0] == 0:
					continue
				ret.outputs[int(row[0])-1] = float(row[1])
			return ret # does not loop twice
	def __init__(self, id, owner, energy, group=None):
		global _tw_regions
		self.units = []
		self.bldgs = []
		self.owner = int(owner)
		self.id = int(id)
		self.group = group
		self.energy = float(energy)
		self.neighbors = _tw_regions[self.id].neighbors
		self.polygons = _tw_regions[self.id].polygons
	def __json__(self):
		return json([\
			self.id,\
			self.owner,\
			self.energy,\
			self.units,\
			self.bldgs,\
			self.neighbors,\
			# self.polygons,\
			])

class TechWarsActions(LimoBlock):
	def render(self, next=None):
		try:
			action = str(self.args.get('action',None))
			player = self.session.get('techwarsPlayer', None)
			if action == 'getPlayers':
				yield json( TechWarsModel().getPlayers(self.session.user.uid) )
			elif action == 'selectPlayer':
				player = int(self.args.get('player',None))
				for row in FastQuery("select uid from players where id = %d" % player):
					assert row[0] == self.session.user.uid
				self.session['techwarsPlayer'] = player
				yield json('OK')
			elif action == 'checkPlayer':
				yield json(TechWarsModel().checkPlayer(self.args.get('name',None)))
			elif action == 'createPlayer':
				player = int(self.args.get('player',None))
				ret = TechWarsModel().createPlayer(self.session.user.uid, player)
				self.session['techwarsPlayer'] = ret
				yield json(ret)

			# -------------------------------------------------------
			## Anything below here REQUIRES that a player be selected
			# -------------------------------------------------------
			if not player:
				assert player, "Must select a player."

			if action == 'getRegions':
				yield json(TechWarsModel().getRegions(player))
			elif action == "clearSelectedPlayer":
				del self.session['techwarsPlayer']
				player = None
				yield json("OK")
			elif action == 'deleteSelectedPlayer':
				pid = self.session['techwarsPlayer']
				TechWarsModel().deletePlayer(pid)
				del self.session['techwarsPlayer']
				yield json("OK")
			elif action == 'claimRegion':
				assert self.session.hasRole('admin')
				region = int(self.args.get('region',None))
				if player is not None and region is not None:
					TechWarsModel().claimRegion(player, region)
					yield json('OK')
				else:
					yield json(Exception('Missing region or player'))
			elif action == 'getNeighbors':
				region = self.args.get('region',0)
				yield json(TechWarsModel().getNeighbors(self, region))
			elif action == 'moveTroops':
				utid = int(self.args.get('utid',0))
				regiona = int(self.args.get('regiona',0))
				regionb = int(self.args.get('regionb',0))
				count = int(self.args.get('count',0))
				yield json(TechWarsModel().moveTroops(utid, count, regiona, regionb))
			elif action == 'computeAttack':
				attacker = int(self.args.get('attacker',0))
				defender = int(self.args.get('defender',0))
				utid = int(self.args.get('utid',0))
				count = int(self.args.get('count',0))
				assert attacker > 0 and defender > 0 and utid > 0
				yield json(TechWarsModel().computeAttack(player, attacker, defender, utid, count))
			elif action == 'computeAttackFromReserves':
				utid = int(self.args.get('utid',0))
				defender = int(self.args.get('defender',0))
				assert utid > 0 and defender > 999 and defender < 99999, "Invalid inputs"
				yield json(TechWarsModel().computeAttackFromReserves(player, defender, utid))
			elif action == 'getRegionHTML':
				region = int(self.args.get('region',0))
				assert region > 0, "Invalid region %s" % region
				yield json( ''.join(flatten(Template.render('region_body', { 'region': TechWarsModel().getRegion(region) }))))
			elif action == 'redeemOutput':
				region = int(self.args.get('region',0))
				assert region > 0, "Invalid region %s" % region
				yield json(TechWarsModel().redeemBuildingOutput(region).getUnits(region))
			elif action == 'addBuilding':
				region = int(self.args.get('region',0))
				btid = int(self.args.get('btid',0))
				assert region > 0, "Invalid region %s" % region
				assert btid > 0, "Invalid building type %s" % btid
				cost = TechWarsModel().addBuilding(region,btid)
				bldgs = TechWarsModel().getBuildings(region)
				yield json([cost, bldgs])
			elif action == 'getBuildings':
				region = int(self.args.get('region',0))
				assert region > 0, "Invalid region %s" % region
				yield json(TechWarsModel().getBuildings(region))
			elif action == 'moveReserves':
				utid = int(self.args.get('utid',0))
				count = int(self.args.get('count',0))
				target = int(self.args.get('target',0))
				assert utid > 0 and count > 0 and target > 0, "Invalid arguments."
				m = TechWarsModel().moveReserves(player, utid, count, target)
				yield json([m.getReserveUnits(player), m.getUnits(target)])
			elif action == 'statusUpdate':
				yield json(TechWarsModel().getUnreadStatusUpdates(player))
			else:
				yield json(Exception("Unknown action: %s" % action))
		except Exception, e:
			yield json(e)

class TechWarsBlock(HTMLBlock):
	def init(self):
		self.requireCSS('techwars.css')
		self.requireJS('techwars.js')
		self.setPageTitle('Techwars')
	def render(self, next=None):
		if not self.session.user:
			yield Template.render("must_login")
		elif self.session.get('techwarsPlayer',None) is None:
			yield Template.render('select_player', {'players': TechWarsModel().getPlayers(self.session.user.uid)})
		else:
			pid = int(self.session['techwarsPlayer'])
			regions = TechWarsModel().getRegions(pid)
			player = TechWarsModel().getPlayer(pid)
			neighbors = {}
			for region in regions.values():
				for neighbor in region.neighbors:
					neighbors.setdefault(neighbor, [])
					neighbors[neighbor].append(region.id)
			yield Template.render('region_list', {\
				"player": player,\
				"regions": regions,\
				"building_types": TechWarsModel().getBuildingTypes()\
			})
			yield """
			<script type="text/javascript">
				ProductionRates = ["""+', '.join(["%.8f" % f for f in TechWarsModel().getProductionRates()])+"""];
				ActionCosts = [""" +', '.join(["%.8f" % f for f in TechWarsModel().getActionCosts()])+"""];
			</script>
			"""
		yield LimoBlock.render(self, next)

class TechWarsPlayerBlock(HTMLBlock):
	def render(self, next=None):
		player = self.session.get('techwarsPlayer', None)
		if player is not None:
			reserves = TechWarsModel().getReserves(player)
			player = TechWarsModel().getPlayer(player)
			self.title = "Welcome, %s." % player
			self.body = Template.render('player_block', {
				'player': player,\
				'reserves': reserves,\
			});
		yield HTMLBlock.render(self, next)


