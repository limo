
<span class='label'>Reserves:</span>
<div class='reserves'>
	<ul>
		%(["<li class='%d'>%.2f</li>" % (i+1, reserves[i]) for i in range(len(reserves))])s
	</ul>
</div>
<a href="#attack" class="attack">Attack &raquo;</a>
<a href="#move" class="move">Move &raquo;</a>
<a href="#deselect-player" class="deselect">Deselect Player &raquo;</a>
<a href="#delete-player" class="delete">Delete Player &raquo;</a>

<div id="delete-player-confirm-dialog">
	<span>Are you sure you want to delete the player?</span>
	<button class='yes'>Yes</button>
	<button class='no'>No</button>
</div>
