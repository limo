
<li class='region-item' id='%(region.id)s'>
	<h3 class='region-title'><span>%(region.id)s</span></h3>
	<div id="region-%(region.id)s" class='region-body'>
		<!--<span class='label'>Id:</span><div class='id'>%(region.id)s</div>-->
		<!--<span class='label'>Owner:</span><div class='owner'>%(region.owner)s</div>-->
		<span class='label label-energy'>Energy:</span><div class='energy'>%("%.2f" % region.energy)s</div>
		<span class='label label-neighbors'>Neighbors:</span><div class='neighbors'>
			<ul>
				%(["<li>%s</li>" % n for n in region.neighbors])s
			</ul>
		</div>
		<span class='label label-units'>Units:</span><div class='units'>
			<ul>
				%(["<li class='%d'>%.2f</li>" % (i+1,region.units[i]) for i in range(3)])s
			</ul>
			<a href='#attack' class='attack'>Attack &raquo;</a>
			<a href='#transfer' class='transfer'>Move &raquo;</a>
		</div>
		<span class='label label-bldgs'>Buildings:</span><div class='bldgs'>
			<ul>
				%(["<li class='%d'>%.0f [<span class='output'>%.2f</span>]</li>" % (i+1,region.bldgs[i], region.outputs[i]) for i in range(3)])s
			</ul>
			<a href="#build" class="build">Build &raquo;</a>
			<a href="#redeem" class="redeem">Recruit &raquo;</a>
		</div>
	</div>
</li>
