

<p>You don't control any regions at the moment.

<p>If you've just begun the game, you should <span class='help' referto='#block-techwarsplayer .block-body .attack'>Attack &raquo;</span>
some Region using your Reserve troops.

<p>Browse the map below to choose a suitable initial region to attack.

<p>Where would you like to start looking: <input type="text" id="startRegion" /> <button class="btn-search">Search</button>

<div id="google-map"> </div>

