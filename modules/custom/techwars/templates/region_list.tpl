
<ul id="region-list" class="region-list">
%( ( render('region_body', {'region':v})
		 for k,v in regions.items()
	 )
)s
</ul>
<div id="google-map"></div>

<div id='transfer-dialog'>
</div>

<div id='build-dialog'>
	<ul>
		%(["<li btid='%d'>%s (%.0f energy)</li>" % (row[0], row[1], row[3]) for row in building_types])s
	</ul>
	<a href="#" class="cancel">Cancel</a>
</div>

<div id='attack-dialog'>
</div>

<div id='victory-dialog'>
	<h3>VICTORY!!</h3>
	<span>Congratulations, you have captured a territory.</span>
	<button>close</button>
</div>
