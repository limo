#!/usr/bin/python env

from modules.core import LimoBlock, LimoModel

class TutorialModel(LimoModel):
	def model(self):
		self\
			.defaultPage("/tutorial")\
			.defaultBlock("TutorialBlock")\
			.attachBlockToRegion("/tutorial", "main", "TutorialBlock")
		return super(LimoModel, self).model()


class TutorialBlock(LimoBlock):
	def render(self, next=None):
		yield "Hello World"
		yield LimoBlock.render(self, next)

