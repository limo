from __future__ import division
from modules.core import HTMLBlock, LimoModel
from db import Query, Model, safe
from template import Template, TemplateNotFoundError
from errors import HTTPStatusError, HTTPRedirect
import hashlib, os, sys, gzip, math


class BlogModel(LimoModel):
	def model(self):
		self\
			.table("blog_posts")\
				.column("postid char(40) primary key")\
				.column("blogid varchar(32) not null", version=1)\
				.column("senderid int not null")\
				.column("subject varchar(128) not null")\
				.column("body text not null")\
				.column("entered timestamp default current_timestamp")\
			.defaultPage("/blog")\
			.defaultBlock("BlogBlock")\
			.attachBlockToRegion("/","main","BlogBlock")\
			.attachBlockToRegion("/blog","main","BlogBlock")\
			.defaultRole("create blog posts")\
			.defaultRole("edit blog posts")\
			.assignRoleToGroup("create blog posts", "admin")\
			.assignRoleToGroup("edit blog posts", "admin")
		return super(LimoModel, self).model()

	def createPost(self, blog, sender, subject, body, entered='NULL'):
		if blog and sender and subject and body:
			postid = Model.int_uid(','.join([blog,sender,subject,body]))
			Query("""insert ignore into blog_posts (postid, blogid, senderid, subject, body, entered)
			  select '%s','%s', %d, '%s', '%s', '%s' """
			  % (postid, safe(blog), int(sender), safe(subject), safe(body), safe(entered)))
		else:
			raise Exception("Insufficient args: %s, %s, %s, %s, %s" \
					% (str(blog), str(sender), str(subject), str(body), str(entered)))

		return self

	def removePost(self, postid):
		if postid and len(postid) == 40:
			Query("delete from blog_posts where postid = '%s'" % safe(postid))
		return self

	def getAllPosts(self, blog, page=None, perpage=10):
		sql="""select bp.*, u.name as sender from blog_posts bp
			join users u on bp.senderid = u.uid
			where bp.blogid = '%s'
			order by entered desc""" % safe(blog)
		if page is not None:
			sql += """ limit %d, %d""" % ((page-1)*perpage, perpage)
		return Query(sql)

class BlogBlock(HTMLBlock):
	def action(self):
		if self.args.get('action', False) == 'new':
			if not self.session.hasRole("create blog posts"):
				raise HTTPStatusError(403, "Forbidden")
			blog = "default"
			sender = self.args.get('senderid', False)
			subject = self.args.get('subject', False)
			body = self.args.get('body', False)
			BlogModel().createPost(blog, sender, subject, body)
			self.session.addMessage("Blog Post created.");
		if self.args.get('action', False) == 'remove':
			if not self.session.hasRole("edit blog posts"):
				raise HTTPStatusError(403, "Forbidden")
			postid = self.args.get('postid', False)
			BlogModel().removePost(postid)
			self.session.addMessage("Blog Post removed.");

	def init(self):
		self.requireCSS("block-blog.css")
		self.requireJS("jquery.js")
		self.requireJS("block-blog.js")

	def render(self, next=None):
		self.title = ""
		blog = "default"
		page = int(self.args.get('p',1))
		perpage = int(self.args.get('pp',10))
		posts = BlogModel().getAllPosts(blog, page, perpage)
		# use a sub-query to add in a computed after-the-fact column
		if len(posts) > 0:
			posts = posts.Query("select *, '%s' as controls from __self__" % (self.template("block-blog-post-controls", {}) if self.session.hasRole("edit blog posts") else ""))
		pages = int((float(len(posts))/perpage)+.999)
		pager = self.template("pager", {'pages':pages, 'perpage': perpage, 'maxpages':8, 'page':page})
		params = { 'pager': pager,\
			'posts': posts,\
			'form': self.template("form-blog-create-post", {'args': self.args, 'uid': self.session.user.uid}) \
				if self.session.hasRole("create blog posts") else ""
		}
		self.body = self.template("block-blog", params)
		return HTMLBlock.render(self, next)

