$(document).ready(function() {
	$(".toggle-handle")
		.css('cursor','pointer')
		.click(function() {
			var p =	$(this).parent().find(".toggle-panel");
			p.slideToggle();
		});
	$(".block-blog-post-controls").each(function() {
		var postid = $(this).parent().attr('postid');
		$(this)
			.find(".remove")
			.click(function() {
				$.get( "/blog", {"action":"remove", "postid":postid}, function() {
					$(this).parent().remove();
				});
			});
		$(this)
			.find(".edit")
			.click(function() {
				alert("edit");
		});
	});
});
