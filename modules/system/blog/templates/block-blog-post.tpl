<div class="block-blog-post" id="blog-post-id-%(post.postid)s" postid="%(post.postid)s">
	<div class="header">
		<div class="header-inner">
			<span class="entered">Sent At:</span><div class="entered">%(post.entered)s</div>
			<span class="subject">Subject:</span><div class="subject">%(urldecode(post.subject))s</div>
			<span class="sender">by</span><div class="sender">%(post.sender)s</div>
		</div>
	</div>
	<div class="body">%(urldecode(post.body))s</div>
	%(post.controls)s
</div>
