<!-- the create form: -->
%(form)s
<!-- the list of posts in the current blog -->
<ul class="blog-post-list">
	<li class="blog-post-list-toc">POSTS: (%(len(posts))s):<br>
		<ul class="toc">
		%(''.join(["""
			<li><a href="/post/%s">%s</a></li>
			""" % (p.postid, urldecode(p.subject)) for p in posts]))s
		</ul>
	</li>
	<!--<li class="blog-post-list-head">
		%(pager)s
		<div class="clearboth">&nbsp;</div>
	</li>-->
	%(''.join(["""
	<li class='blog-post-list-item'>%s</li>
	""" % render("block-blog-post", {'post': post}) for post in posts]))s
</ul>
