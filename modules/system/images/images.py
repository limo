from __future__ import with_statement
from modules.core import StaticBlock, LimoModel
from db import Query, safe, FastQuery
from image import ImageLab
import hashlib, os, sys, gzip
from errors import *
from theme import Theme
from settings import Settings

##
# The Image module provides a dynamic image system that can alter existing images, or create them from scratch
# The ImageBlock creates an image in /cache based on profile information gleaned from the url,
# it then uses the core StaticBlock to re-route the request to serve the static file.
# The ImageModel defines the available profiles, and binds the /images url
##

class ImageModel(LimoModel):
	def model(self):
		self\
			.defaultBlock("ImageBlock")\
			.defaultPage("/images")\
			.defaultPage("/favicon.ico")\
			.attachBlockToRegion("/images", "entire", "ImageBlock")\
			.attachBlockToRegion("/favicon.ico", "entire", "ImageBlock")\
			.table("image_profiles")\
				.column("profile_id varchar(64) primary key not null")\
				.column("profile_text text not null")\
				.defaultData(["n2048x100.4c.png", """new("RGBA", (2048,100), (0,0,0,0))
					.fourcornerbox([0,0], (2048,100), radius=10, fill="#aaa", outline="#ccc")"""])\
				.defaultData(["n12x100.2c.png", """new("RGBA", (24,100), (255,255,255,255))
					.fourcornerbox([0,0], (24,100), radius=10, fill="#aaa", outline="#ccc")
					.crop([12,0,24,100])"""])\
				.defaultData(["n200x2048.4c.png", """new("RGBA", (200,2048), (0,0,0,0))
					.fourcornerbox([0,0], (200,2048), radius=10, fill="#eee", outline="#ccc")"""])\
				.defaultData(["n200x12.2c.png", """new("RGBA", (200,200), (255,255,255,255))
					.fourcornerbox([0,0], (200,200), radius=10, fill="#f33", outline="#ccc")
					.crop([0,0,200,24])"""])\
				.defaultData(["n140x20.2c.png", """new("RGBA", (160,20), (255,255,255,255))
					.fourcornerbox([0,0],(160,20), radius=10, fill="#ff3", outline="#333")
					.crop([0,0,140,20])"""])\
				.defaultData(["n2048x12.2c.blue.png", """new("RGBA", (2048,24), (255,255,255,255))
					.fourcornerbox([0,0], (2048,24), radius=10, fill="#77f", outline="#ccc")
					.crop([0,0,2048,12])"""])\
				.defaultData(["n12x12.2c.blue.png", """new("RGBA", (24,24), (255,255,255,255))
					.fourcornerbox([0,0], (24,24), radius=10, fill="#77f", outline="#ccc")
					.crop([12,0,24,12])"""])\
				.defaultData(["r48x48", """resize([48,48])"""])\
				.defaultData(["r1x19", """resize([1,19])"""])\
				.defaultData(["r45x45", """resize([45,45])"""])\
				.defaultData(["r50x54", """resize([50,54])"""])\
				.defaultData(["r60x45", """resize([60,45])"""])\
				.defaultData(["r45x60", """resize([45,60])"""])\
				.defaultData(["r60x60", """resize([60,60])"""])\
				.defaultData(["c45x45", """crop([0,0,45,45])"""])\
				.defaultData(["c50x50", """crop([0,0,50,50])"""])\
				.defaultData(["r320x480","""resize([320,480])"""])\
				.defaultData(["images", """open(r"%s", "r")"""])\
				.defaultData(["favicon.ico", """new("RGBA",[14,14], (255,255,255,255))
					.circle([4,4],4,fill="blue")
					.circle([9,9],5,fill="#aaaaff")
					.text((6,-1), "L")"""])
		return super(LimoModel, self).model()

	_profile_cache = {}
	def getProfileText(self, profile_id, default_value=False):
		if self._profile_cache.get(profile_id,False) is False:
			for row in FastQuery("""select profile_text from image_profiles where profile_id = '%s'""" % (safe(profile_id))):
				self._profile_cache[profile_id] = row[0]
				break
		return self._profile_cache.get(profile_id,default_value)

class ImageBlock(StaticBlock):
	def __init__(self, request, response, session):
		StaticBlock.__init__(self, request, response, session)
		model = ImageModel()
		url = request['REQUEST_URI'].split('/')
		script = "ImageLab()."
		output_file = [Settings.cacheDir,]
		input_file = False
		for token in url:
			if token == '':
				continue
			text = model.getProfileText(token, False)
			output_file.append(token)
			if text:
				script += text + "."
			else:
				# the first token in the url that isnt a recognized profile, must be an image
				input_file = token
				break
				#TODO: really instead of breaknig out here, we should allow urls like /<profile>/<image>/<profile>/<image>
				# to allow layering and compositing of images
				# probably use PIL's .merge() to do this
		# script will now have lots of steps, lets remove redundant ones
		script = script.replace('.open(r"%s", "r").open','.open')
		script = script.replace('.open(r"%s", "r").new','.new')
		if input_file:
			pre_theme_file = os.path.sep.join(["images",input_file])
			input_file = Theme().getFile(pre_theme_file)
			if not input_file:
				raise HTTPStatusError(404, "Image '%s' was never registered by any theme." % pre_theme_file)
			if not os.path.isfile(input_file):
				raise HTTPStatusError(404,"Token '%s' is not a recognized image profile, and is not a file found on disk (but it was registered in a theme?!?)." % input_file)
			if script.find("%s") > -1:
				script = script % input_file
			else:
				raise HTTPStatusError(404,"Got '%s' in url, but the script had no place to put it: %s" % (input_file, script))
		else:
			# there was no input file, so make sure the script doesn't ask for one
			script = script.replace('.open(r"%s", "r")','')
		# make sure the output folder is created
		if len(output_file) > 1:
			try:
				os.makedirs(os.path.sep.join(output_file[:-1]))
			except:
				pass
		# collapse the output_file to a path
		output_file = os.path.sep.join(output_file)
		# self.log("output_file: %s" % output_file)
		if (not os.path.isfile(output_file)) or (input_file and os.path.getmtime(output_file) < os.path.getmtime(input_file)):
			# generate the output file
			# hack for .gifs:
			if input_file and input_file[-4:].lower() == ".gif":
				# for now gifs just pass through un-touched, since PIL has no support for them
				with open(input_file, "rb") as fi:
					with open(output_file, "wb") as fo:
						fo.write(fi.read())
			else:
				# the real image generation
				script = script.replace('..','.')
				script = script.replace('\n','').replace('\t','').replace('\r','')
				while script[-1] == '.':
					script = script[:-1]
				# invoke the ImageLab script to instantiate a transformed image
				# and to write the image to disk
				try:
					im = eval(script)
				except Exception, e:
					self.log("Error during script eval: %s script: %s" % (str(e),script))
					raise
				try:
					im.save(output_file, "PNG")
				except AttributeError, e:
					raise HTTPStatusError(404, "Image script produced no image: %s" % (script if Settings.showVerbose404 else "(showVerbose404=false)"))
				except Exception, e:
					raise HTTPStatusError(500, "Could not save output_file: %s error: %s" % (output_file, str(e)))
				# self.log("wrote output file: %s" % output_file)
		# re-write the request to read from the generated output file
		request['REQUEST_URI'] = "/"+Settings.cacheDir+output_file.replace(Settings.cacheDir,'').replace(os.path.sep,"/")
		# self.log("Serving an OUTPUT FILE: %s" % str(request['REQUEST_URI']))
		# then serve the static file from the cache folder
		StaticBlock.__init__(self, request, response, session)

