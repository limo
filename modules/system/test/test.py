
from modules.core import LimoBlock
from model import LimoModel
from tests import LimoTest
from limoutil import *

class TestModel(LimoModel):
	def model(self):
		self\
				.defaultPage("/tests")\
				.defaultBlock("TestBlock")\
				.attachBlockToRegion("/tests","main","TestBlock")\
				.attachBlockToRegion("/tests","left","SessionBlock")\
				.attachBlockToRegion("/tests","right","EmptyBlock")

class TestBlock(LimoBlock):
	def render(self, next=None):
		yield ','.join([a.name() for a in LimoTest.all()])
		yield LimoBlock.render(self, next)
