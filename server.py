
import sys, os, re, datetime, compress, cStringIO, traceback, cgitb, Queue
from multiprocessing import Pool, Pipe, Lock, Process, cpu_count
from session import *
# from fcgi import WSGIServer
from fcgi import FastCGIServer
from fcgi.fcgiutil import flatten, GeneratorException
from template import Template, TemplateExecutionError, ellipsis, pad
from limoutil import *
import settings
Settings = settings.Settings
__all__ = ["LimoServer",]

class Cookies(dict):
	def __init__(self, s):
		if len(s) > 1:
			for t in s.split(';'):
				p = t.split('=')
				if len(p) == 2:
					(k,v) = p
					k = k.strip()
					v = v.strip()
					if self.get(k,None).value is None:
						self[k] = v
					else:
						log("Duplicate cookie value: %s %s %s" % (k,v,self[k]))
						raise DeleteCookie(k)
				else:
					raise Exception("len(t) did not == 2? %d %s from t: %s from s: %s" % (len(p), p, t, s))
	def __str__(self):
		return ';'.join(['%s=%s' % (k,v) for k,v in self.items()])

	# wrapped version of the dict accessors
	def get(self,k,default=None):
		try:
			return self.__getitem__(k)
		except KeyError:
			return Cookie(name=k,value=default)
	def __getitem__(self,k):
		return dict.__getitem__(self,k)
	def __setitem__(self,k,v):
		if type(v) in (str,unicode):
			dict.__setitem__(self,k,Cookie(name=k,value=v))
		elif v.__class__.__name__ is 'Cookie':
			dict.__setitem__(self,k,v)
		else:
			raise TypeError("Expected string or Cookie, got %s" % v.__class__.__name__)
	def __delitem__(self,k):
		dict.__delitem__(self,k)

class Cookie:
	def __init__(self,name,value,expires=None,path=None,domain=None):
		self.name = name
		self.value = value
		self.expires = expires
		self.max_age = -1
		self.path = path
		self.domain = domain
	def __str__(self):
		cookie = "%s=%s" % (self.name,self.value)
		expires = "; Expires=%s" % self.expires.strftime("%a, %d %b %Y %H:%M:%S GMT")\
				if type(self.expires) is datetime.datetime else ""
		path = "; Path=%s" % self.path if self.path is not None else ""
		domain = "; Domain=%s" % self.domain if self.domain is not None else ""
		max_age = "; Max-Age=%d" % self.max_age if self.max_age > -1 else ""
		cookie = cookie\
				+ expires\
				+ path\
				+ domain\
				+ max_age
		return cookie

class Response():
	def __init__(self):
		self.status = '200 OK'
		self.cookies = Cookies('')
		self.headers = {}
		self.headers['Content-Type'] = 'text/html'

	def getheaders(self):
		ret = [('Status',self.status),]
		for k,v in self.headers.items():
			ret.append((k,v))
		for k,v in self.cookies.items():
			ret.append(('Set-Cookie',str(v)))
		return ret

	def headerstring(self):
		return '\r\n'.join(["%s: %s" %(k,v) for k,v in self.getheaders()])+'\r\n\r\n'

	def __str__(self):
		return self.headerstring()

class Request(dict):
	def __init__(self, environ, stdin=None):
		for k,v in environ.items():
			if k == "wsgi.input":
				# add POST_DATA if we find an input stream to read
				self["POST_DATA"] = ''.join(v.readlines())
			if not k.startswith('wsgi.'):
				self[k] = v
		if stdin is not None:
			self["POST_DATA"] = ''.join(stdin.readlines())
		# add QUERY_STRING and REQUEST_URI if missing
		if self.get("QUERY_STRING",None) is None:
			self["QUERY_STRING"] = ""
		if self.get('REQUEST_URI',None) is None:
			q = ('?'+self["QUERY_STRING"]) if len(self["QUERY_STRING"]) > 0 else ""
			self['REQUEST_URI'] = self['SCRIPT_NAME']+q
			environ['REQUEST_URI'] = self['REQUEST_URI'] # write this back, in case we need it later during an error redirect out of here
		# normalize script_name
		i = self['REQUEST_URI'].find('?')
		if i > -1:
			self['SCRIPT_NAME'] = self['REQUEST_URI'][:i]
		else:
			self['SCRIPT_NAME'] = self['REQUEST_URI']
		# IIS puts all sorts of crap into the environment, which we should remove
		self._discarded = []
		for k,v in self.items():
			if not (k.startswith('HTTP')\
				or k in ('SERVER_SOFTWARE','SCRIPT_NAME','LOCAL_ADDR','REQUEST_METHOD','POST_DATA',\
					'SERVER_PROTOCOL','QUERY_STRING','CONTENT_LENGTH','SERVER_NAME','REMOTE_ADDR','SERVER_ADDR',\
					'PATH_TRANSLATED','SERVER_PORT','PATH_INFO','GATEWAY_INTERFACE','REMOTE_HOST','REQUEST_URI')):
				del self[k]
				self._discarded.append(k)
		self.cookies = Cookies(self.get('HTTP_COOKIE', ''))
		# log the request headers
		if Settings.logRequestHeaders:
			log("REQUEST: %s" % str(self))
	def __str__(self):
		return "self = %s; cookies = %s; discarded = %s;" % ('{'+', '.join(["'%s': '%s'" % (k,v) for k,v in self.items()\
			if not k.startswith('wsgi')])+'}', str(self.cookies), str(self._discarded))

class LimoProcess():
	_freeList = Queue.Queue()
	_all = []
	@staticmethod
	def get():
		try:
			p = LimoProcess._freeList.get()
			return p
		except:
			log("Exception in LimoProcess.get: %s" % str(e))
			return None
	@staticmethod
	def load():
		#return "[%.2f%%]" % ((1.0 - (LimoProcess._free/LimoProcess._total))*100)
		return "[%d/%d]" % (LimoProcess._freeList.qsize(), len(LimoProcess._all))
	def __init__(self, target,index=-1,skipModels=False):
		""" 'target' is a function like: def request(pipe, index, skip=False), that reads requests from the pipe and sends responses back over it.
		To work on win32, the target function cannot be a member of anything (static or otherwise).
		If skipModels is True, then the Model objects won't be initialized for that instance (since presumably they already were imported by some previous launch in the very recent future)
		"""
		self.target = target
		self.index = index
		self.is_started = False
		self.skip_models = skipModels
		if index == -1:
			self.index = len(LimoProcess._all)
		self.start()

	def free(self):
		if self.is_started:
			LimoProcess._freeList.put(self)

	def start(self):
		LimoProcess._all.append(self)
		self.parent_conn, child_conn = Pipe(True) # duplex pipes to talk to the children
		self.p = Process(target=self.target, args=(child_conn,self.index, self.skip_models))
		self.p.daemon = True
		self.p.start()
		try:
			self.send("INIT") # this will block until the process is awake enough to recieve it
			ack = self.recv() # and then again until it is awake enough to respond
			assert ack == "READY", "Expected READY, got: %s" % str(ack) # and lets just make sure nothing got garbled
			self.is_started = True
			self.free()
		except:
			self.stop(nicely=False)
			raise
	def stop(self,nicely=True):
		# right away make sure people dont try to use us for something while we are going down
		LimoProcess._all.remove(self)
		self.is_started = False
		# ask the process nicely to die
		if nicely:
			log("Sending STOP")
			self.send("STOP")
			log("Waiting for response")
			ack = self.recv()
			assert ack == "CLOSE", "Expected CLOSE, got: %s" % str(ack)
			log("Handler acknowledged stop request.")
		# then just pull the plug
		self.p.terminate()
		self.is_started = False
		del self.p
		del self.parent_conn

	# masquerade as a Pipe
	def send(self,data):
		return self.parent_conn.send(data)
	def recv(self):
		return self.parent_conn.recv()
	def close(self):
		self.stop()

class LimoServer(Loggable):
	def __init__(self):
		self.socket = None
		self.last_restart = datetime.datetime.now()

	def stop(self,message=None):
		self.log("SERVER GOT DEATH REQUEST: %s" % message)
		sys.exit(1)

	def log(self,msg):
		log("[%s] %s" % (self.socket, msg))

	def start(self,count):
		assert count > 0, "Must have at least one handler starting"
		self.procs = []
		# start a ghetto process pool
		# these will be our back end request handlers
		skipModels = False
		for i in range(count):
			self.procs.append(LimoProcess(fcgi_handler,i,skipModels))
			if not skipModels:
				skipModels = True
			self.log("Process %d started." % i)
			# TODO: Start On Demand, up to a maximum

	def restart(self):
		now = datetime.datetime.now()
		diff = ms_elapsed(self.last_restart)
		if diff < 10000:
			log("Detected two quick restarts (%.0fms), interpreting as an exit request." % diff)
			return False
		for s in self.procs:
			s.stop()
			s.start()
		return True

	def run(self, socket=Settings.tempDir+"/limo.sock", count=None):
		try:
			self.socket = str(socket)
			if count is None:
				count = cpu_count()
			if count != 0:
				self.log("Starting %d handler processes" % count)
				self.start(count)
			self.log("All handlers started.")
			# then start the front facing fastcgi server
			self.request_count = 0
			# ws = WSGIServer(self.wsgi_request, bindAddress = socket, multithreaded=True)
			self.log("Binding address: %s" % (str(socket)))
			ws = FastCGIServer(bindAddress = socket)
			restart = ws.run(self.fcgi_request,log_traffic=Settings.logAllRequestTraffic)
			if restart:
				self.log("Server restarting...")
				del ws
				if self.restart():
					self.run(socket=socket,count=0)
		except Exception, e:
			self.stop(str(e))
		finally:
			self.log("Server exiting...")

	# the new fastcgi server eschews wsgi for more stream-ability
	def fcgi_request(self, request):
		start_wait = datetime.datetime.now()
		run_pipe = LimoProcess.get()
		wait_time = ms_elapsed(start_wait)
		saw_header_end = False
		headers = ""
		try:
			start = datetime.datetime.now()
			request = Request(request.params, request.stdin)
			run_pipe.send(request)
			bytes = 0
			status = None
			while True:
				data = run_pipe.recv()
				if type(data) in (str, unicode):
					bytes += len(data)
					if status is None and data.startswith('Status: '):
						status = data[8:data.find('\r',8)]
					if not saw_header_end:
						n = data.find('\r\n\r\n')
						if n != -1:
							headers += data[:n+2] # grab the last chunk of headers plus one crlf
							saw_header_end = True
						else:
							headers += data
					yield data
				elif data.__class__.__name__ is 'EOFToken':
					break
				elif data.__class__.__name__ is 'ErrorToken':
					if data.fatal:
						run_pipe.stop()
					self.log("Handler returned ErrorToken: %s" % (data.msg))
				else:
					raise ValueError("Don't know what to do with response: %s %s" % (str(type(data)), str(data)))
			load = LimoProcess.load()+"+%d"%run_pipe.index
			render_time = ms_elapsed(start)
		finally:
			run_pipe.free()
		if not Settings.disableAllLogging:
			if wait_time > 1:
				wait_time = "+%.0f" % (wait_time)
			else:
				wait_time = ""
			if bytes > 10000:
				bytes = "%.2fKb" % (bytes / 1024.0)
			else:
				bytes = "%sb" % bytes
			log("%s %s %s %s %s %s %s %s %s" % (
				(str(start)[5:-3] if Settings.logRequestStartTime else ""),\
				(pad(str(load),9) if Settings.logRequestLoad else ""),\
				pad(request['REQUEST_METHOD'],4),\
				pad(ellipsis(request['REQUEST_URI'],40,17), 40),\
				pad(status,9),\
				pad("(%s)"%bytes,10),\
				pad("(%.0f%sms)"%(render_time, wait_time),11),\
				("[sha:%s]"%ellipsis(request.cookies.get('sha','None').value,7,0)[:-3] if Settings.logSessionToken else ""),\
				("\n[headers:\n%s]" % headers) if Settings.logResponseHeaders else "",\
				))

class EOFToken(object):
	""" This is a token passed from LimoProcess handlers to the dispatcher, to signal they are done sending data. """
	pass

class ErrorToken(object):
	""" This token is used by a handler to send data to the dispatcher that should be sent directly to the web server's stderr.
	If the fatal flag is True, the handler is considered dead, and is never .free()'d again.
	"""
	def __init__(self, msg, fatal=False):
		assert type(msg) in (str, unicode)
		self.msg = msg
		self.fatal = fatal

def get_redirect_response(e,defaultLocation="/"):
	response = Response()
	response.data = e.status
	response.status = e.status
	have_location = False
	for item in e.headers:
		response.headers[item[0]] = item[1]
		if item[0] == "Location":
			have_location = True
	if not have_location:
		response.headers["Location"] = defaultLocation
	# log("REDIRECT: %s Location: %s" % (e.status, response.headers["Location"]))
	return response

def get_status_response(e):
	response = Response()
	response.status = e.status
	for item in e.headers:
		response.headers[item[0]] = item[1]
	return response

def get_error_response():
	(a,b,c) = sys.exc_info()
	if ( a == GeneratorException ):
		x,y,z = (type(b.error), b.error, b.tb)
		del a,b,c
		a,b,c = x,y,z
	log("Handler saw an uncaught exception: %s: %s\n%s" % (a,b,'\n'.join(traceback.format_tb(c))))
	response = Response()
	response.status = "200 OK"
	response.headers["Content-Type"] = "text/html"
	response.data = cgitb.html((a,b,c))
	del a, b, c
	return response

# copies of fcgi_handler are the actual handler 'threads'
# each one waits for a request to land in its pipe, then processes it
# and writes the result back out the pipe
def fcgi_handler(pipe, index=0, skipModels=False):
	from limoutil import log, profile
	from cache import Cache
	import modules
	import settings
	try:
		while True:
			# block until we get a new request in the pipe
			request = pipe.recv()
			if request == "INIT": # initialize the handler
				Settings = reload(settings).Settings
				log("handler: Initializing modules...")
				modules.init(skipModels=skipModels)
				log("handler: Building sitemap...")
				sitemap = LimoModel().getSiteMap()
				log("handler: Done initializing and ready to process requests.")
				pipe.send("READY")
			elif request == "STOP": # stop the handler gracefully
				log("Stopping on request")
				break
			else: # begin a normal request phase
				try:
					Settings = reload(settings).Settings
					response = Response()
					session = Session(request, response)
					messages = session.getMessages() # TODO: pass session messages along so they get on the page somehow
					if len(messages):
						session.clearMessages()
						log("Got messages from previous page: %s" % messages)
					regions = {'entire': '', 'left': '', 'right': '', 'main': '', 'head': '', 'header': '', 'footer': ''}
					url = request["REQUEST_URI"].split("?")[0].split("/")
					# optionally, redirect to standardize URLs
					if Settings.strictScriptNameRedirects:
						s = request["SCRIPT_NAME"]
						if len(s) > 0 and s[-1] != "/" and s[-3] != "." and s[-4] != ".":
							fixed = s + "/"
							q = request["QUERY_STRING"]
							if len(q) == 0:
								raise HTTPRedirect(fixed, status="301 Moved Permanently")
					# compute regions
					for i in range(len(url)):
						step = '/'.join(url[:i+1]) if i > 0 else '/'
						try:
							regions.update(sitemap[step])
						except KeyError, e:
							break
					if Settings.logRequestRegions:
						log("REGIONS: %s" % str(regions))
					# define some one-off helper functions
					# get a list of real blocks, given a comma-sep list of block names
					def text_to_blocks(text, request, response, session):
						try:
							return [modules.blocks[block](request, response, session)
								for block in reversed(text.split(",")) if block != '']
						except KeyError:
							raise KeyError("%s not in %s" % (block, modules.blocks.keys()))
					# set up the blocks prior to rendering, call init() and action()
					# redirect if an action added a message to the session
					def init_blocks(b, request, response, session):
						ret = text_to_blocks(regions.get(b,''), request, response, session)
						for block in ret:
							block.init()
							try:
								# log("Calling %s.action()..." % str(type(block).__name__))
								block.action()
								if len(session.getMessages()) > 0:
									log("Got messages: %s" % session.getMessages())
									raise HTTPRedirect(request["SCRIPT_NAME"])
							except HTTPStatusError, e:
								log("Passing along status error: %s %s" % (str(type(e)), str(e)))
								raise
							except Exception:
								# session.addMessage("Action Failure: %s" % str(e))
								e = sys.exc_info()
								log("Action Failure: %s %s %s" % (str(e[0]), str(e[1]), traceback.format_tb(e[2])))
								del e
						return ret
					# given a list of blocks, b, call render() on each item and return the result
					def render_blocks(b):
						ret = None
						for block in regions[b]:
							ret = block.render(ret)
						return ret
					# this generator, on each call, will return one piece of the final result
					def result_generator(request, response, session):
						if Settings.developmentMode:
							modules.init(skipModels=False) # will only re-init changed modules
						if len(regions.get('entire','')) > 0:
							regions['entire'] = init_blocks('entire', request, response, session)
							yield render_blocks('entire')
						else:
							for region in regions.keys():
								if region == 'entire':
									continue
								regions[region] = init_blocks(region, request, response, session)
							page = {}
							for region in regions.keys():
								if region == 'entire':
									continue
								elif region == 'head':
									page[region] = render_blocks(region)
								else:
									page[region] = Template.render("region", {'name':region,'output':render_blocks(region)})
							page['page_class'] = 'logged-in' if session.user is not None else 'not-logged-in'
							yield Template.render("page", page)
					# the final output will come out at the end of a series of generator-filters

					# define the initial input generator as our result generator from above
					# this is the basic request generator that will pump LIMO's regions and return raw text or html
					gen = result_generator(request, response, session)
					# for now, compression is configured based on user agent
					# so, first get the configured compression type, like 'gzip','deflate'
					compression = Settings.getCompression(request.get('HTTP_USER_AGENT',''))
					# then if the user-agent really says it supports it, lets add a compressor filter
					if request.get('HTTP_ACCEPT_ENCODING','').find(compression) > -1:
						if Settings.logCompression:
							log("server: Using compression: %s" % compression);
						if compression == "deflate":
							gen = compress.generate_deflate(gen, cache_file=request.get('LIMO_CACHE_FILE',None))
						else:
							gen = compress.generate_gzip(gen, cache_file=request.get('LIMO_CACHE_FILE',None))
						response.headers['Vary'] = 'Accept-Encoding'
						response.headers['Content-Encoding'] = compression
					elif compression != None:
						# log("server: Compression enabled, but not supported by client: %s" % request.get('HTTP_ACCEPT_ENCODING'))
						pass
					# if we dont want to immediately write pieces of the page to the network as they are available
					# then use a quick buffering filter
					if Settings.bufferOutput:
						def buffer(g):
							buf = cStringIO.StringIO()
							count = 0
							for i in flatten(g):
								count += 1
								buf.write(i)
								# yield None # TODO: remove this
								# by yielding none here we keep loop overhead the same (for comparison sake), but dont produce any incremental output
								# this allows this setting to test the difference between Transfer-Encoding: chunked responses and not
							else:
								yield buf.getvalue()
							del buf
							# log("Buffered output collapsed %d writes" % count)
						# then wrap the result generator in a buffering filter
						gen = buffer(gen)
					# now that all the output layers are in place
					sent_headers = False
					send_count = 0
					# execute the whole generator tree and write the results out
					for text in flatten(gen):
						if text is not None and len(text) > 0:
							if not sent_headers:
								send_count += 1
								pipe.send(response.headerstring() + text)
								sent_headers = True
								continue
							send_count += 1
							pipe.send(text)
					# write an EOF to mark the end
					pipe.send(EOFToken())
				except GeneratorException, e: # generator exceptions bundle the real errors, so lets unpack and catch the real thing
					if e.error.__class__.__name__ == 'HTTPRedirect':
						pipe.send(get_redirect_response(e.error, request['REQUEST_URI']).headerstring())
						pipe.send(EOFToken())
						continue
					elif e.error.__class__.__name__ == 'HTTPStatusError':
						pipe.send(get_status_response(e.error).headerstring())
						pipe.send(EOFToken())
						continue
					else:
						response = get_error_response()
						pipe.send(response.headerstring())
						pipe.send(response.data)
						pipe.send(EOFToken())
						continue
				except HTTPRedirect, e:
					pipe.send(get_redirect_response(e, request['REQUEST_URI']).headerstring())
					pipe.send(EOFToken())
					continue
				except HTTPStatusError, e:
					pipe.send(get_status_response(e).headerstring())
					pipe.send(EOFToken())
					continue
				except KeyboardInterrupt: # the main process catches the sigint and tells us what to do about it
					continue
				except Exception, e:
					response = get_error_response()
					pipe.send(response.headerstring())
					pipe.send(response.data)
					pipe.send(EOFToken())
					continue
				finally: # do our best to clean up and finalize all the request objects
					try:
						# make sure all the .css and .js files required in this request are written out to the disk
						# (as the html we just sent out referenced them, so the browser is likely to request them soon)
						Cache.CSS.saveContent()
						Cache.JS.saveContent()
					except:
						pass
					try:
						# and that the requirements are cleared before processing the next request
						Cache.CSS.clear()
						Cache.JS.clear()
					except:
						pass
					try:
						# write the session back to the database
						session.save()
					except Exception, e:
						log("Error saving session: %s" % str(e))
						pass
					if Settings.profileServer and Settings.profileLogQueries:
						for (sql, ms) in Query.getQueryLog():
							log("QUERY: [%.2f] %s" % (float(ms), sql))
					Query.clearQueryLog()
	except EOFError: # the pipe died (maybe the parent process suddenly died)
		pass
	except Exception, e:
		response = get_error_response()
		pipe.send(response.headerstring())
		pipe.send(response.data)
		pipe.send(EOFToken())
		pipe.send(ErrorToken("Handler exiting due to error: %s %s" % (a, b), fatal=True))
		del a, b, c
		return
	finally:
		modules.clear()
		log("Handler exiting")
		pipe.close()
		log("Handler closed")
		sys.exit(1)

