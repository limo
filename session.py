
import sys, os, re, zlib, hashlib, base64, datetime, random
from db import Query, Model, safe, FastQuery
from model import LimoModel
from template import urldecode
from errors import *
from limoutil import *
from settings import Settings

class Session():
	def __init__(self, request, response):
		self.sha = None
		sha = request.cookies.get('sha','').value
		if len(sha) == 40 and sha == safe(sha):
			self._loadsha(sha)
		if self.sha is None:
			self._newsha()
		assert self.sha is not None
		response.cookies['sha'] = self.sha
		# response.cookies['sha'].expires = datetime.datetime.utcnow()+datetime.timedelta(minutes=Settings.sessionTimeout)
		response.cookies['sha'].max_age = Settings.sessionTimeout*60
		response.cookies['sha'].path = "/"
		# log the visit
		visit = self.logVisit(url=request['REQUEST_URI'], remote=request["REMOTE_ADDR"]+":"+request.get("REMOTE_PORT","-1"))
		self._dirty = False

	def _newsha(self):
		sha = hashlib.sha1()
		sha.update(str(datetime.datetime.now()))
		sha.update(str(os.urandom(8192)))
		sha = sha.hexdigest()
		FastQuery("begin work; insert into sessions (sha, data) values ('%s', '{}'); commit;" % sha)
		if not self._loadsha(sha):
			raise Exception("Critical Error: I ran an insert/commit, and then couldnt find the data I committed.  Database corruption?")
		assert self.sha is not None
		self._dirty = True
		return sha

	def _loadsha(self, sha):
		for row in FastQuery("select sid,sha,uid,data from sessions where sha = '%s'" % sha):
			try:
				assert row[3][:4] == 'eJyr', "Session data does not start with zlib magic bytes: %s" % row[3][:4]
				self.data = eval(zlib.decompress(base64.b64decode(row[3])))
			except Exception, e:
				self.data = {}
				self._dirty = True
			self.sid = int(row[0])
			self.sha = str(row[1])
			if row[2] == 0:
				self.user = None
			else:
				self.user = User(row[2])
			return True
		return False

	def logVisit(self, url='', remote=''):
		if Settings.detectSessionHijack:
			for row in FastQuery("select remote from visits where sid = %d order by entered desc limit 1" % self.sid):
				if row[0].split(':')[0] != remote.split(':')[0]:
					raise HTTPStatusError(500, "Session Hijack detected, session token '%s' was used on '%s' originally and now on '%s'"
							% (self.sha, row[0], remote))
		FastQuery("begin work; insert into visits (sid, url, remote) values (%d, '%s', '%s'); commit"
				% (self.sid, safe(url), safe(remote)))

	def login(self, login, passwd, save=False):
		assert len(login) > 0, "Login required"
		assert len(passwd) > 0, "Password required"
		for row in FastQuery("select uid from users where login = '%s' or email = '%s' and passwd = '%s'" % (login, login, passwd)):
			self.uid = row[0]
			self._dirty = True
			if save:
				self.save()
			return
		raise Exception("User not found")

	def logout(self, save=False):
		self.uid = 0
		self._dirty = True
		if save:
			self.save()

	def save(self):
		if self.user is None:
			uid = 0
		else:
			uid = self.user.uid
		if self._dirty:
			data = base64.b64encode(zlib.compress(str(self.data)))
			FastQuery("begin work; update sessions set data = '%s', uid = %d where sid = %d; commit" % (data, uid, self.sid))
			self._dirty = False

	def hasRole(self, name):
		if self.user is None:
			return False;
		return self.user.hasRole(name)

	def getMessages(self):
		return self.get('messages','')
	def addMessage(self, msg):
		self['messages'] = self.getMessages() + msg
	def clearMessages(self):
		del self['messages']

	# Session objects behave like a hash table
	def get(self, key, default):
		return self.data.get(key, default)
	def __getitem__(self, key):
		return self.data.__getitem__(key)
	def __setitem__(self, key, val):
		self.data.__setitem__(key, val)
		self._dirty = True
	def __delitem__(self, key):
		self.data.__delitem__(key)
		self._dirty = True

	def __str__(self):
		return "{sid: %s, sha: %s, user: %s, data: %s}" % (self.sid, self.sha, str(self.user), str(self.data))
	def __unicode__(self):
		return unicode(self.__str__())

class User():
	def __init__(self, uid):
		getuser=Query("""
			select u.*, r1.name as ur_name, r2.name as gr_name from users u
			left join user_roles ur on ur.uid = u.uid
			left join roles r1 on r1.rid = ur.rid
			left join group_users ug on ug.uid = u.uid
			left join group_roles gr on gr.gid = ug.gid
			left join roles r2 on r2.rid = gr.rid
			where u.uid = %d
		""" % uid)
		if len(getuser) > 0:
			self.uid = int(getuser['uid'][0])
			(self.name, self.login, self.email, self.theme)\
				= [urldecode(x) for x in getuser['name, login, email, theme'][0]]
			self.roles\
				= [str(r.role_name) for r in getuser.Query("""
				select distinct role_name from (
					select ur_name as role_name from __self__
					union
					select gr_name as role_name from __self__
				) where role_name <> 'None'
			""")]
		else:
			raise Exception("No such user id: %d" % int(uid))

	def setPassword(self, passwd):
		sha = Model.uid(passwd)
		Query("update users set passwd = '%s' where uid = %d" % (passwd, self.uid))

	def setTheme(self, theme):
		self.theme = theme
		Query("update users set theme = '%s' where uid = %d" % (theme, self.uid))

	def hasRole(self, role):
		role = str(role)
		for r in self.roles:
			if r == role:
				return True
		return False

	def __str__(self):
		return "{uid: %s, name: %s, login: %s, email: %s, roles: %s}" % (self.uid, self.name, self.login, self.email, str(self.roles))

	@staticmethod
	def getUser(login, password):
		password = Model.uid(password)
		q = Query("select uid from users where login = '%s' and passwd = '%s'" % (login, password))
		if len(q) is 1:
			return User(q[0].uid)
		else:
			return None

	@staticmethod
	def createUser(name, login, passwd, email, active=1):
		LimoModel()\
			.defaultUser(safe(name), safe(login), safe(passwd), safe(email), int(active))\
			.assignUserToGroup(login, 'users')\
			.execute()
		for row in Query("select uid from users where name = '%s' and login = '%s'" % (name, login)):
			return row.uid
		return False

