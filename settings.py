import re, sys

class Settings:
	tempDir = r"c:\temp" if sys.platform == "win32" else "/tmp"

	# where to listen for incoming fastcgi requests
	# ex. ('localhost',XXXX) will listen on tcp port XXXX
	# a filesystem path will try to listen on a UNIX Socket
	# ex. /tmp/limo.sock
	defaultSocket = ('localhost',7143)

	baseUrl = "http://localhost"

	allowedServerNames = ('localhost','192.168.1.101',)

	# the directory to store the cache (for compressed pages, processed images, etc.)
	# - this will see a LOT of activity so put it somewhere fast
	# - it can be relative to the location of limo.py, or absolute
	# - should not end in a "/"
	cacheDir = "cache"

	# minutes before session cookies expire
	sessionTimeout = 90

	# should the final html be prettify'd by BeautifulSoup (implies bufferOutput)
	prettifyHTML = True

	# will collapse all output to a single write
	bufferOutput = True

	# enabling Etags support
	supportEtags = True

	# if enabled, requests to urls that lack a trailing slash will be redirected to a url that includes a trailing slash.
	# http://localhost/foo -> http://localhost/foo/
	# the app server sees no difference between the two URLS, but browsers generate relative URLs differently for each.
	strictScriptNameRedirects = True

	# log all the incoming request headers
	logRequestHeaders = False

	# log all outgoing response headers
	logResponseHeaders = False

	# log the start times of each request
	logRequestStartTime = True

	# logs what the load on the server is at the time of each request
	logRequestLoad = True

	# log the regions that render each page
	logRequestRegions = False

	# log what kind of compression is used for each request
	logCompression = False

	# VERY VERBOSE: logs every fastcgi packet in and out
	logAllRequestTraffic = False

	# log the session token passed for each request
	logSessionToken = True

	# show verbose 404 messages, including the real error
	showVerbose404 = True

	# logs messages like: Theme[default] => templates/block.tpl => theme/default/templates/block.tpl
	# to let you know where files are coming from
	debugThemes = False

	# dumps a text sitemap to the log as the server starts up
	debugSitemap = True

	# log messages when each module is loaded successfully (errors are always logged)
	logModuleLoading = False

	# whether to profile anything at all
	# if set to False, all calls to profile() will return None
	# if true, every second call to profile('foo') returns the ms elapsed since the first call to profile('foo')
	profileServer = True
	# should profile results be logged to the log file
	profileLogOutput = False
	# will write every query in a request to the log file
	profileLogQueries = False
	# if the current session has profile privileges, output profile data in html
	profileHTMLOutput = False

	# if a theme cannot find a request file:
	abortOnMissingFile = False # raises 404 if some template is missing for that page
	warnOnMissingFile = True# print a message in the log when this happens
	# NOTE: the page will render anyway, but with some content missing

	# detect if a session token is used on one machine, and moved to another
	# causes an extra query for every session load
	detectSessionHijack = False

	disableAllLogging = False

	compressPerUserAgent = (
		("iPhone OS", "gzip"),
		("Android", "gzip"),
		(".*", "gzip"),
	)
	@staticmethod
	def getCompression(UserAgent):
		for k,v in Settings.compressPerUserAgent:
			if re.search(k, UserAgent) is not None:
				return v
		return None

	# controls a few things:
	# - should tests be loaded at server start?
	developmentMode = True

def _true(value):
	if value in (True,False,): # if they were actual boolean types already
		return value
	# otherwise only deal with strings
	assert type(value) == str
	# trim the strings
	while value[0] == ' ':
		value = value[1:]
	while value[-1] == ' ':
		value = value[:-1]
	value = value.lower()
	return (value == '1' or value == 'true')
