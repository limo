#!/usr/bin/env python
from __future__ import with_statement
import datetime, re, math, inspect, os
from cache import Cache
from limoutil import *
from fcgi.fcgiutil import flatten
from errors import *
from theme import Theme
import fcgi.fcgiutil

class Template:
	""" A simple extension to the standard %(value)s string substitution that allows any expression to be within the %()s

	render() returns a generator that will let you 'stream' the output of the template as it's processed.

	e.g. templates/region-head.tpl is just an echo template:
	%(output)s

	>>> Template.render("region-head", { 'output': 'foo' })
	'foo'

	Provide a list of file names, and the first valid file will be used.

	>>> Template.render(["region-foobar", "region-head"], { 'output': 'foo' })
	'foo'

	Since any expression is allowed in a replacement you can do things that you cant do with normal %s subsitution.

	Things like writing out lists:

	First, lets create a small test template.

	>>> o=open("templates/test-join.tpl")
	>>> o.write("data: %(', '.join(list_of_things))s")
	>>> o.close()

	Then, render the template with a simple list.

	The Template.render call returns a generator-iterator, so the ''.join() collapses that for the test.

	>>> ''.join(Template.render("test-join", {'list_of_things': ['one','two','three']}))
	'data: one, two, three'

	>>> import os
	>>> os.remove("templates/test-join.tpl")

	You can do things like access members of objects, items in dicts, or use any of the standard objects provided to every template:
	* all the functions from limoutil (log, ms_elapsed, etc)
	* render (Template.render)
	* math (the module)

	"""

	@staticmethod
	def render(file, args=None):
		if args is None:
			args = {}
		if file is None:
			raise TemplateNotFoundError(file)
		if type(file) == list or type(file) == tuple:
			file_str = os.path.sep.join(file)
		else:
			file_str = file
		if not file_str.endswith(".tpl"):
			file_tpl = file_str + ".tpl"
		else:
			file_tpl = file_str
		if not os.path.isfile(file_tpl):
			t = Theme().getFile(["templates",file_tpl])
			if t is None:
				raise TemplateNotFoundError("Could not find any of [%s, %s, %s]"
						% (file, file_str, ["templates",file_tpl]))
			file_tpl = t
		file = file_tpl
		if file is None or not os.path.isfile(file):
			raise TemplateNotFoundError(file)
		# profile("Template.render(%s)" % file)
		try:
			for i in ('log','profile','ms_elapsed','ellipsis','pad','urlencode','urldecode','htmlescape','math','json','flatten'):
				args[i] = eval(i)
			args['render'] = lambda x,y: ''.join(flatten(Template.render(x,y)))
			Template._start_timer(file)
			try:
				with open(file,"rb") as f:
					 yield Template.mod(f.read(), args, file)
			except IOError, e:
				raise TemplateNotFoundError(e)
			except Exception, e:
				raise TemplateExecutionError("While running template: %s, args: %s, error: %s" % (file, str(args), str(e)))
			finally:
				Template._stop_timer(file)
		finally:
			pass# profile("Template.render(%s)" % file)

	@staticmethod
	def mod(s, data,file=''):
		f = s.find('%(',0)
		g = 0
		ret = []
		while f > -1:
			# grab everything before the match
			# ret.append(s[g:f])
			yield s[g:f]
			# find the end of the match
			g = s.find(')s',f)
			if g == -1:
				raise TemplateSyntaxError("Opening %( must be matched by a closing )s")
			new = s[f+2:g] # grab the contents of the match
			try:
				new = eval(new,data) if new is not None else '' # evaluate the contents of the match
			except NameError, e:
				new = "NameError-%s"%str(e)
				new = new.replace("'","").replace('"','')
			except Exception, e:
				raise TemplateExecutionError("Error in template expression: %s %s while evaluating %%(%s)s in file %s" % \
						(str(type(e)),str(e),new,file))
			if new is not None:
				# insert the result of evaluation
				# ret.append(str(new))
				yield new
			g = g+2 # skip past the )s marker
			f = s.find('%(',g) # begin the next search
		# grab everything from the last match to the end
		# ret.append(s[g:len(s)])
		yield s[g:len(s)]
		# then stack it all up and return it
		# return ''.join(ret)

	@staticmethod
	def _start_timer(file):
		Template.debug_start[file] = datetime.datetime.now()

	@staticmethod
	def _stop_timer(file):
		time = datetime.datetime.now() - Template.debug_start[file]
		msec = (time.seconds * 1000) + (time.microseconds / 1000.0) + .000000001
		Template.debug_timing[file] = msec
		Template.debug_counts[file] = Template.debug_counts.get(file, 0) + 1

	debug_counts = {}
	debug_timing = {}
	debug_start = {}

class TemplateSyntaxError(Exception):
	pass

class TemplateExecutionError(Exception):
	pass

class TemplateNotFoundError(FileNotFoundError):
	def __str__(self):
		return "Template not found: %s" % self.file

