import BeautifulSoup, httplib2, re, datetime, threading, copy, time
from limoutil import *

__all__ = [ 'LimoTest' ]

alive_count = 0
load_report = {}
class AbortException(Exception):
	pass
class HttpTest(Loggable):
	def __init__(self,url=None,method="GET",post_data=None, fetch_resources=True):
		self.clear()
		if url is not None:
			self.load(url, method, post_data, fetch_resources)
	def clear(self):
		self.response = None
		self.soup = None
		self.jar = HttpTest._cookiejar()
		self.load_report = {}
	def test(self):
		raise NotImplementedError()
	def stress_test(self, thread_count=2, repeat_count=1):
		global alive_count, load_report
		def repeat(f, n=1):
			global alive_count
			try:
				for i in range(n):
					f()
			except AbortException:
				# exit gracefully (aborts can come from farther down as well)
				pass
			alive_count -= 1
		threads = []
		results = []
		for i in range(thread_count):
			c = copy.deepcopy(self)
			c.run_lock = threading.Lock()
			threads.append(threading.Thread(target=repeat, args=(c.test, repeat_count)))
			threads[-1].start()
			alive_count += 1
		try:
			while alive_count > 0:
				time.sleep(1)
			for i in range(thread_count):
				threads[-1].join()
			print ("Stress Test: %d threads, %d repeat count" % (thread_count, repeat_count))
			print ("Results:\n%s" % ('\n'.join(["%s:\n\t%.2f ms avg over %d runs" % (k,sum(v)/len(v),len(v)) for k,v in sorted(load_report.items())])))
		except Exception, e:
			print "Aborting stress_test because of exception: %s" % str(e)

	def check_lock(self):
		try:
			if self.run_lock.acquire(False):
				self.run_lock.release()
			else:
				raise AbortException()
		except AttributeError:
			pass

	def load(self, url, method="GET",post_data=None, fetch_resources=True):
		global load_report
		self.url = url
		headers = {}
		headers = self.jar.cookies(url)

		# print "Loading with method:",method,"headers:",headers
		self.check_lock()
		start = datetime.datetime.now()
		(self.response, self.content) = httplib2.Http().request(url, method=method, body=post_data, headers=headers)
		load_report[url] = load_report.get(url,[]) + [ms_elapsed(start)]
		# print "Got response:",self.response
		if self.response.get('set-cookie',None) is not None:
			# print "Setting cookie: %s" % self.response['set-cookie']
			self.jar.set_cookie(self.response['set-cookie'])
		self.resources = []
		try:
			self.soup = BeautifulSoup.BeautifulSoup(self.content)
			if fetch_resources:
				self.resources.extend( [x['href'] for x in self.soup.findAll('link')] )
				self.resources.extend( [x['src'] for x in self.soup.findAll('script') if x.get('src',None) is not None] )
				# print "Found resources: %s" % str(self.resources)
				for r in range(len(self.resources)):
					if type(self.resources[r]) not in (str,unicode):
						continue
					scan_css = self.resources[r].endswith(".css")
					res_url = _absolute(url, self.resources[r])
					# print "Loading resource",self.resources[r],"url:",res_url
					self.check_lock()
					start = datetime.datetime.now()
					self.resources[r] = (res_url, httplib2.Http().request(res_url, method="GET", headers=self.jar.cookies(res_url)), )
					load_report[res_url] = load_report.get(res_url, []) + [ms_elapsed(start)]
					if scan_css:
						url_re = r"url\(([^\)]+)\)"
						for match in re.finditer(url_re, self.resources[r][1][1]):
							sub_url = match.group(1)
							sub_url = _absolute(url, sub_url)
							# print "Loading sub_url",sub_url
							self.check_lock()
							start = datetime.datetime.now()
							self.resources.append( (sub_url, httplib2.Http().request(sub_url, method="GET", headers=self.jar.cookies(sub_url)), ) )
							load_report[sub_url] = load_report.get(sub_url, []) + [ms_elapsed(start)]
		except:
			raise
		return self
	def has_status(self, status):
		assert type(status) in (str, unicode), "Status must be a string: %s" % str(type(status))
		assert len(status) == 3, "Status must be a 3-number code, like 200, 302, etc. Not: '%s'" % status
		assert self.response.get('status','999')[:3] == status, "Status in response (%s) did not match expected value (%s)" % (self.response.get('status',''),status)
		return self
	def has_text(self, text):
		assert self.content.find(text) > -1, "Text %s not found in response: %s" % (text, self.content)
		return self
	def has_pattern(self, pattern):
		assert re.search(pattern, self.content), "Text did not match pattern: %s" % text
		return self
	def has_cookie(self, name,value=None):
		assert self.jar.has_cookie(name), "Response did not set expected cookie: %s" % name
		if value is not None:
			assert self.jar.get_cookie(name) == value, "Response contained proper cookie, but with unexpected value: %s expected: %s" % (self.cookies[name].value, value)
		return self
	def has_resource(self, name):
		names = [x[0] for x in self.resources]
		assert _contains(names, name), "Resource %s was not a part of the response: %s" % (name, names)
		return self
	def was_redirected(self, status="302",location=None):
		assert self.response.previous is not None and self.response.previous.get('status','999')[:3] == status, "Response was not previously redirected as expected."
		assert location is None or self.response.previous.get('location',None) == location, "Redirect was to the wrong url: %s expected: %s" % (self.response.previous.get('location',None), location)
		return self
	def has_soup(self, tag, **kw):
		assert len(self.soup.findAll(tag, **kw)) > 0, "Response did not match soup expression: '%s', %s" % (tag, str(kw))
		return self

	class _cookiejar(dict):
		def set_cookie(self, str):
			for s in str.split('\n'):
				c = HttpTest._cookie.parse(s)
				self[c.name] = c
			self._expire_cookies() # expire everything right away, in case we just set a deleted cookie
		def has_cookie(self, name):
			return self.get(name,None) is not None
		def get_cookie(self, name):
			return self[name].value
		def cookies(self, url):
			self._expire_cookies()
			d = url.split('/')
			(d,p) = ("."+d[2],'/'+'/'.join(d[3:]))
			return {"Cookie": '; '.join([str(x) for x in self.values()
					if (x.domain is None or d.find(x.domain) > -1)
						and (x.path is None or p.startswith(x.path))])}
		def _expire_cookies(self):
			now = datetime.datetime.utcnow()
			_del = []
			for k,v in self.items():
				if v.expires is not None and now > v.expires:
					_del.append(k)
			for k in _del:
				del self[k]
	class _cookie:
		def __init__(self,name,value,expires=None,path=None,domain=None):
			self.name = name
			self.value = value
			self.expires = expires
			self.path = path
			self.domain = domain
		@staticmethod
		def parse(string):
			c = HttpTest._cookie(None,None)
			for chunk in string.split('; '):
				try:
					(label,value) = chunk.split('=')
					label = label.lower()
					if label == "path":
						#print "Setting path: %s" % value
						c.path = value
					elif label == "expires":
						try:
							c.expires = datetime.datetime.strptime(value, "%a, %d-%b-%Y %H:%M:%S %Z")
							#print "Set expires: %s"%str(c.expires)
						except ValueError:
							raise ValueError("Server returned a cookie expiration date in non-regular format: %s" % value)
					elif label == "domain":
						assert value is not None and value[0] == "." # required by spec
						c.domain = value
						# print "Set domain: %s" % c.domain
					else:
						#print "Setting name: %s value: %s" % (label, value)
						c.name = label
						c.value = value
				except:
					raise
					#TODO handle 'httponly' garbage
			assert c.name is not None
			return c
		def __str__(self):
			return "%s=%s" % (self.name, self.value)

class LocalTest(HttpTest):
	"""
		Example sub-class of HttpTest:

		# >>> LocalTest().test()

		>>> LocalTest().stress_test(thread_count=4, repeat_count=2)

	"""
	def test(self):
		self\
			.load("http://localhost")\
				.has_status("200")\
				.has_soup('div',id="page",attrs={'class':"not-logged-in"})\
				.has_text("<html")\
				.has_text("</html>")\
				.has_cookie("sha")\
			.load("http://localhost",method="POST",post_data="action=login&username=admin&password=admin")\
				.was_redirected("303")\
				.has_status("200")\
				.has_soup('div',id='page',attrs={'class':'logged-in'})\
			.load("http://localhost?action=logout")\
				.has_status("200")\
				.was_redirected("303","http://localhost/")\
				.has_soup('div',id="page",attrs={'class':"not-logged-in"})

class LimoTest(HttpTest):
	_all = []
	@staticmethod
	def all():
		for test in LimoTest._all:
			yield test
	def __init__(self,url=None,method="GET",post_data=None, fetch_resources=True):
		HttpTest.__init__(self,url=url,method=method,post_data=post_data,fetch_resources=fetch_resources)
		LimoTest._all.append(self)
	def __del__(self):
		LimoTest._all.remove(self)
	# override load so we can use limo-based urls
	def load(self, page, method="GET", post_data=None, fetch_resources=True):
		HttpTest.load(self, _absolute(Settings.baseUrl,page), post_data=post_data, fetch_resources=fetch_resources)
		return self
	# override redirected so we can use limo-based urls
	def was_redirected(self, status="302", location=None):
		if location is not None:
			location = _absolute(Settings.baseUrl, location)
		HttpTest.was_redirected(self, status=status, location=location)
		return self
	def name(self):
		return self.__class__.__name__

def _contains(list, item):
	for i in list:
		if i == item:
			return True
	return False

def _absolute(base, offset):
	"""
		Make a url absolute from that base.
		>>> _absolute("http://localhost", "/cache/1234.js")
		'http://localhost/cache/1234.js'
		>>> _absolute("http://localhost/prematics", "/images/foo.png")
		'http://localhost/images/foo.png'
		>>> _absolute("http://localhost","prematics")
		'http://localhost/prematics'
		>>> _absolute("http://localhost/","prematics")
		'http://localhost/prematics'
		>>> _absolute("http://localhost","http://slashdot.org")
		'http://slashdot.org'
	"""
	# print("_absolute(%s, %s)" % (base, offset))
	b = base.lower().split("?")[0]
	if b[-1] == "/":
		b = b[:-1]
	o = offset.lower()
	domain = "/".join(b.split("/")[:3])
	if offset.startswith("/"):
		return domain + offset
	elif o.startswith('https:') or o.startswith('http:'):
		return offset
	else:
		return '/'.join((b, offset))

if __name__ == "__main__":
	import doctest
	doctest.testmod()
