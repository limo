#!/bin/zsh

PROCS=$1

if [ -z "$PROCS" ]; then
	echo "usage: $0 <num-simultaneous-processes>"
	exit 1
fi

for i in {1..$PROCS}; do
	wget -r -l inf -p http://localhost 2>&1 | grep Downloaded | awk "{print $i,\$0}" &
done

