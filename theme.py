#!/usr/bin/env python
import random, os, thread
from limoutil import *
from errors import *
from settings import Settings

class Theme(Loggable):
	""" Theme is really just a path redirector.
	During initialization, all the files under basedir are scanned for files,
	each file is stripped to its offset from the theme base, or the module directory base,
	and saved as a part of the theme under its shorter theme path.
	Example.

	>>> import os
	>>> try:
	...		os.mkdir("/tmp/theme_test")
	...		os.mkdir("/tmp/theme_test/theme1")
	...		os.mkdir("/tmp/theme_test/theme2")
	...		f = open("/tmp/theme_test/theme1/somefile.tpl","wb")
	...		f.write("somefile content")
	...		f.close()
	...		f = open("/tmp/theme_test/theme2/somefile.tpl","wb")
	...		f.write("this is the second somefile content")
	...		f.close()
	... except:
	...		pass
	>>> T = Theme(basedir="/tmp/theme_test")
	>>> T.provideFile("somefile.tpl", "/tmp/theme_test/theme1/somefile.tpl")
	>>> T.provideFile("somefile.tpl", "/tmp/theme_test/theme2/somefile.tpl")
	>>> T.getFile("somefile.tpl",theme="theme1")
	'/tmp/theme_test/theme1/somefile.tpl'
	>>> T.getFile("somefile.tpl", theme="theme2")
	'/tmp/theme_test/theme2/somefile.tpl'

	If provideFile() is called and pointed to a file outside of basedir,
	that file is considered a 'module file', as in, provided by a custom module,
	and should be considered the default value for the file if no theme provides
	its own file.

	Precedence
	----------

	Precedence for 'theme' files is binary, the theme either has the requested file and returns it,
	or it returns the best-match 'module' file.

	The module file with the lowest weight (passed to provideFile), is considered the best-match.

	>>> import limoutil
	>>> import datetime
	>>> start = datetime.datetime.now()
	>>> T = Theme()
	>>> ms_elapsed(start)
	0

	"""

	_instance = None
	def __init__(self, basedir="theme"):
		self.basedir = basedir
		if Theme._instance is not None:
			self.module_files = Theme._instance.module_files
			self.theme_files = Theme._instance.theme_files
		else:
			self.module_files = {}
			self.theme_files = {}
			Theme._instance = self
			for theme in os.listdir(basedir):
				d = os.path.sep.join([basedir,theme])
				if os.path.isdir(d):
					for mf in os.walk(d):
						loc = mf[0]
						loc_offset = loc.replace(d, '')
						for f in mf[2]:
							self.provideFile( os.path.sep.join([loc_offset, f]), os.path.sep.join([loc, f]), 99)
			self.sortAllFiles()

	def provideFile(self, filename, location, weight=999):
		"""
			provideFile should always be called in batches when possible,
			since you have to call sortAllFiles() for the results to apply.
		"""
		if filename[0] == os.path.sep:
			filename = filename[1:]
		if self.module_files.get(filename, None) is None:
			self.module_files[filename] = []
		if location.startswith(self.basedir):
			# strip the theme name off the file
			_t = location.split(os.path.sep)[len(self.basedir.split(os.path.sep)):][0]
			if Settings.debugThemes:
				self.log("Adding file %s to theme %s" % (filename, _t))
			if self.theme_files.get(_t, None) is None:
				self.theme_files[_t] = {}
			self.theme_files[_t][filename] = location
		else:
			if Settings.debugThemes:
				self.log("Providing module file: %s location: %s weight: %d" % ( filename, location, weight))
			self.module_files[filename].append( (weight, location) )

	def getFile(self, filename, theme="default"):
		if type(filename) in (list,tuple):
			filename = os.path.sep.join(filename)
		try:
			f = self.theme_files[theme][filename]
		except KeyError, e:
			try:
				f = self.module_files[filename][0][1]
			except KeyError, ee:
				if Settings.warnOnMissingFile:
					log("WARNING: Theme has no known source for requested file: %s" % filename)
				if Settings.abortOnMissingFile:
					raise ee
				f = None
		if Settings.debugThemes:
			log("Theme[%s] => %s => %s" % (theme, filename, f))
		return f

	def sortAllFiles(self):
		profile("sort theme files")
		for k,v in self.module_files.items():
			self.module_files[k] = sorted(v)[::-1] # a copy sorted by weight
		profile("sort theme files")
		count = len(self.module_files.keys())+len(self.theme_files.keys())
		if Settings.debugThemes:
			self.log("All %d files: %s" % (count, ", ".join(sorted(self.module_files.keys()))))
		else:
			self.log("Loaded %d files." % ( count, ))
		return count

if __name__ == "__main__":
	import doctest
	doctest.testmod()
