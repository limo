$(document).ready(function() {
		$("#pp-control").change(function() {
			var loc = window.location;
			var pp = $(this).val();
			if ( /pp=/.test(loc) ) {
				loc = loc + "";
				loc = loc.replace(/pp=\d+/,"pp="+pp);
			} else if ( /\?/.test(loc) ) {
				loc = loc + "&pp=" + pp;
			} else {
				loc = loc + "?pp=" + pp;
			}
			window.location = loc;
		});
});

