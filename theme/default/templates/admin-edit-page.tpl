
<form action="" method="post">
<input type="hidden" name="action" value="edit-page">
<div class="edit-page">
	<h3>%(page.url)s</h3>
	<div class="edit-page-inner">
		<table width="100%">
			%(''.join(['''
			<tr>
				<td width="100" class="bold">%s:</td>
				<td>
					<div class="add_block"><div class="controls"><div class="edit"><span>edit</span></div><div class="add"><span>add</span></div></div><ul>%s</ul></div>
					%s
				</td>
			</tr>
			''' % (r.name,\
						 ''.join(['<li>%s</li>'%b for b in allblocks]),\
						 ''.join([str(b) for b in r.blocks]),\
						)\
			for r in page.regions])\
			)s
		</table>
	</div>
</div>
</form>
