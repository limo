
<table class="table-visits" width="100%" cellspacing="0" cellpadding="0">
	<tr><th>#</th><th>entered<th>vid<th>sid<th>url<th>remote</tr>
	<tr>
		<td colspan="6">
			%(render("pager", {'rows': rows,
				'perpage': self.args.get('pp',10),
				'page': self.args.get('p',1),
			}))s
		</td>
	</tr>
	%((render("block-visits-row", {
		"index": i,
		"entered": "/".join(rows[i].entered.split("-")[1:]),
		"vid": rows[i].vid,
		"sid": rows[i].sid,
		"url": rows[i].url,
		"remote": "--",
	}) for i in range(len(rows))))s
</table>
