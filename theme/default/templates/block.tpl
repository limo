%(before)s
<div id='block-%(id)s' class='block'>
	<div id='block-%(id)s-inner' class='block-inner'>
		<h2 class="block-title"><span>%(title)s</span></h2>
		<div class="block-body">
			%(body)s
		</div>
	</div>
</div>
%(after)s
