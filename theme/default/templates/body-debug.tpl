<blockquote>
	<h3>Request</h3>: <table>
		%(("<tr><td>%s<td>%s" % (k,v) for k,v in sorted(request.items())))s
	</table>
	<h3>Response</h3>: %(str(response))s<br>
	<h3>Session</h3>: %(str(session))s<br>
	<h3>Templates</h3>: <table>
		%(("<tr><td>%s<td>%5.2fms<td>* %d</td></tr>" % (k, float(str(v)), counts.get(k,0) ) for k,v in sorted(timings.items())))s
	</table>
	<h3>Queries</h3>: <table>
		%(("<tr><td>%5.3fms<td>%s</tr>" % (q[1],q[0]) for q in queries))s
	</table>
</blockquote>
