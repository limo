<div class="blog-post-create-form">
	<div class="toggle-handle">Create New Post</div>
	<div class="toggle-panel hidden">
		<form action="" method="post">
			<input type="hidden" name="action" value="new">
			<input type="hidden" name="senderid" value="%(uid)s"><br>
			Subject: <input type="text" size="40" name="subject" value="%(args.get('subject',''))s"><br>
			Body:<br>
			<textarea name="body">%(args.get('body',''))s</textarea><br>
			<input type="submit" value="Submit">
		</form>
	</div>
</div>
