
<div class="module-row" id="module-row-%(row.mid)s">
	<span class="module-title">Name:</span>
	<span class="module-name">%(row.name)s</span>
	<span class="module-status">%("Active" if row.active == 1 else "Inactive")s</span>
	<a href="?action=%('enable' if row.active == 0 else 'disable')s&name=%(row.name)s">
	%("Enable" if row.active == 0 else "Disable")s
	</a>
</div>
