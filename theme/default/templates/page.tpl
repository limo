<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	%(head)s
	<body>
		<div id="page" class="%(page_class)s">
			<div id="page-inner">
				%(header)s
				%(right)s
				%(left)s
				%(main)s
				%(footer)s
			</div>
		</div>
	</body>
</html>
